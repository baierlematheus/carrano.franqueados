<?php defined('BASEPATH') || exit('No direct script access allowed');

class About extends Front_Controller
{
    public function index() {
		
		$this->load->model('about_model');
		$data['about'] = $this->about_model->getAbout();

		$this->load->view(__FUNCTION__, $data);
	}
}