<?php defined('BASEPATH') || exit('No direct script access allowed');

class About_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {


			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Imagem')
			->setFieldHelper('<b>Dimensões:</b> 822x538 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(2000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');

			$this->crud->addField('char')
			->setFieldName('title_pt')
			->setLabel('Título Português');	

			$this->crud->addField('text')
			->setFieldName('text_pt')
			->setLabel('Texto Português');

			$this->crud->addField('char')
			->setFieldName('title_en')
			->hideFrom('list')
			->setLabel('Título Inglês');	

			$this->crud->addField('text')
			->setFieldName('text_en')
			->hideFrom('list')
			->setLabel('Texto Inglês');	

			$this->crud->addField('char')
			->setFieldName('title_es')
			->hideFrom('list')
			->setLabel('Título Espanhol');	

			$this->crud->addField('text')
			->setFieldName('text_es')
			->hideFrom('list')
			->setLabel('Texto Espanhol');	

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	function getAbout(){

		return $this->db->select('*, ai.file_name as img')
		->from('about a')
		->join('about_image ai', 'ai.about_id = a.id', 'left')
		->get()
		->row_array();

	}

}