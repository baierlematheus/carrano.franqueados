<section class="sobre" id="sobre">
	<div class="content">
		<div class="left">
			<h2><?php echo $about['title_' . site_lang()]; ?></h2>
			<div class="texto"><?php echo nl2br($about['text_' . site_lang()] ); ?></div>
		</div>


		<div class="right">
			<img src="<?php echo site_url("graph/about_img/" . $about['img']); ?>" alt="Fábrica Carrano" title="Fábrica Carrano" class="img-fabrica">
			<?php echo Modules::run("brands") ?>
		</div>
	</div>
</section>