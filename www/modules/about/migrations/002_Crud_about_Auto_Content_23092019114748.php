<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019114748 

class Migration_Crud_About_Auto_content_23092019114748 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"text_en":{"type":"LONGTEXT","null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"text_es":{"type":"LONGTEXT","null":true}}', true);
		$this->dbforge->add_column('about', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'about_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('about_image');

				$this->db->query('ALTER TABLE ' . 'about_image' . '
					ADD CONSTRAINT `' . md5('about_image' . 'about' . 'about_id') . '`
					FOREIGN KEY (' . 'about_id' . ')
					REFERENCES `' . 'about' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"text_en":{"type":"LONGTEXT","null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"text_es":{"type":"LONGTEXT","null":true}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('about', $key);
		}
	
		
				
			


	}
	
}