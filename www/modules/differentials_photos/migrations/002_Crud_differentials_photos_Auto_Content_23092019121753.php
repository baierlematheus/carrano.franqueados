<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019121753 

class Migration_Crud_Differentials_photos_Auto_content_23092019121753 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"text_en":{"type":"LONGTEXT","null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"text_es":{"type":"LONGTEXT","null":true},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('differentials_photos', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'differentials_photos_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('differentials_photos_image');

				$this->db->query('ALTER TABLE ' . 'differentials_photos_image' . '
					ADD CONSTRAINT `' . md5('differentials_photos_image' . 'differentials_photos' . 'differentials_photos_id') . '`
					FOREIGN KEY (' . 'differentials_photos_id' . ')
					REFERENCES `' . 'differentials_photos' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"text_en":{"type":"LONGTEXT","null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"text_es":{"type":"LONGTEXT","null":true},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('differentials_photos', $key);
		}
	
		
				
			


	}
	
}