<?php defined('BASEPATH') || exit('No direct script access allowed');

class Differentials_photos extends Front_Controller
{
    public function index() {

		$this->load->model('differentials_photos_model');
		$data['differentials_photos'] = $this->differentials_photos_model->getDifferentialsPhotos();

		$this->load->view(__FUNCTION__, $data);
	}
}