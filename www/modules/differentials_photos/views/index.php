<section class="diferenciais" id="diferenciais">
	<div class="content">

		<?php
		foreach ($differentials_photos as $dp) {
			?>

			<div class="item">
				<img src="<?php echo site_url("graph/differentials_photos/" . $dp['img']); ?>">
				<h3 class="title"><?php echo $dp['title_' . site_lang()]; ?></h3>
				<div class="desc"><?php echo nl2br($dp['text_' . site_lang()] ); ?></div>
			</div>	
			<?php
		}		
		?>
	</div>
</section>
