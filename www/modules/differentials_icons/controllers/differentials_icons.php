<?php defined('BASEPATH') || exit('No direct script access allowed');

class Differentials_icons extends Front_Controller
{
    public function index() {

		$this->load->model('differentials_icons_model');
		$data['differentials_icons'] = $this->differentials_icons_model->getDifferentialsIcons();

		$this->load->view(__FUNCTION__, $data);
	}
}