<?php defined('BASEPATH') || exit('No direct script access allowed');

class Differentials_icons_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Ícone')
			->setFieldHelper('<b>Dimensões:</b> máx: 137x120 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg | .png ')
			->setMaxSize(1000)
			->setValidationRules('required')
			->setAllowedTypes('jpg|png');

			$this->crud->addField('char')
			->setFieldName('title_pt')
			->setLabel('Descrição Português');	

			$this->crud->addField('char')
			->setFieldName('title_en')
			->hideFrom('list')
			->setLabel('Descrição Inglês');	

			$this->crud->addField('char')
			->setFieldName('title_es')
			->hideFrom('list')
			->setLabel('Descrição Espanhol');	

			$this->wtche->form->addField('number')
			->setFieldName('order')
			->setLabel('Ordem')
			->setValidationRules('required')
			->setInlineEdit();

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	function getDifferentialsIcons(){

		return $this->db->select('di.*, dii.file_name as img')
		->from('differentials_icons di')
		->join('differentials_icons_image dii', 'dii.differentials_icons_id = di.id', 'left')
		->where('di.active', 1)
		->order_by('di.order', 'ASC')
		->get()
		->result_array();

	}

}