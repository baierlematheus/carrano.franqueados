<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019133017 

class Migration_Crud_Differentials_icons_Auto_content_23092019133017 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('differentials_icons', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'differentials_icons_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('differentials_icons_image');

				$this->db->query('ALTER TABLE ' . 'differentials_icons_image' . '
					ADD CONSTRAINT `' . md5('differentials_icons_image' . 'differentials_icons' . 'differentials_icons_id') . '`
					FOREIGN KEY (' . 'differentials_icons_id' . ')
					REFERENCES `' . 'differentials_icons' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"title_pt":{"type":"VARCHAR","constraint":255,"null":true},"title_en":{"type":"VARCHAR","constraint":255,"null":true},"title_es":{"type":"VARCHAR","constraint":255,"null":true},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('differentials_icons', $key);
		}
	
		
				
			


	}
	
}