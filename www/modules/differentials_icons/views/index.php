<div class="content icones">
	<?php
	foreach ($differentials_icons as $di) {
		?>
		<div class="box">
			<div class="bx-img">
				<img src="<?php echo base_url() . 'uploads/differentials_icons_image/' . $di['img']; ?>" class="icon" />
			</div>
			<div class="desc"><?php echo $di['title_' . site_lang()]; ?></div>
		</div>
		<?php
	}		
	?>
</div>