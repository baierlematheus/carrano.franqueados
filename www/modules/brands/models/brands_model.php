<?php defined('BASEPATH') || exit('No direct script access allowed');

class Brands_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('char')
			->setFieldName('name')
			->setValidationRules('required')
			->setLabel('Nome');

			$this->crud->addField('char')
			->setFieldName('link')
			->setLabel('Link')
			->setValidationRules('required')
			->setFieldHelper('Exemplo: http://www.google.com.br');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Logo')
			->setFieldHelper('<b>Dimensões:</b> máx: 156x81 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg | .png')
			->setMaxSize(1000)
			->setValidationRules('required')
			->setAllowedTypes('jpg|png');

			$this->wtche->form->addField('number')
			->setFieldName('order')
			->setLabel('Ordem')
			->setValidationRules('required')
			->setInlineEdit();

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}


	function getBrands(){

		return $this->db->select('*, bi.file_name as img')
		->from('brands b')
		->join('brands_image bi', 'bi.brands_id = b.id', 'left')
		->where('b.active', 1)
		->order_by('b.order', 'ASC')
		->get()
		->result_array();

	}

}