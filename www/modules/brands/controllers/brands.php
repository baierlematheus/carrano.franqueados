<?php defined('BASEPATH') || exit('No direct script access allowed');

class Brands extends Front_Controller
{
    public function index() {

		$this->load->model('brands_model');
		$data['brands'] = $this->brands_model->getBrands();

		$this->load->view(__FUNCTION__, $data);
	}
}