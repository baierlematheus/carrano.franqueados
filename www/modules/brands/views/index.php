<div class="marcas">
	<?php
	foreach ($brands as $brand) {
		?>
		<a href="<?php echo $brand['link']; ?>" target="_blank">
			<img src="<?php echo base_url() . 'uploads/brands_image/' . $brand['img']; ?>" alt="<?php echo $brand['name']; ?>" title="<?php echo $brand['name']; ?>"/>
		</a>
	<?php  } ?>
</div>