<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019113358 

class Migration_Crud_Brands_Auto_content_23092019113358 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"link":{"type":"VARCHAR","constraint":255,"null":false},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('brands', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'brands_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('brands_image');

				$this->db->query('ALTER TABLE ' . 'brands_image' . '
					ADD CONSTRAINT `' . md5('brands_image' . 'brands' . 'brands_id') . '`
					FOREIGN KEY (' . 'brands_id' . ')
					REFERENCES `' . 'brands' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"link":{"type":"VARCHAR","constraint":255,"null":false},"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('brands', $key);
		}
	
		
				
			


	}
	
}