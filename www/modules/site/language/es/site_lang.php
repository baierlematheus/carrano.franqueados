<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// MENUS
$lang['menu_sobre'] = 'Sobre Carrano';
$lang['menu_diferenciais'] = 'Diferenciales';
$lang['menu_comunicacao'] = 'Comunicación';
$lang['menu_eventos'] = 'Eventos';


$lang['bt_franqueado'] = 'SER UNA FRANQUICIA';

$lang['tit_depoimentos'] = 'QUIEN VENDE CARRANO';
$lang['desc_depoimentos'] = 'Si su tienda se identifica con la sofisticación de la marca y sus modelos atrevidos, ¡Carrano es la opción correcta para estar presente en su PDV!';


$lang['Janeiro'] = 'enero';
$lang['Fevereiro'] = 'febrero';
$lang['Março'] = 'marzo';
$lang['Abril'] = 'abril';
$lang['Maio'] = 'mayo';
$lang['Junho'] = 'junio';
$lang['Julho'] = 'julio';
$lang['Agosto'] = 'agosto';
$lang['Setembro'] = 'septiembre';
$lang['Outubro'] = 'octubre';
$lang['Novembro'] = 'noviembre';
$lang['Dezembro'] = 'diciembre';

$lang['tit_eventos'] = 'CALENDARIO DE FERIAS Y EVENTOS';

$lang['tit_contato'] = 'FORMULARIO DE CONTACTO';
$lang['desc_contato'] = '¡Complete el formulario y tenga a Carrano en su tienda ahora!';

$lang['cc_nome'] = 'Tu nombre'; 
$lang['cc_email'] = 'Su correo electrónico'; 
$lang['cc_phone'] = 'Su teléfono';
$lang['cc_loja'] = 'Tiene una tienda';
$lang['cc_sim'] = 'Sí';
$lang['cc_nao'] = 'No';
$lang['cc_estado'] = 'Estado';
$lang['cc_sel_estado'] = 'Seleccione su estado';
$lang['cc_sel_cidade'] = 'Seleccione su ciudad';
$lang['cc_pais'] = 'Pais';
$lang['cc_sel_pais'] = 'Selecciona tu pais';
$lang['cc_cidade'] = 'Ciudad';
$lang['cc_msg'] = 'Mensaje'; 
$lang['cc_esc_msg'] = 'Escribe tu mensaje';
$lang['cc_enviar'] = 'Enviar formulario';

$lang['msg_ok'] = '¡Contacto enviado con éxito!';
$lang['msg_error'] = 'Error en el envío del contacto. Por favor, inténtelo de nuevo más tarde.';