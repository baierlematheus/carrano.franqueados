<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// MENUS
$lang['menu_sobre'] = 'About Carrano';
$lang['menu_diferenciais'] = 'Competitive Advantages';
$lang['menu_comunicacao'] = 'Communication';
$lang['menu_eventos'] = 'Events';

$lang['bt_franqueado'] = 'Be a franchisee';

$lang['tit_depoimentos'] = 'WHO SELLS CARRANO';
$lang['desc_depoimentos'] = "If your store identifies with the brand's sophistication and its bold models, Carrano is the right choice to be present at your PDV!";


$lang['Janeiro'] = 'january';
// $lang['Fevereiro'] = 'february';
$lang['Fevereiro'] = 'february';
$lang['Março'] = 'march';
$lang['Abril'] = 'april';
$lang['Maio'] = 'may';
$lang['Junho'] = 'june';
$lang['Julho'] = 'july';
$lang['Agosto'] = 'august';
$lang['Setembro'] = 'september';
$lang['Outubro'] = 'october';
$lang['Novembro'] = 'november';
$lang['Dezembro'] = 'december';


$lang['tit_eventos'] = 'FAIR AND EVENTS CALENDAR';

$lang['tit_contato'] = 'CONTACT FORM';
$lang['desc_contato'] = 'Fill out the form and have Carrano in your store now!';


$lang['cc_nome'] = 'Your Name'; 
$lang['cc_email'] = 'Your Email'; 
$lang['cc_phone'] = 'Your Phone';
$lang['cc_loja'] = 'Has a store';
$lang['cc_sim'] = 'Yes';
$lang['cc_nao'] = 'No';
$lang['cc_estado'] = 'State';
$lang['cc_sel_estado'] = 'Select Your State';
$lang['cc_sel_cidade'] = 'Select Your City';
$lang['cc_pais'] = 'Country';
$lang['cc_sel_pais'] = 'Select Your Country';
$lang['cc_cidade'] = 'City';
$lang['cc_msg'] = 'Message'; 
$lang['cc_esc_msg'] = 'Write your message';
$lang['cc_enviar'] = 'Submit Form';

$lang['msg_ok'] = 'Contact sent successfully!';
$lang['msg_error'] = 'Contact submission failed. Please try again later.';