<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


// MENUS
$lang['menu_sobre'] = 'Sobre a Carrano';
$lang['menu_diferenciais'] = 'Diferenciais';
$lang['menu_comunicacao'] = 'Comunicacão';
$lang['menu_eventos'] = 'Eventos';


$lang['bt_franqueado'] = 'Seja um revendedor';

$lang['tit_depoimentos'] = 'QUEM VENDE CARRANO';
$lang['desc_depoimentos'] = 'Se a sua loja se identifica com a sofisticação da marca e os seus modelos ousados, a Carrano é a escolha certa para estar presente no seu PDV!';


$lang['Janeiro'] = 'janeiro';
$lang['Fevereiro'] = 'fevereiro';
$lang['Março'] = 'março';
$lang['Abril'] = 'abril';
$lang['Maio'] = 'maio';
$lang['Junho'] = 'junho';
$lang['Julho'] = 'julho';
$lang['Agosto'] = 'agosto';
$lang['Setembro'] = 'setembro';
$lang['Outubro'] = 'outubro';
$lang['Novembro'] = 'novembro';
$lang['Dezembro'] = 'dezembro';

$lang['tit_eventos'] = 'CALENDÁRIO DE FEIRAS E EVENTOS';


$lang['tit_contato'] = 'FORMULÁRIO DE CONTATO';
$lang['desc_contato'] = 'Preencha o formulário e tenha Carrano na sua loja já!';


$lang['cc_nome'] = 'Seu nome'; 
$lang['cc_email'] = 'Seu e-mail'; 
$lang['cc_phone'] = 'Seu telefone';
$lang['cc_loja'] = 'Possui loja';
$lang['cc_sim'] = 'Sim';
$lang['cc_nao'] = 'Não';
$lang['cc_estado'] = 'Estado';
$lang['cc_sel_estado'] = 'Selecione seu estado';
$lang['cc_sel_cidade'] = 'Selecione sua cidade';
$lang['cc_pais'] = 'País';
$lang['cc_sel_pais'] = 'Selecione seu país';
$lang['cc_cidade'] = 'Cidade';
$lang['cc_msg'] = 'Mensagem';
$lang['cc_esc_msg'] = 'Escreva sua mensagem';
$lang['cc_enviar'] = 'Enviar Formulário';


$lang['msg_ok'] = 'Contato enviado com sucesso!';
$lang['msg_error'] = 'Falha no envio do Contato. Por favor tente novamente mais tarde.';