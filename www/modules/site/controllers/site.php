<?php defined('BASEPATH') || exit('No direct script access allowed');

class Site extends Front_Controller
{
 //    public function _remap($method, $params=array()){
	// 	$this->load->view($method, array('params'=>$params));
	// }

	public function index(){
		
		$this->load->model('events/events_model');
		$data['events'] = $this->events_model->getEvents();

		$this->load->view('index.php', $data);
	}
}