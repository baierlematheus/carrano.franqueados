<?php
	$this->load->view('site/includes/head');
	$this->load->view('site/includes/header');
?>

<div class="main">
	<main class="content-form">
		<a href="<?php echo site_url(); ?>" class="link">VOLTAR</a>
		<hr>
		<span class="row">&nbsp;</span>
		<span class="row">&nbsp;</span>

		<form action="" class="js-form-validate">
			<fiedlset class="row">
				<legend>
					<strong>Contato</strong>
				</legend>

				<label class="row">
					<input type="text" value="" class="input" required="required">
				</label>
				
				<label class="row">
					<input type="email" value="" class="input" required="required">
				</label>

				<label class="row">
					<input type="tel" value="" class="input" required="required" data-mask-type="phone">
				</label>

				<label class="row" id="parsley-container-estado">
					<div class="select-mask">
						<select name="" id="" required="required" class="input select" data-parsley-errors-container="#parsley-container-estado">
							<option value="" selected disabled>Estado</option>
							<option value="etado-1"> estado 1 </option>
							<option value="etado-2"> estado 2 </option>
							<option value="etado-3"> estado 3 </option>
							<option value="etado-4"> estado 4 </option>
						</select>
					</div>
				</label>

				<label class="row" id="parsley-container-cidade">
					<div class="select-mask">
						<select name="" id="" required="required" class="input select" data-parsley-errors-container="#parsley-container-cidade">
							<option value="" selected disabled>Cidade</option>
							<option value="etado-1"> estado 1 </option>
							<option value="etado-2"> estado 2 </option>
							<option value="etado-3"> estado 3 </option>
							<option value="etado-4"> estado 4 </option>
						</select>
					</div>
				</label>

				<label class="row">
					<textarea name="" id="" placeholder="Mensagem" required="required" class="input textarea"></textarea>
				</label>
			</fiedlset>

			<?php /* SUBMIT --------------------------------------------------------------------------------------- */ ?>
			<label class="row">
				<input type="submit" value="enviar" class="input submit">
			</label>
		</form>
	</main>
</div>

<?php $this->load->view('site/includes/footer'); ?>