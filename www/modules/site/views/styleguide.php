<?php $this->load->view('site/includes/head'); ?>

<div class="main">
	<div class="content-form">
		<a href="<?php echo site_url(); ?>" class="link">VOLTAR</a>
		<hr>
		<span class="row">&nbsp;</span>
		<span class="row">&nbsp;</span>

		<main class="row">
			<form action="" class="js-form-validate">
				<fiedlset class="row">
					<legend>
						<strong>VALIDAÇÃO E MASCARAS PARA FORMULÁRIOS</strong>
					</legend>
					
					<span class="row">&nbsp;</span>
					<span class="row">&nbsp;</span>
					<label class="row">
						<span>NOME</span>
						<input type="text" value="" placeholder="Ex.: Seu nome" class="input" required="required">
					</label>
					
					<span class="row">&nbsp;</span>
					<label class="row">
						<span>CPF</span>
						<input type="text" value="" placeholder="Ex.: 999.999.999-99" class="input" required="required" data-mask-type="cpf">
					</label>

					<span class="row">&nbsp;</span>
					<label class="row">
						<span>Telefone</span>
						<input type="tel" value="" placeholder="Ex.: (99) 99999-9999" class="input" required="required" data-mask-type="phone">
					</label>

					<span class="row">&nbsp;</span>
					<label class="row">
						<span>DATA</span>
						<input type="text" value="" placeholder="Ex.: 99/99/9999" class="input" required="required" data-mask-type="date">
					</label>


					<span class="row">&nbsp;</span>
					<label class="row" id="parsley-container-select">
						<span>Estado</span>
						<div class="select-mask">
							<select name="" id="" required="required" class="input select" data-parsley-errors-container="#parsley-container-select">
								<option value="">Estado</option>
								<option value="etado-1"> estado 1 </option>
								<option value="etado-2"> estado 2 </option>
								<option value="etado-3"> estado 3 </option>
								<option value="etado-4"> estado 4 </option>
							</select>
						</div>
					</label>
				</fiedlset>

				<?php /* INPUT STEPPER -------------------------------------------------------------------------------- */ ?>
				<span class="row">&nbsp;</span>
				<span class="row">&nbsp;</span>
				<span class="row">&nbsp;</span><hr>
				<fieldset class="row">
					<legend>
						<strong>UTILIZANDO O PLUGIN INPUT-STEPPER</strong>
					</legend>
					<span class="row">&nbsp;</span>
					<label class="row">
						<div class="input-stepper">
							<input type="text" class="input">
							<button class="input-stepper-decrease" data-input-stepper-decrease>-</button>
							<button class="input-stepper-increase" data-input-stepper-increase>+</button>
						</div>
					</label>
				</fieldset>

				<?php /* RADIO CSS STYLE ------------------------------------------------------------------------------ */ ?>
				<span class="row">&nbsp;</span><hr>
				<fieldset class="row container-radio">
					<legend>
						<strong>RADIO BUTTON ESTILIZADO APENAS COM CSS</strong> <br> <br> <br>
					</legend>

					<input type="radio" required name="rb" id="rb1" />
					<label for="rb1">Check this</label>

					<input type="radio" name="rb" id="rb2" />
					<label for="rb2">... or this...</label>

					<input type="radio" name="rb" id="rb3" />
					<label for="rb3">or maybe this</label>
				</fieldset>

				<?php /* CHECKBOX CSS STYLE --------------------------------------------------------------------------- */ ?>
				<span class="row">&nbsp;</span><hr>
				<fiedlset class="row container-checkbox">
					<legend>
						<strong>CHECKBOX ESTILIZADO APENAS COM CSS</strong> <br> <br> <br>
					</legend>

					<input type="checkbox" required name="cb" id="cb1" />
					<label for="cb1">Check this</label>

					<input type="checkbox" name="cb" id="cb2" />
					<label for="cb2">... and this...</label>

					<input type="checkbox" name="cb" id="cb3" />
					<label for="cb3">and maybe this</label>
				</fiedlset>

				<?php /* SUBMIT --------------------------------------------------------------------------------------- */ ?>
				<span class="row">&nbsp;</span>
				<span class="row">&nbsp;</span>
				<span class="row">&nbsp;</span>
				<label class="row">
						<input type="submit" value="enviar" class="input submit">
				</label>
			</form>
		</main>


		<div class="row center-container">
			<br><br><br><br><hr>

			<?php // WT.AG ------------------------ ?>
			<div id="logo-wt-container"></div>

			<br><br><br><br>
		</div>
	</div>
</div>

<?php $this->load->view('site/includes/footer'); ?>