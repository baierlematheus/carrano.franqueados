<?php
$this->load->view('site/includes/head');
$this->load->view('site/includes/header');
?>
<?php echo Modules::run("banner"); ?>
<?php echo Modules::run("about"); ?>
<?php echo Modules::run("differentials_photos"); ?>

<section class="comunicacao" >
	
	<?php echo Modules::run("differentials_icons"); ?>
	<?php echo Modules::run("testimonials"); ?>
	
</section>

<?php echo Modules::run("communication"); ?>

<section class="eventos">
	
	<div class="content bx-eventos" id="eventos">
		<h2><?php echo lang('tit_eventos'); ?></h2>

		<div class="list-eventos">
			<?php
			foreach ($events as $event) {

				$ini = explode(' ', $event['initial_date']);
				$end = explode(' ', $event['end_date']);

				

				$s_ini = explode('-', $ini[0]);
				$s_end = explode('-', $end[0]);

				if($s_ini[1] == $s_end[1]){

					if(site_lang() == 'pt'){
						$data = $s_ini[2] . ' a ' . $s_end[2] . ' de ' . lang(mesescrito($s_ini[1])) . ' de ' . $s_ini[0];
					}

					if(site_lang() == 'es'){
						$data = $s_ini[2] . '-' . $s_end[2] . ' de ' . lang(mesescrito($s_ini[1])) . ' de ' . $s_ini[0];
					}

					if(site_lang() == 'en'){
						$data = lang(mesescrito($s_ini[1])) . ' ' . $s_ini[2] . '-' . $s_end[2] . ', ' . $s_ini[0];
					}

				}


				if($s_ini[1] != $s_end[1]){

					if(site_lang() != 'en'){
						$data = $s_ini[2] . ' de ' . lang(mesescrito($s_ini[1])) . ' a ' . $s_end[2] . ' de ' . lang(mesescrito($s_end[1])) . ' de ' . $s_ini[0];
					}

					if(site_lang() == 'en'){
						$data = lang(mesescrito($s_ini[1])) . ' ' . $s_ini[2] . ' to ' . lang(mesescrito($s_end[1])) . ' ' . $s_end[2] . ', ' . $s_ini[0];
					}
				}

				
				?>

				<div class="box">
					<img src="<?php echo site_url("graph/event/" . $event['img']); ?>" />
					<div class="data"><?php echo $data; ?></div>
					<div class="local"><?php echo $event['name'] . ' - ' . $event['place']; ?></div>
				</div>

				<?php
			}
			?>
		</div>
	</div>
	
	<?php echo Modules::run("franchisees"); ?>
	
</section>

<?php $this->load->view('site/includes/footer'); ?>