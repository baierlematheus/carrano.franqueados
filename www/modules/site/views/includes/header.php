<header class="header" id="topo-desktop">
	<!-- CABEÇALHO -->
	<div class="content">
		<a class="logo" href="<?php echo site_url(); ?>" >
			
			<img src="<?php echo img_path() . 'img/logo.png'; ?>" class="img-logo">
			<img src="<?php echo img_path() . 'img/logo_scroll.png'; ?>" class="img-logo-scroll">
		</a>

		<ul class="menu">
				<li>
					<a data-href="sobre" class="js-menu scroll-sobre"><?php echo lang('menu_sobre'); ?></a>
				</li>
				<li>
					<a data-href="diferenciais" class="js-menu scroll-diferenciais" ><?php echo lang('menu_diferenciais'); ?></a>
				</li>
				<li>
					<a data-href="comunicacao" class="js-menu scroll-comunicacao" ><?php echo lang('menu_comunicacao'); ?></a>
				</li>
				<li>
					<a data-href="eventos" class="js-menu scroll-eventos" ><?php echo lang('menu_eventos'); ?></a>
				</li>

			</ul>
			
		 <div class="m-right"> 
			<a data-href="contato" class="js-menu scroll-contato bt-seja-franqueado"><?php echo lang('bt_franqueado'); ?></a>

			<span class="lang-sel"><?php echo site_lang(); ?></span>	

			<?php echo Modules::run("content_languages") ?>
		</div> 
	</div>
</header>

<header class="header" id="topo-mobile">
	<!-- CABEÇALHO -->
	<div class="content">
		<a class="logo" href="<?php echo site_url(); ?>" >
			<img src="<?php echo img_path() . 'img/logo.png'; ?>" class="img-logo">
		</a>
		<span class="bt-menu">
			<img src="<?php echo img_path() . 'img/open.png'; ?>" class="img-open">
			<img src="<?php echo img_path() . 'img/close.png'; ?>" class="img-close">
		</span>
		
		
	</div>

	<div class="box-menu">

			<ul class="menu">
				<li>
					<a data-href="sobre" class="js-menu scroll-sobre"><?php echo lang('menu_sobre'); ?></a>
				</li>
				<li>
					<a data-href="diferenciais" class="js-menu scroll-diferenciais" ><?php echo lang('menu_diferenciais'); ?></a>
				</li>
				<li>
					<a data-href="comunicacao" class="js-menu scroll-comunicacao" ><?php echo lang('menu_comunicacao'); ?></a>
				</li>
				<li>
					<a data-href="eventos" class="js-menu scroll-eventos" ><?php echo lang('menu_eventos'); ?></a>
				</li>
			</ul>

			<div class="redes-sociais">

              <a href="https://www.facebook.com/carrano.oficial" target="_blank" class="facebook">
                <img src="<?php echo img_path() . 'img/facebook.png'; ?>" class="img-facebook" />
                <span>Facebook</span>
              </a>

              <a href="https://www.instagram.com/carranooficial/" target="_blank">
                <img src="<?php echo img_path() . 'img/instagram.png'; ?>" class="instagram" />
                <span>Instagram</span>
              </a>

              
            </div>

            <div class="box-languages">
            	<a href="<?php echo base_url() . 'pt'; ?>" <?php echo (site_lang() == 'pt') ? 'class="sel"' : ''; ?>>PT</a>
            	<a href="<?php echo base_url() . 'en'; ?>" <?php echo (site_lang() == 'en') ? 'class="sel"' : ''; ?>>EN</a>
            	<a href="<?php echo base_url() . 'es'; ?>" <?php echo (site_lang() == 'es') ? 'class="sel"' : ''; ?>>ES</a>
	        </div>
	

		<div data-href="contato" class="js-menu scroll-contato bt-seja-franqueado"><?php echo lang('bt_franqueado'); ?></div>
	</div>
</header>