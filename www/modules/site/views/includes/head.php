<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?php echo Modules::run('seo'); ?>

    <?php echo Modules::run('favicon'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="<?php echo isset($canonical_url) && $canonical_url ? $canonical_url : current_url(); ?>" />

    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="<?php echo site_url(); ?>" />
    <meta property="og:site_name" content="WT Agência" />
    <meta name='theme-color' content='#ffffff'/>

    <meta name="format-detection" content="telephone=no">
   <?php
        Assets::add_js('global.min.js'); 
        Assets::add_css('global.css');
    ?>
    <script type="text/javascript">

        var lang_url = '<?php echo lang_url() ?>';
        var site_url = '<?php echo site_url() ?>';
        var base_url = '<?php echo base_url() ?>';

    </script>

    <?php
        echo Assets::css();
        
        $this->load->library('user_agent');
        echo $this->settings_lib->item('analytics.code'); //Trocar mais tarde pelo Modules::run()
    ?>
    
</head>
<body class="<?php echo $this->agent->is_mobile() ? 'mobile' : '' ?> <?php echo site_lang(); ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDJ5LVX";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->