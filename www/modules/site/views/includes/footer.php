     	<footer class="footer" id="contato">
     		<div class="content">
     			<div class="top">
             <a href="https://www.carrano.com.br" class="logo-rodape" target="_blank">
              <img src="<?php echo img_path() . 'img/logo.png'; ?>" alt="Carrano" title="Carrano"/>
            </a>


            <ul class="menu">
             <li>
          <a data-href="sobre" class="js-menu scroll-sobre"><?php echo lang('menu_sobre'); ?></a>
        </li>
        <li>
          <a data-href="diferenciais" class="js-menu scroll-diferenciais" ><?php echo lang('menu_diferenciais'); ?></a>
        </li>
        <li>
          <a data-href="comunicacao" class="js-menu scroll-comunicacao" ><?php echo lang('menu_comunicacao'); ?></a>
        </li>
        <li>
          <a data-href="eventos" class="js-menu scroll-eventos" ><?php echo lang('menu_eventos'); ?></a>
        </li>
            </ul>

          </div>

          <div class="bottom">

            <div class="redes-sociais">

              <a href="https://www.facebook.com/carrano.oficial" target="_blank" class="facebook">
                <img src="<?php echo img_path() . 'img/facebook.png'; ?>" class="img-facebook" />
                <span>Facebook</span>
              </a>

              <a href="https://www.instagram.com/carranooficial/" target="_blank">
                <img src="<?php echo img_path() . 'img/instagram.png'; ?>" class="instagram" />
                <span>Instagram</span>
              </a>

              
            </div>



            <div id="logo-wt-container"></div>

          </div>
        </div>
      </footer>
      <?php echo Assets::js(); ?>
      <?php if ($message = $this->session->flashdata('message')){ ?>
        <script type="text/javascript">alert('<?php echo lang($message); ?>')</script>
      <?php } ?>
    </body>
    </html>