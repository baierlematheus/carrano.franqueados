<?php defined('BASEPATH') || exit('No direct script access allowed');

class Communication_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Imagem')
			->setFieldHelper('<b>Dimensões:</b> 952x488 <br> <b>Tamanho máximo:</b> 2mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(2000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');

			$this->crud->addField('char')
			->setFieldName('title_pt')
			->setLabel('Título Português');	

			$this->crud->addField('text')
			->setFieldName('text_pt')
			->setLabel('Texto Português');

			$this->crud->addField('char')
			->setFieldName('title_en')
			->hideFrom('list')
			->setLabel('Título Inglês');	

			$this->crud->addField('text')
			->setFieldName('text_en')
			->hideFrom('list')
			->setLabel('Texto Inglês');	

			$this->crud->addField('char')
			->setFieldName('title_es')
			->hideFrom('list')
			->setLabel('Título Espanhol');	

			$this->crud->addField('text')
			->setFieldName('text_es')
			->hideFrom('list')
			->setLabel('Texto Espanhol');

			$this->wtche->form->addField('number')
			->setFieldName('order')
			->setLabel('Ordem')
			->setValidationRules('required')
			->setInlineEdit();

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	function getCommunications(){

		return $this->db->select('c.*, ci.file_name as img')
		->from('communication c')
		->join('communication_image ci', 'ci.communication_id = c.id', 'left')
		->where('c.active', 1)
		->order_by('c.order', 'ASC')
		->get()
		->result_array();

	}

}