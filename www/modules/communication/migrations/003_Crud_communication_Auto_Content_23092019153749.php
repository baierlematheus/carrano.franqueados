<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019153749 

class Migration_Crud_Communication_Auto_content_23092019153749 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('communication', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"order":{"type":"INT","constraint":11,"null":false},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('communication', $key);
		}
	
		
	}
	
}