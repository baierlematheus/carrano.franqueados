<?php defined('BASEPATH') || exit('No direct script access allowed');

class Communication extends Front_Controller
{
    public function index() {

		$this->load->model('communication_model');
		$data['communications'] = $this->communication_model->getCommunications();

		$this->load->view(__FUNCTION__, $data);
	}
}