<section class="blocos" id="comunicacao">

	<?php
	foreach ($communications as $communication) {
		?>

		<div class="line">
			<img src="<?php echo site_url("graph/communication/" . $communication['img']); ?>" class="img-principal" />
			<div class="conteudo">
				<h4><?php echo $communication['title_' . site_lang()]; ?></h4>
				<div class="texto"><?php echo nl2br($communication['text_' . site_lang()] ); ?></div>

				<?php
				if ($communication === end($communications)) {
					echo Modules::run("buttons");
				}		
				?>
				

			</div>
		</div>
		<?php
	}		
	?>

</section>