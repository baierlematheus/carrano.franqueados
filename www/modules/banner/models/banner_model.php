<?php defined('BASEPATH') || exit('No direct script access allowed');

class Banner_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->wtche->form->addField('number')
			->setFieldName('order')
			->setLabel('Ordem')
			->setValidationRules('required')
			->setInlineEdit();

			$this->crud->addField('char')
			->setFieldName('link')
			->setLabel('Link')
			->setFieldHelper('Exemplo: http://www.google.com.br')
			->hideFrom('list');

			$array = array(0 => 'Não', 1 => 'Sim');

			$this->wtche->form->addField('select')
			->setFieldName('window')
			->setItemsArray($array)
			->setLabel('Abrir em nova janela?')
			->hideFrom('list');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Imagem Desktop')
			->setFieldHelper('<b>Dimensões:</b> 1920x967 <br> <b>Tamanho máximo:</b> 2mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(2000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image_mobile')
			->setLabel('Imagem Mobile')
			->setFieldHelper('<b>Dimensões:</b> 768x1264 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(1000)
			->setAllowedTypes('jpg')
			->setValidationRules('required')
			->hideFrom('list');

			$this->wtche->form->addField('select')
			->setFieldName('languages')
			->setRelationMultiple($this, 'id', 'content_languages', 'name');

			$this->crud->addField('text')
			->setFieldName('text_pt')
			->hideFrom('list')
			->setLabel('Texto Português');	

			$this->crud->addField('text')
			->setFieldName('text_en')
			->hideFrom('list')
			->setLabel('Texto Inglês');		

			$this->crud->addField('text')
			->setFieldName('text_es')
			->hideFrom('list')
			->setLabel('Texto Espanhol');	

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	function getBanners($language){

		return $this->db->select('b.*, bi.file_name as desktop, bm.file_name as mobile')
		->from('banner b')
		->join('banner_image bi', 'bi.banner_id = b.id', 'left')
		->join('banner_image_mobile bm', 'bm.banner_id = b.id', 'left')
		->join('banner_content_languages bcl', 'b.id = bcl.banner_id')
		->join('content_languages cl', 'cl.id = bcl.content_languages_id AND cl.key = "' . $language . '"')
		->where('b.active', 1)
		->order_by('b.order', 'ASC')
		->get()
		->result_array();

	}

}