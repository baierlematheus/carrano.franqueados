<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019100143 

class Migration_Crud_Banner_Auto_content_23092019100143 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"order":{"type":"INT","constraint":11,"null":false},"link":{"type":"VARCHAR","constraint":255,"null":true},"window":{"type":"ENUM(\"0\",\"1\")","null":true},"text_pt":{"type":"LONGTEXT","null":true},"text_en":{"type":"LONGTEXT","null":true},"text_es":{"type":"LONGTEXT","null":true},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('banner', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'banner_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('banner_image');

				$this->db->query('ALTER TABLE ' . 'banner_image' . '
					ADD CONSTRAINT `' . md5('banner_image' . 'banner' . 'banner_id') . '`
					FOREIGN KEY (' . 'banner_id' . ')
					REFERENCES `' . 'banner' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'banner_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('banner_image_mobile');

				$this->db->query('ALTER TABLE ' . 'banner_image_mobile' . '
					ADD CONSTRAINT `' . md5('banner_image_mobile' . 'banner' . 'banner_id') . '`
					FOREIGN KEY (' . 'banner_id' . ')
					REFERENCES `' . 'banner' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			

$this->load->dbforge();

$fields = array(
		          	'banner_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
					),
					'content_languages_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->create_table('banner_content_languages');

				$this->db->query('ALTER TABLE ' . 'banner_content_languages' . '
					ADD CONSTRAINT `' . md5('banner_content_languages' . '_' . 'languages' . '_' . 'banner_id') . '`
					FOREIGN KEY (' . 'banner_id' . ')
					REFERENCES `' . 'banner' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');

				$this->db->query('ALTER TABLE ' . 'banner_content_languages' . '
					ADD CONSTRAINT `' . md5('banner_content_languages' . '_' . 'languages' . '_' . 'content_languages_id') . '`
					FOREIGN KEY (' . 'content_languages_id' . ')
					REFERENCES `' . 'content_languages' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');


	}

	public function down()
	{
		
		$this->fields = json_decode('{"order":{"type":"INT","constraint":11,"null":false},"link":{"type":"VARCHAR","constraint":255,"null":true},"window":{"type":"ENUM(\"0\",\"1\")","null":true},"text_pt":{"type":"LONGTEXT","null":true},"text_en":{"type":"LONGTEXT","null":true},"text_es":{"type":"LONGTEXT","null":true},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('banner', $key);
		}
	
		
				
			


				
			




	}
	
}