<section class="banner" >
	<div class="itens">
		
		<?php
		foreach ($banners as $banner) {
			$target = ($banner['window'] == 0) ? '' : 'target="_blank"';
			echo ($banner['link'] != '') ? '<a href="'. $banner['link'] .'" ' . $target . '>' : '';
		?>
		<div class="item">
			<img src="<?php echo site_url("graph/banner_desktop/" . $banner['desktop']); ?>" class="img-desktop">
			<img src="<?php echo site_url("graph/banner_mobile/" . $banner['mobile']); ?>" class="img-mobile">
			<h1 class="texto"><?php echo nl2br($banner['text_'.site_lang()]); ?></h1>
		</div>
		<?php
			echo ($banner['link'] != '') ? '</a>' : '';
		}
		?>
	</div>


	<div data-href="contato" class="js-menu scroll-contato bt-seja-franqueado"><?php echo lang('bt_franqueado'); ?></div>
</section>
