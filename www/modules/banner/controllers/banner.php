<?php defined('BASEPATH') || exit('No direct script access allowed');

class Banner extends Front_Controller
{
    public function index() {
    	
		$this->load->model('banner_model');
		$data['banners'] = $this->banner_model->getBanners(site_lang());

		$this->load->view(__FUNCTION__, $data);
	}
}