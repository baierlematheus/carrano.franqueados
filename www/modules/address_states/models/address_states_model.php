<?php defined('BASEPATH') || exit('No direct script access allowed');

class Address_states_model extends WT_Module_Model {
	
	public function setupCrud() {
		
		$this->wtche->form->addField('char')
		->setFieldName('initials')
		->setLabel('Sigla')
		->setMaxLength(2)
		->setValidationRules('required');

		$this->wtche->form->addField('char')
		->setFieldName('name')
		->setLabel('Nome')
		->setMaxLength(100)
		->setValidationRules('required');

	}


	function getStates(){

		return $this->db->select('s.*')
		->from('address_states s')
		->get()
		->result_array();
	}

	

}