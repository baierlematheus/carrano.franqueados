<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Address_states_Fields extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'initials' => array(
			'type' => 'VARCHAR',
			'constraint' => 2
		),
		'name' => array(
			'type' => 'VARCHAR',
			'constraint' => 100
		),
	);

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = 'address_states';

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}