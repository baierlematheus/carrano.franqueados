<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_Default_Address_States extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = get_module_name(__FILE__);

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->db->insert_batch($this->table_name, array(
			array('initials' => 'AC', 'name' => 'Acre'),
			array('initials' => 'AL', 'name' => 'Alagoas'),
			array('initials' => 'AM', 'name' => 'Amazonas'),
			array('initials' => 'AP', 'name' => 'Amapá'),
			array('initials' => 'BA', 'name' => 'Bahia'),
			array('initials' => 'CE', 'name' => 'Ceará'),
			array('initials' => 'DF', 'name' => 'Distrito Federal'),
			array('initials' => 'ES', 'name' => 'Espírito Santo'),
			array('initials' => 'GO', 'name' => 'Goiás'),
			array('initials' => 'MA', 'name' => 'Maranhão'),
			array('initials' => 'MG', 'name' => 'Minas Gerais'),
			array('initials' => 'MS', 'name' => 'Mato Grosso do Sul'),
			array('initials' => 'MT', 'name' => 'Mato Grosso'),
			array('initials' => 'PA', 'name' => 'Pará'),
			array('initials' => 'PB', 'name' => 'Paraíba'),
			array('initials' => 'PE', 'name' => 'Pernambuco'),
			array('initials' => 'PI', 'name' => 'Piauí'),
			array('initials' => 'PR', 'name' => 'Paraná'),
			array('initials' => 'RJ', 'name' => 'Rio de Janeiro'),
			array('initials' => 'RN', 'name' => 'Rio Grande do Norte'),
			array('initials' => 'RO', 'name' => 'Rondônia'),
			array('initials' => 'RR', 'name' => 'Roraima'),
			array('initials' => 'RS', 'name' => 'Rio Grande do Sul'),
			array('initials' => 'SC', 'name' => 'Santa Catarina'),
			array('initials' => 'SE', 'name' => 'Sergipe'),
			array('initials' => 'SP', 'name' => 'São Paulo'),
			array('initials' => 'TO', 'name' => 'Tocantins'),
		));
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		$this->db->truncate($this->table_name); 
	}

	//--------------------------------------------------------------------

}