<?php defined('BASEPATH') || exit('No direct script access allowed');

class Content_languages extends Front_Controller
{
    public function index() {

   		$this->load->model('content_languages_model');

    	$data['languages'] = $this->content_languages_model->get_languages( site_lang() );

		$this->load->view(__FUNCTION__, $data);
	}
}