<?php defined('BASEPATH') || exit('No direct script access allowed');

class Content_languages_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			//Content CRUD

		} elseif ($context == 'settings') {
			
			$this->crud->addField('char')
			->setFieldName('name')
			->setLabel('Nome')
			->setValidationRules('required');

			$this->crud->addField('char')
			->setFieldName('key')
			->setLabel('Chave')
			->setValidationRules('required');

		}

	}


	public function get_languages($language){

		return $this->db->select('*')
		->from('content_languages')
		->where("key != '" . $language . "'")
		->get()
		->result_array();

	}

}