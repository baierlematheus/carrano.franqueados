<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019093330 

class Migration_Crud_Content_languages_Auto_settings_23092019093330 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"key":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		$this->dbforge->add_column('content_languages', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"key":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('content_languages', $key);
		}
	
		
	}
	
}