<?php defined('BASEPATH') || exit('No direct script access allowed');

class address_cities extends Base_Controller {

	public function get_cities($state_id) {

		$this->load->model('address_cities_model');

		$cities = $this->address_cities_model->getCities($state_id);

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($cities));

	}

}