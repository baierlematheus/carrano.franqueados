<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Address_cities_Fields extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'address_states_id' => array(
			'type' => 'INT',
			'constraint' => 10,
            'unsigned' => TRUE
		),
		'name' => array(
			'type' => 'VARCHAR',
			'constraint' => 100
		),
	);

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = 'address_cities';

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}