<?php defined('BASEPATH') || exit('No direct script access allowed');

class Address_cities_model extends WT_Module_Model {
	
	public function setupCrud() {
		
		$this->wtche->form->addField('select')
			->setFieldName('address_states_id')
			->setRelation('id', 'address_states', 'name')
			->setLabel('Estado')
			->setValidationRules('required');

		$this->wtche->form->addField('char')
			->setFieldName('name')
			->setLabel('Nome')
			->setMaxLength(100)
			->setValidationRules('required');

	}

	public function getCities($state_id) {

		return $this->where('address_states_id', $state_id)->find_all();

	}

}