<?php defined('BASEPATH') || exit('No direct script access allowed');

class Franchisees extends Front_Controller
{
	public function index() {

		$this->load->model('address_states/address_states_model');
		$this->load->model('address_contries/address_contries_model');

		$order = 'name_' . site_lang();

		$data['states'] = $this->address_states_model->getStates();
		$data['countries'] = $this->address_contries_model->getCountries($order);

		$this->load->view(__FUNCTION__, $data);
	}

	public function send() {

		$this->load->model('franchisees_model');

		$data = $this->input->post();
		$captcha = $data['g-recaptcha-response'];

		$recaptcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdaGLsUAAAAANxT_leeCA7WCZx2vR502Y5yai7D&response={$captcha}");
		$response = json_decode($recaptcha);
	

		if($response->success == true && $response->score > 0.5){

			unset($data['g-recaptcha-response']);

			if ($this->franchisees_model->validateFor('create', 'content')) {
				if ($franchisees_id = $this->franchisees_model->insert($data)) {

					$data = $this->franchisees_model->getById($franchisees_id);

					$this->session->set_flashdata('message', lang('msg_ok'));

				    //Envia o email
					$this->load->library('emailer/emailer');
					$this->load->library('settings/settings_lib');

					$email_data['Nome'] = $data->name;
					$email_data['E-mail'] = $data->email;
					$email_data['Telefone'] = $data->phone;
					$email_data['Possui Empresa'] = ($data->company == 1) ? 'Sim' : 'Não';

					if($data->cnpj != ''){
						$email_data['CNPJ'] = $data->cnpj;
					}

					if($data->state != ''){
						$email_data['Estado'] = $data->state;
					}

					if($data->city != ''){
						$email_data['Cidade'] = $data->city;
					}


					if($data->pais != ''){
						$email_data['País'] = $data->pais;
					}


					if($data->city_foreign != ''){
						$email_data['Cidade'] = $data->city_foreign;
					}

					
					
					$email_data['Data do Contato'] = date("d/m/Y H:i:s", strtotime($data->created_on) );

					$email_data['Mensagem'] = $data->message;

					$settings = $this->settings_lib->find_all();


					$subject = "Carrano Franqueados - Contato";
					$message = $this->load->view('emailer/email/_generic', array('email_data' => $email_data), TRUE);



					$data = array(
						'to'	=> 'vendas@carrano.com.br',
						'reply_to' => $data->email,
						'subject'	=> $subject,
						'message'	=> $message
					);


					if ($this->emailer->send($data)) {
						$this->session->set_flashdata('message', 'msg_ok');
					} else {
						$this->session->set_flashdata('message', 'msg_error');
					}


					redirect(site_url() . site_lang() . '/#contato');

				} else {
					$this->session->set_flashdata('message', 'msg_error');
				}
			} else {
				$this->session->set_flashdata('message', 'msg_error');
			}

		}else{
			$this->session->set_flashdata('message', 'msg_error');
		}

		redirect(site_url() . site_lang() . '/#contato');
		

	}
}