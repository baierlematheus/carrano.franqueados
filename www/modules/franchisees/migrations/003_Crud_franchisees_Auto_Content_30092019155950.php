<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 30092019155950 

class Migration_Crud_Franchisees_Auto_content_30092019155950 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"country":{"type":"INT","constraint":10,"unsigned":true,"null":true},"city_foreign":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		$this->dbforge->add_column('franchisees', $this->fields);
	
		
					$this->db->query('ALTER TABLE ' . 'franchisees' . '
						ADD CONSTRAINT `' . '20def32c277e1d4c2574e42b5bf926a9' . '`
						FOREIGN KEY (' . 'country' . ')
						REFERENCES `' . 'address_contries' . '` (' . 'id' . ')
						ON DELETE CASCADE
						ON UPDATE CASCADE');
				


	}

	public function down()
	{
		
		$this->fields = json_decode('{"country":{"type":"INT","constraint":10,"unsigned":true,"null":true},"city_foreign":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('franchisees', $key);
		}
	
		

				


	}
	
}