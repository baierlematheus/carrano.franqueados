<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 24092019110148 

class Migration_Crud_Franchisees_Auto_content_24092019110148 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"email":{"type":"VARCHAR","constraint":255,"null":false},"phone":{"type":"VARCHAR","constraint":255,"null":false},"company":{"type":"TINYINT","constraint":1,"null":true},"cnpj":{"type":"VARCHAR","constraint":255,"null":true},"state":{"type":"INT","constraint":10,"unsigned":true,"null":false},"city":{"type":"INT","constraint":10,"unsigned":true,"null":false},"message":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		$this->dbforge->add_column('franchisees', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":false},"email":{"type":"VARCHAR","constraint":255,"null":false},"phone":{"type":"VARCHAR","constraint":255,"null":false},"company":{"type":"TINYINT","constraint":1,"null":true},"cnpj":{"type":"VARCHAR","constraint":255,"null":true},"state":{"type":"INT","constraint":10,"unsigned":true,"null":false},"city":{"type":"INT","constraint":10,"unsigned":true,"null":false},"message":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('franchisees', $key);
		}
	
		
	}
	
}