<script src="https://www.google.com/recaptcha/api.js?render=6LdaGLsUAAAAAGY9JAuF9-g5bM-6YblvfOhJu3vx"></script>
<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('6LdaGLsUAAAAAGY9JAuF9-g5bM-6YblvfOhJu3vx', {action: 'homepage'}).then(function(token) {
			$('#g-recaptcha-response').val(token);
		});
	});
</script>
<div class="content contato" id="contato">
	<div class="left">
		<h2><?php echo lang('tit_contato'); ?></h2>
		<div class="texto"><?php echo lang('desc_contato'); ?></div>
	</div>

	<form method="post" action="<?php echo base_url() . site_lang() . '/franchisees/send'; ?>" class="right js-form-validate">
		<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" value=""/>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
		<div class="half">
			<label><?php echo lang('cc_nome'); ?><i>*</i></label>
			<input type="text" name="name" required/>	
		</div>
		<div class="half">
			<label><?php echo lang('cc_email'); ?><i>*</i></label>
			<input type="text" name="email" required/>	
		</div>


		<div class="half">
			<label><?php echo lang('cc_phone'); ?><i>*</i></label>
			<input type="text" name="phone" data-mask-type="phone" required/>	
		</div>
		<div class="half check">
			<div class="left">
				<label><?php echo lang('cc_loja'); ?><i>*</i></label>

				<div class="inputs">
					<div class="container-checkbox">
						<input type="radio" required name="company" id="rdo_yes" value="1" >
						<label for="rdo_yes"><?php echo lang('cc_sim'); ?></label>
					</div>

					<div class="container-checkbox">
						<input type="radio" required name="company" id="rdo_no" value="0" >
						<label for="rdo_no"><?php echo lang('cc_nao'); ?></label>
					</div>
				</div>
			</div>

			<?php

			if( site_lang() == 'pt'){
				?>

				<div class="right disabled cnpj">
					<label>CNPJ<i>*</i></label>
					<input type="text" name="cnpj" data-mask-type=cnpj disabled="disabled"/>	
				</div>

				<?php	
			}
			?>

		</div>



		<?php

		if( site_lang() != 'pt'){
			?>
			<div class="half">
				<label><?php echo lang('cc_pais'); ?><i>*</i></label>
				<select name="country" required>
					<option value=""><?php echo lang('cc_sel_pais'); ?></option>
					<?php
					foreach ($countries as $country) {
						?>
						<option value="<?php echo $country['id']; ?>"><?php echo $country['name_' . site_lang()]; ?></option>
						<?php	
					}
					?>

				</select>
			</div>
			<div class="half">
				<label><?php echo lang('cc_cidade'); ?><i>*</i></label>
				<input type="text" name="city_foreign" required/>	
			</div>

			<?php
		}else{ 
			?>



			<div class="half">
				<label><?php echo lang('cc_estado'); ?><i>*</i></label>
				<select name="state" required>
					<option value=""><?php echo lang('cc_sel_estado'); ?></option>
					<?php
					foreach ($states as $state) {
						?>
						<option value="<?php echo $state['id']; ?>"><?php echo $state['name']; ?></option>
						<?php	
					}
					?>

				</select>
			</div>
			<div class="half box-city disabled">
				<label><?php echo lang('cc_cidade'); ?><i>*</i></label>
				<select name="city" required disabled="disabled" data-place="<?php echo lang('cc_sel_cidade'); ?>">
					<option value=""><?php echo lang('cc_sel_cidade'); ?></option>
				</select>
			</div>

			<?php
		}
		?>

		<div class="full">
			<label><?php echo lang('cc_msg'); ?></label>
			<textarea name="message" required placeholder="<?php echo lang('cc_esc_msg'); ?>"></textarea>
		</div>

		<div class="half">

			<!-- <script type="text/javascript">
				var onloadCallback = function() {
					grecaptcha.render('html_element', {
						'sitekey' : '6LdxIroUAAAAABIQfJ50pkBNkdCsJeeMdPboKBf2'
					});
				};
			</script>
			<div id="html_element"></div>
			<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer> -->
			</script>
		</div>
		<div class="half">
			<button type="submit"><?php echo lang('cc_enviar'); ?></button>
		</div>
	</form>
</div>