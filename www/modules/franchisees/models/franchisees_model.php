<?php defined('BASEPATH') || exit('No direct script access allowed');

class Franchisees_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('char')
			->setFieldName('name')
			->setLabel('Nome')
			->setValidationRules('required');

			$this->crud->addField('char')
			->setFieldName('email')
			->setLabel('E-mail')
			->setValidationRules('required');

			$this->crud->addField('char')
			->setFieldName('phone')
			->setLabel('Telefone')
			->setValidationRules('required');

			$this->crud->addField('switcher')
			->setFieldName('company')
			->setLabel('Possui Empresa?')
			->setLabelOn('Sim')
			->setLabelOff('Não')
			->setValidationRules('required')
			->setInlineEdit()
			->setValidationRules('numeric');

			$this->crud->addField('char')
			->setFieldName('cnpj')
			->hideFrom('list')
			->setLabel('CNPJ');

			$this->crud->addField('select')
			->setFieldName('state')
			->setRelation('id', 'address_states', 'name')
			->setLabel('Estado')
			->hideFrom('list');

			$this->crud->addField('select')
			->setFieldName('city')
			->setRelation('id', 'address_cities', 'name')
			->setLabel('Cidade')
			->setDependency('state', 'address_states_id')
			->hideFrom('list');

			$this->crud->addField('select')
			->setFieldName('country')
			->setRelation('id', 'address_contries', 'name_pt')
			->setLabel('País')
			->hideFrom('list');

			$this->crud->addField('char')
			->setFieldName('city_foreign')
			->setLabel('Cidade')
			->hideFrom('list');

			$this->crud->addField('text')
			->setFieldName('message')
			->hideFrom('list')
			->setLabel('Mensagem');


		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	public function getById($id){

		return
		$this->db->select('f.*, ae.name as state, ac.name as city, co.name_pt as pais')
		->from('franchisees f')
		->join('address_states ae', 'ae.id = f.state', 'left')
		->join('address_cities ac', 'ac.id = f.city', 'left')
		->join('address_contries co', 'co.id = f.country', 'left')
		->where('f.id',$id)
		->get()->row();

	}

}