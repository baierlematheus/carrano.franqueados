<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 23092019153032 

class Migration_Crud_Buttons_Auto_content_23092019153032 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"link_pt":{"type":"VARCHAR","constraint":255,"null":true},"link_en":{"type":"VARCHAR","constraint":255,"null":true},"link_es":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		$this->dbforge->add_column('buttons', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'buttons_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('buttons_image_pt');

				$this->db->query('ALTER TABLE ' . 'buttons_image_pt' . '
					ADD CONSTRAINT `' . md5('buttons_image_pt' . 'buttons' . 'buttons_id') . '`
					FOREIGN KEY (' . 'buttons_id' . ')
					REFERENCES `' . 'buttons' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'buttons_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('buttons_image_en');

				$this->db->query('ALTER TABLE ' . 'buttons_image_en' . '
					ADD CONSTRAINT `' . md5('buttons_image_en' . 'buttons' . 'buttons_id') . '`
					FOREIGN KEY (' . 'buttons_id' . ')
					REFERENCES `' . 'buttons' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'buttons_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('buttons_image_es');

				$this->db->query('ALTER TABLE ' . 'buttons_image_es' . '
					ADD CONSTRAINT `' . md5('buttons_image_es' . 'buttons' . 'buttons_id') . '`
					FOREIGN KEY (' . 'buttons_id' . ')
					REFERENCES `' . 'buttons' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"link_pt":{"type":"VARCHAR","constraint":255,"null":true},"link_en":{"type":"VARCHAR","constraint":255,"null":true},"link_es":{"type":"VARCHAR","constraint":255,"null":true}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('buttons', $key);
		}
	
		
				
			


				
			


				
			


	}
	
}