<?php defined('BASEPATH') || exit('No direct script access allowed');

class Buttons extends Front_Controller
{
    public function index() {

		$this->load->model('buttons_model');
		$data['buttons'] = $this->buttons_model->getButtons();

		$this->load->view(__FUNCTION__, $data);
	}
}