<?php defined('BASEPATH') || exit('No direct script access allowed');

class Buttons_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('char')
			->setFieldName('link_pt')
			->setLabel('Link Português')
			->setFieldHelper('Exemplo: http://www.google.com.br')
			->hideFrom('list');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image_pt')
			->setLabel('Imagem Português')
			->setFieldHelper('<b>Dimensões:</b> 152x45 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(1000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');

			$this->crud->addField('char')
			->setFieldName('link_en')
			->setLabel('Link Inglês')
			->setFieldHelper('Exemplo: http://www.google.com.br')
			->hideFrom('list');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image_en')
			->setLabel('Imagem Inglês')
			->setFieldHelper('<b>Dimensões:</b> 152x45 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(1000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');


			$this->crud->addField('char')
			->setFieldName('link_es')
			->setLabel('Link Espanhol')
			->setFieldHelper('Exemplo: http://www.google.com.br')
			->hideFrom('list');

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image_es')
			->setLabel('Imagem Espanhol')
			->setFieldHelper('<b>Dimensões:</b> 152x45 <br> <b>Tamanho máximo:</b> 1mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(1000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');



		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}


	function getButtons(){

		return $this->db->select('b.*, bipt.file_name as img_pt, bien.file_name as img_en, bies.file_name as img_es')
		->from('buttons b')
		->join('buttons_image_pt bipt', 'bipt.buttons_id = b.id', 'left')
		->join('buttons_image_en bien', 'bien.buttons_id = b.id', 'left')
		->join('buttons_image_es bies', 'bies.buttons_id = b.id', 'left')
		->get()
		->result_array();

	}

}