<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 24092019081025 

class Migration_Crud_Events_Auto_content_24092019081025 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":true},"state":{"type":"INT","constraint":10,"unsigned":true,"null":false},"initial_date":{"type":"DATETIME"},"end_date":{"type":"DATETIME"},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('events', $this->fields);
	
		
				$this->load->dbforge();

				$fields = array(
		          	'id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'events_id' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('events_image');

				$this->db->query('ALTER TABLE ' . 'events_image' . '
					ADD CONSTRAINT `' . md5('events_image' . 'events' . 'events_id') . '`
					FOREIGN KEY (' . 'events_id' . ')
					REFERENCES `' . 'events' . '` (' . 'id' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			


	}

	public function down()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":true},"state":{"type":"INT","constraint":10,"unsigned":true,"null":false},"initial_date":{"type":"DATETIME"},"end_date":{"type":"DATETIME"},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('events', $key);
		}
	
		
				
			


	}
	
}