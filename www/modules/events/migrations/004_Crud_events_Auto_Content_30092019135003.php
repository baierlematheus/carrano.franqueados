<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 30092019135003 

class Migration_Crud_Events_Auto_content_30092019135003 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"place":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		$this->dbforge->add_column('events', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"place":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('events', $key);
		}
	
		
	}
	
}