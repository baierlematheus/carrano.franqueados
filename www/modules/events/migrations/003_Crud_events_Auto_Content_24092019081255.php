<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 24092019081255 

class Migration_Crud_Events_Auto_content_24092019081255 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"city":{"type":"INT","constraint":10,"unsigned":true,"null":false}}', true);
		$this->dbforge->add_column('events', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"city":{"type":"INT","constraint":10,"unsigned":true,"null":false}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('events', $key);
		}
	
		
	}
	
}