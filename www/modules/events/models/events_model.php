<?php defined('BASEPATH') || exit('No direct script access allowed');

class Events_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('file_upload')
			->setFieldName($this, 'image')
			->setLabel('Imagem')
			->setFieldHelper('<b>Dimensões:</b> 514x300 <br> <b>Tamanho máximo:</b> 2mb <br> <b>Formatos:</b> .jpg ')
			->setMaxSize(2000)
			->setValidationRules('required')
			->setAllowedTypes('jpg');

			$this->crud->addField('char')
			->setFieldName('name')
			->setLabel('Nome');

			// $this->crud->addField('select')
			// ->setFieldName('state')
			// ->setRelation('id', 'address_states', 'name')
			// ->setLabel('Estado')
			// ->setValidationRules('required');

			// $this->crud->addField('select')
			// ->setFieldName('city')
			// ->setRelation('id', 'address_cities', 'name')
			// ->setLabel('Cidade')
			// ->setDependency('state', 'address_states_id')
			// ->setValidationRules('required')
			// ->hideFrom('list');

			$this->crud->addField('char')
			->setFieldName('place')
			->setValidationRules('required')
			->setLabel('Local do Evento');

			$this->crud->addField('char')
			->setFieldName('initial_date')
			->setLabel('Data inicial')
			->setValidationRules('required')
			->setDateTimeField();

			$this->crud->addField('char')
			->setFieldName('end_date')
			->setLabel('Data final')
			->setValidationRules('required')
			->setDateTimeField();

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');

		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}

	function getEvents(){

		return $this->db->select('e.*, ei.file_name as img, ae.initials as uf, ac.name as city')
		->from('events e')
		->join('events_image ei', 'ei.events_id = e.id', 'left')
		->join('address_states ae', 'ae.id = e.state', 'left')
		->join('address_cities ac', 'ac.id = e.city', 'left')
		->where('e.end_date >= NOW()')
		->where('e.active', 1)
		->order_by('e.initial_date', 'ASC')
		->limit(6)
		->get()
		->result_array();

	}

}