<?php defined('BASEPATH') || exit('No direct script access allowed');

class Testimonials extends Front_Controller
{
    public function index() {

		$this->load->model('testimonials_model');
		$data['testimonials'] = $this->testimonials_model->getTestimonials();

		$this->load->view(__FUNCTION__, $data);
	}
}