<?php defined('BASEPATH') || exit('No direct script access allowed');

class Testimonials_model extends WT_Module_Model {
	
	public function setupCrud($context = null) {
		
		if ($context == 'content') {

			$this->crud->addField('text')
			->setFieldName('text_pt')
			->setValidationRules('required')
			->setLabel('Depoimento Português');

			$this->crud->addField('text')
			->setFieldName('text_en')
			->hideFrom('list')
			->setValidationRules('required')
			->setLabel('Depoimento Inglês');		

			$this->crud->addField('text')
			->setFieldName('text_es')
			->hideFrom('list')
			->setValidationRules('required')
			->setLabel('Depoimento Espanhol');

			$this->crud->addField('char')
			->setFieldName('name')
			->setValidationRules('required')
			->setLabel('Nome');	

			$this->crud->addField('char')
			->setFieldName('description')
			->setValidationRules('required')
			->setFieldHelper('Exemplo: Nome da loja - Cidade | UF')
			->setLabel('Descrição');	

			$this->crud->addField('switcher')
			->setFieldName('active')
			->setLabel('Ativo')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');


		} elseif ($context == 'settings') {
			
			//Settings CRUD

		}

	}


	function getTestimonials(){

		return $this->db->select('t.*')
		->from('testimonials t')
		->where('t.active', 1)
		->order_by('id','RANDOM')
		->limit(3)
		->get()
		->result_array();

	}

}