<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Testimonials_Permissions extends Migration
{

	/**
	 * Permissions to Migrate
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name' => 'Testimonials.Content.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Testimonials.Content.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Testimonials.Content.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Testimonials.Content.Delete',
			'description' => '',
			'status' => 'active',
		),
	);

	/**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $permissions_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name = 'testimonials';

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'id' => array(
			'type' => 'INT',
			'constraint' => 10,
            'unsigned' => TRUE,
			'auto_increment' => TRUE,
		),
		'created_on' => array(
			'type' => 'datetime',
			'default' => '0000-00-00 00:00:00',
		),
		'modified_on' => array(
			'type' => 'datetime',
			'default' => '0000-00-00 00:00:00',
		),
		'created_by' => array(
			'type' => 'INT',
			'constraint' => 10,
            'unsigned' => TRUE,
            'null' => TRUE,
		),
		'modified_by' => array(
			'type' => 'INT',
			'constraint' => 10,
            'unsigned' => TRUE,
            'null' => TRUE
		),
	);

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{

		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->permissions_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);

		$this->dbforge->add_field($this->fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_name);

		$this->load->model("{$this->table_name}/{$this->table_name}_model");
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->permissions_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->permissions_name, array('name' => $permission_value['name']));
		}

		$this->db->where('module', $this->table_name)->delete('schema_crud_version');

		$this->dbforge->drop_table($this->table_name);
	}

	//--------------------------------------------------------------------

}