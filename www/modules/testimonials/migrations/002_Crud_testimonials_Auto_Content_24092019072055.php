<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 24092019072055 

class Migration_Crud_Testimonials_Auto_content_24092019072055 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"text_en":{"type":"LONGTEXT","null":true},"text_es":{"type":"LONGTEXT","null":true},"description":{"type":"VARCHAR","constraint":255,"null":true},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		$this->dbforge->add_column('testimonials', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"name":{"type":"VARCHAR","constraint":255,"null":true},"text_pt":{"type":"LONGTEXT","null":true},"text_en":{"type":"LONGTEXT","null":true},"text_es":{"type":"LONGTEXT","null":true},"description":{"type":"VARCHAR","constraint":255,"null":true},"active":{"type":"TINYINT","constraint":1,"null":true,"default":1}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('testimonials', $key);
		}
	
		
	}
	
}