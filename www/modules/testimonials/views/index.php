<div class="content depoimentos">
	<div class="top">
		<h2><?php echo lang('tit_depoimentos'); ?></h2>

		<div class="desc">
			<?php echo lang('desc_depoimentos'); ?>
		</div>	
	</div>

	<div class="bx-depoimentos">

		<?php
		foreach ($testimonials as $testimonial) {
			?>

			<div class="box">
				<div class="texto"><?php echo nl2br($testimonial['text_' . site_lang()] ); ?></div>
				<span class="nome"><?php echo $testimonial['name']; ?></span>
				<div class="infos"><?php echo $testimonial['description']; ?></div>
			</div>
			<?php
		}		
		?>		
	</div>
</div>	