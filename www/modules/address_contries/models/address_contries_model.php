<?php defined('BASEPATH') || exit('No direct script access allowed');

class Address_contries_model extends WT_Module_Model {
	
	public function setupCrud() {
		
			$this->wtche->form->addField('char')
			->setFieldName('name_pt')
			->setLabel('Nome Português')
			->setValidationRules('required');

			$this->wtche->form->addField('char')
			->setFieldName('name_en')
			->setLabel('Nome Inglês')
			->setValidationRules('required');

			$this->wtche->form->addField('char')
			->setFieldName('name_es')
			->setLabel('Nome Espanhol')
			->setValidationRules('required');

			$this->wtche->form->addField('number')
			->setFieldName('code')
			->setLabel('Código')
			->setValidationRules('required');

			$this->wtche->form->addField('char')
			->setFieldName('iso')
			->setLabel('Iso')
			->setValidationRules('required');

			$this->wtche->form->addField('char')
			->setFieldName('iso3')
			->setLabel('Iso 3')
			->setValidationRules('required');

	}

	function getCountries($order){

		return $this->db->select('c.*')
		->from('address_contries c')
		->order_by($order, 'ASC')
		->get()
		->result_array();
	}

}