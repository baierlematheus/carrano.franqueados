<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: 30092019152608 

class Migration_Crud_Address_contries_Auto_content_30092019152608 extends Migration
{

	private $fields = array();

	public function up()
	{
		
		$this->fields = json_decode('{"name_pt":{"type":"VARCHAR","constraint":255,"null":false},"name_en":{"type":"VARCHAR","constraint":255,"null":false},"name_es":{"type":"VARCHAR","constraint":255,"null":false},"code":{"type":"INT","constraint":11,"null":false},"iso":{"type":"VARCHAR","constraint":255,"null":false},"iso3":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		$this->dbforge->add_column('address_contries', $this->fields);
	
		
	}

	public function down()
	{
		
		$this->fields = json_decode('{"name_pt":{"type":"VARCHAR","constraint":255,"null":false},"name_en":{"type":"VARCHAR","constraint":255,"null":false},"name_es":{"type":"VARCHAR","constraint":255,"null":false},"code":{"type":"INT","constraint":11,"null":false},"iso":{"type":"VARCHAR","constraint":255,"null":false},"iso3":{"type":"VARCHAR","constraint":255,"null":false}}', true);
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column('address_contries', $key);
		}
	
		
	}
	
}