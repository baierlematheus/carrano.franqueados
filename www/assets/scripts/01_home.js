function home(){

	//TRAZ CIDADES

	$('[name="state"]').change(function(){

		$('.box-city').addClass('disabled');
		$('[name="city"]').attr('disabled', true);
		$('[name="city"]').html('');
		$('[name="city"]').append('<option value="">' + $('[name="city"]').data('place')  + '</option>');

		$.ajax({
			url: base_url + 'address_cities/get_cities/' + $(this).val(), 
			success: function(result){
								
				$.each(result, function(i, item) {
   					$('[name="city"]').append('<option value="' + item.id + '">' + item.name + '</option>');
				});

				$('.box-city').removeClass('disabled');
				$('[name="city"]').removeAttr('disabled');

			}
		});

	});


	// EMPRESA FORM

	$('[name="company"]').click(function(){
		
		if( $(this).val() == 1 ){

			$('.cnpj').removeClass('disabled');
			$('[name="cnpj"]').removeAttr('disabled');
			$('[name="cnpj"]').attr("required", true);

		}else{
			$('.cnpj').addClass('disabled');
			$('[name="cnpj"]').attr("disabled", true);
			$('[name="cnpj"]').removeAttr('required');
			$('.cnpj ul.parsley-errors-list').removeClass('filled');
		}

	});


	// OPEN BOX LANG

	$('.lang-sel').click(function(){

		if( $('.box-languages').is(':visible') ){
			$('.box-languages').css('display','none');
		}else{
			$('.box-languages').css('display','flex');
		}
		
	});

	// OPEN BOX LANG

	//BANNER

	var owl_banner = $(".banner .itens");

	owl_banner.owlCarousel({
		navigation: false,
		autoPlay: true,
		pagination: true,
		navigationText: false,
		mouseDrag: true,
		touchDrag: true,
		singleItem: true
	});


	//BANNER


	// PASSA ETAPAS SEJA FRANQUEADO
	// $('.js-nav').click(function(){

		
	// 	var etapa = $(this).data('step');
	// 	$('.etapa').hide();
	// 	$('#' + etapa ).show();

	// });



	// RANGE ETAPA 3
	// $('input[type="range"]').rangeslider();





	// owl_brands = $(".carrousel-marcas");

	// owl_brands.owlCarousel({
	// 	navigation: false,
	// 	autoPlay: false,
	// 	pagination: false,
	// 	navigationText: false,
	// 	mouseDrag: false,
	// 	touchDrag: false,
	// 	singleItem: true
	// });

	// if (typeof brandsCarouselStartPosition != 'undefined') {
	// 	owl_brands.trigger('owl.jumpTo', brandsCarouselStartPosition);
	// }


	// $(".box-marcas .navigator .prev").on( "click", function() {
	// 	owl_brands.trigger('owl.prev');
	// });

	// $(".box-marcas .next").on( "click", function() {
	// 	owl_brands.trigger('owl.next');
	// });


	// owl_story = $(".carrousel-nossa-historia");

	// owl_story.owlCarousel({
	// 	navigation: false,
	// 	autoPlay: false,
	// 	pagination: true,
	// 	navigationText: false,
	// 	mouseDrag: true,
	// 	touchDrag: true,
	// 	afterInit: customPager,
	// 	afterUpdate: customPager,
	// 	singleItem: true,
	// 	autoHeight: true
	// });



	// $('.carrousel-nossa-historia .owl-pagination').owlCarousel({
	// 	navigation: true,
	// 	autoPlay: false,
	// 	pagination: false,
	// 	navigationText: false,
	// 	mouseDrag: true,
	// 	touchDrag: true,
	// 	singleItem: false,
	// 	items: 5
	// });


	// $('.carrousel-nossa-historia .owl-pagination .owl-item').click(function(){

	// 	$('.carrousel-nossa-historia .owl-pagination .owl-item').removeClass('atual');
	// 	$('.carrousel-nossa-historia .owl-pagination .owl-item').removeClass('before');
	// 	$(this).addClass('atual');
	// 	fazlinha();
	// });


	// MARCAS MENU SUPERIOR TIPOS


	// $('.js-type-brand').click(function(){

	// 	var tipo_marca = $(this).data('tipo');
	// 	// console.log( $(this).parent().parent().find('.conteudo').find('#'+tipo_marca).html() );



	// 	$(this).parent().parent().find('.conteudo').find('.box-conteudo').hide();
	// 	$(this).parent().parent().find('.conteudo').find('#'+tipo_marca).show();

	// 	$('.js-type-brand').removeClass('active');
	// 	$(this).addClass('active');
	// });


	// JS MOBILE
	if( $(window).width() < 770 ){


		$('.banner').height( $('.img-mobile').height() );


		var owl_depoimentos = $(".bx-depoimentos");

		owl_depoimentos.owlCarousel({
			navigation: false,
			autoPlay: true,
			pagination: true,
			navigationText: false,
			mouseDrag: true,
			touchDrag: true,
			singleItem: true
		});


		var owl_eventos = $(".list-eventos");

		owl_eventos.owlCarousel({
			autoPlay: true,
			mouseDrag: true,
			touchDrag: true,
			items: 2,
			itemsMobile: [768,2]
		});


	}


	$(window).on("scroll", function() {

		var position = $(window).scrollTop();
		// if( position > 120) {
		// 	$(".header").addClass("scrolling");
		// } else {
		// 	$(".header").removeClass("scrolling");
		// }


		// if( $('#numeros').hasClass('naorodou') ){

		// 	if( (position > $(".naorodou").offset().top - 100) && (position <  $("#nossasmarcas").offset().top)  ){

		// 		$('#numeros').addClass('rodou');
		// 		$('#numeros').removeClass('naorodou');

		// 		var counters = $("#numeros .number");

		// 		counters.each(function () {
		// 			$(this).prop('Counter',0).animate({
		// 				Counter: $(this).text()
		// 			}, {
		// 				duration: 4000,
		// 				easing: 'swing',
		// 				step: function (current_number) {
		// 					$(this).text(Math.ceil(current_number));
		// 				}
		// 			});
		// 		});

		// 	}
		// }


		if( (position > $("#sobre").offset().top) && (position <  ($("#diferenciais").offset().top - 120 ) ) ){
			$('.js-menu').removeClass('sel');
			$('.scroll-sobre').addClass('sel');
		}
		if( (position > $("#diferenciais").offset().top) && (position <  ( $("#comunicacao").offset().top - 120) ) ){
			$('.js-menu').removeClass('sel');
			$('.scroll-diferenciais').addClass('sel');
		}
		if( (position > $("#comunicacao").offset().top) && (position <  ( $("#eventos").offset().top - 120) ) ){
			$('.js-menu').removeClass('sel');
			$('.scroll-comunicacao').addClass('sel');
		}
		if( (position > $("#eventos").offset().top) && (position <  ( $("#contato").offset().top - 120) ) ){
			$('.js-menu').removeClass('sel');
			$('.scroll-eventos').addClass('sel');
		}
		if( position > ( $("#contato").offset().top - 400 ) ){
			$('.js-menu').removeClass('sel');
			$('.scroll-contato').addClass('sel');
		}

	});



	// if( $(".header").scrollTop() > 80 ){
	// 	$(".header").addClass("scrolling");
	// }


	$(".js-menu").click(function () {

		$('.js-menu').removeClass('sel');
		$(this).addClass('sel');

		var n = 0;
		var id = "#" + $(this).data('href');

		switch(id) {
			case '#comunicacao':
			var n = 78;
			break;
			default:
		}


		$('html, body').animate({
			scrollTop: $( id ).offset().top - n
		}, 1000);


		if( $(window).width() < 770 ){

			if( $('.bt-menu').hasClass('opened') ){

				$('.bt-menu').removeClass('opened');

				$('.box-menu').animate({
					top: '-100vh'
				}, 800);

			}else{

				$('.bt-menu').addClass('opened');

			}

		}
	});


	// OPEN MENU / CLOSE MENU MOBILE

	$('.bt-menu').click(function(){

		if( $(this).hasClass('opened') ){

			$(this).removeClass('opened');

			$('.box-menu').animate({
				top: '-100vh'
			}, 800);

		}else{

			$(this).addClass('opened');

			$('.box-menu').animate({
				top: '48px'
			}, 800);

		}

		

	});

	// $('.bt-fechar').click(function(){

	// 	$('.box-menu').animate({
	// 		top: '-100vh'
	// 	}, 800);

	// });
}



function customPager() {

	$('.carrousel-nossa-historia .item').each(function(i){

		var titleData = $(this).find('h4').text();
		var paginationLinks = $('.owl-controls .owl-pagination .owl-page span');

		$(paginationLinks[i]).append(titleData).wrap('<h3 class="slideTitle"></h3>');

	});
}

function fazlinha() {
	$('.carrousel-nossa-historia .owl-pagination .owl-item').each(function(i){
		if( $(this).hasClass('atual') ){
			return false;
		}else{
			$(this).addClass('before');
		}
	});
}