$(document).ready(function() {
    // Validação com Parsley.js ---------------
    // ----------------------------------------
    formValidate();

    // Definindo mascaras padroes ------------- //bug no android 5.1 (CPF inverte os numeros)
    // ----------------------------------------
    defaultMasks();

	// Input stepper --------------------------
	// ----------------------------------------
	inputStepper();

    home();
});


// PARSLEY ----------------------------------------------------------
// ------------------------------------------------------------------
function formValidate(){
    if ($('form').hasClass('js-form-validate')) {
        $('.js-form-validate').parsley();
    }   
}


// MASK -------------------------------------------------------------
// ------------------------------------------------------------------
var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
    },
    clearIfNotMatch: true
};
function defaultMasks() {
    $("[data-mask-type=date]").mask('00/00/0000');
    $("[data-mask-type=phone]").mask(SPMaskBehavior, spOptions);
    $("[data-mask-type=ddd]").mask('00');
    $("[data-mask-type=cep]").mask('00000-000');
    $("[data-mask-type=cpf]").mask('000.000.000-00');
    $("[data-mask-type=cnpj]").mask('99.999.999/9999-99');
    $("[data-mask-type=nome]").mask("S", {
        translation: {
            'S': {
                pattern: /[a-zA-Z ]/,
                fallback: '', 
                recursive: true
            },
            placeholder: ""
        }
    });
}


// INPUT STEPPER ----------------------------------------------------
// ------------------------------------------------------------------
function inputStepper(){
    $('.input-stepper').inputStepper({
        min: 0,
        max: 50
    });
    $('.input-stepper input').change(function(event) {
        $(this).parent('.input-stepper').addClass('loading');
    });
}

// CARREGAR ASSINATURA WT -------------------------------------------
// ------------------------------------------------------------------
(function () {
    var ss = document.createElement('script');
    ss.type = 'text/javascript';
    ss.src = '//www.wt.ag/signature/signature.js';
    var sc = document.getElementsByTagName('script')[0];
    sc.parentNode.insertBefore(ss, sc);
})();