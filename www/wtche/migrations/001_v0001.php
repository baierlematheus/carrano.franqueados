<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_v0001 extends Migration {

	/*

	Instalação Inicial do WTchê

	*/
	
	public function up() 
	{
		
		if ( ! $this->db->table_exists('activities')) {
            $this->dbforge->add_field(
            	array(
					'activity_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
						'auto_increment' => true,
			            'null' => false
					),
					'user_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
			            'null' => false,
			            'default' => 0
					),
					'activity' => array(
						'type' => 'LONGTEXT'
					),
					'module' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false
					),
					'created_by' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
			            'null' => TRUE,
					),
					'deleted' => array(
						'type' => 'tinyint',
						'constraint' => 12,
			            'null' => false,
			            'default' => 0
					),
				)
            );
            $this->dbforge->add_key('activity_id', true);
            $this->dbforge->create_table('activities');
        }

		if ( ! $this->db->table_exists('email_queue')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'to_email' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'reply_to' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'subject' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'message' => array(
						'type' => 'TEXT',
			            'null' => false,
					),
					'alt_message' => array(
						'type' => 'TEXT',
						'null' => true,
					),
					'max_attempts' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 3
					),
					'attempts' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 0
					),
					'success' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'date_published' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
					'last_attempt' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
					'date_sent' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('email_queue');
        }

		if ( ! $this->db->table_exists('login_attempts')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
						'auto_increment' => true,
			            'null' => false
					),
					'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false
					),
					'login' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
			            'null' => false
					),
					/* This will probably cause an error outside MySQL and may not
			         * be cross-database compatible for reasons other than
			         * CURRENT_TIMESTAMP
			         */
					'time TIMESTAMP DEFAULT CURRENT_TIMESTAMP'

				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('login_attempts');
        }

		if ( ! $this->db->table_exists('permissions')) {
            $this->dbforge->add_field(
            	array(
					'permission_id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'name' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'description' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'status' => array(
						'type' => 'ENUM',
						'constraint' => "'active','inactive','deleted'",
						'default' => 'active',
						'null' => false,
					)

				)
            );
            $this->dbforge->add_key('permission_id', true);
            $this->dbforge->create_table('permissions');
            $permissions = array(
				array('name' => 'Site.Content.View','description' => 'Visualizar o contexto "Conteúdo"','status' => 'active'),
				array('name' => 'Site.Reports.View','description' => 'Visualizar o contexto "Relatórios"','status' => 'active'),
				array('name' => 'Site.Settings.View','description' => 'Visualizar o contexto "Configurações"','status' => 'active'),
				array('name' => 'Site.Developer.View','description' => 'Visualizar o contexto "Desenvolvedor"','status' => 'active'),
				array('name' => 'WTche.Roles.View','description' => 'Visualizar o menu de Papéis','status' => 'active'),
				array('name' => 'WTche.Roles.Manage','description' => 'Gerenciar Papéis','status' => 'active'),
				array('name' => 'WTche.Roles.Delete','description' => 'Deletar Papéis','status' => 'active'),
				array('name' => 'WTche.Roles.Add','description' => 'Adicionar Papéis','status' => 'active'),
				array('name' => 'WTche.Users.Manage','description' => 'Gerenciar os Usuários','status' => 'active'),
				array('name' => 'WTche.Users.View','description' => 'Gerenciar as configurações dos Usuários','status' => 'active'),
				array('name' => 'WTche.Users.Add','description' => 'Adicionar novos Usuários','status' => 'active'),
				array('name' => 'WTche.Database.View','description' => 'Visualizar o menu de Banco de Dados','status' => 'active'),
				array('name' => 'WTche.Database.Manage','description' => 'Gerenciar as configurações de Banco de Dados','status' => 'active'),
				array('name' => 'WTche.Emailer.View','description' => 'Visualizar as configurações de Emails','status' => 'active'),
				array('name' => 'WTche.Emailer.Manage','description' => 'Gerenciar as configurações de Emails','status' => 'active'),
				array('name' => 'WTche.Logs.View','description' => 'Visualizar os Logs','status' => 'active'),
				array('name' => 'WTche.Logs.Manage','description' => 'Gerenciar os Logs','status' => 'active'),
				array('name' => 'Site.Signin.Offline','description' => 'Permite fazer login quando o site estiver em manutenção (offline)','status' => 'active'),
				array('name' => 'WTche.Permissions.View','description' => 'Visualizar o menu de Permissões','status' => 'active'),
				array('name' => 'WTche.Permissions.Manage','description' => 'Gerenciar as Permissões','status' => 'active'),
				array('name' => 'Permissions.Root.Manage','description' => 'Gerenciar oacesso ao controle de Permissões do papél Root','status' => 'active'),
				array('name' => 'Permissions.Cadastros.Manage','description' => 'Gerenciar oacesso ao controle de Permissões do papél Cadastros','status' => 'active'),
				array('name' => 'WTche.Activities.View','description' => 'Visualizar o menu de Relatórios','status' => 'active'),
				array('name' => 'Activities.Own.View','description' => 'Visualizar seus próprios Relatórios','status' => 'active'),
				array('name' => 'Activities.Own.Delete','description' => 'Deletar seus próprios Relatórios','status' => 'active'),
				array('name' => 'Activities.User.View','description' => 'Visualizar os Relatórios dos Usuários','status' => 'active'),
				array('name' => 'Activities.User.Delete','description' => 'Deletar os Relatórios dos Usuários, exceto o seu','status' => 'active'),
				array('name' => 'Activities.Module.View','description' => 'Visualizar os Relatórios dos Módulos','status' => 'active'),
				array('name' => 'Activities.Module.Delete','description' => 'Deletar os Relatórios dos Módulos','status' => 'active'),
				array('name' => 'Activities.Date.View','description' => 'Visualizar os Relatórios por Datas','status' => 'active'),
				array('name' => 'Activities.Date.Delete','description' => 'Deletar os Relatórios por Datas','status' => 'active'),
				array('name' => 'WTche.Settings.View','description' => 'Visualizar a tela de Configurações','status' => 'active'),
				array('name' => 'WTche.Settings.Manage','description' => 'Gerenciar as Configurações','status' => 'active'),
				array('name' => 'WTche.Migrations.View','description' => 'Visualizar o menu de Migrações','status' => 'active'),
				array('name' => 'WTche.Sysinfo.View','description' => 'Visualizar o menu de Informações do Sistema','status' => 'active'),
				array('name' => 'WTche.Translate.View','description' => 'Visualizar o menu de Traduções','status' => 'active'),
				array('name' => 'WTche.Translate.Manage','description' => 'Gerenciar as Traduções','status' => 'active'),
				array('name' => 'WTche.Profiler.View','description' => 'Visualizar o "Profiler"','status' => 'active'),
				array('name' => 'WTche.Modules.View','description' => 'Permite acessar o módulo de Gerenciamento de Módulos','status' => 'active'),
			);
            $this->db->insert_batch('permissions', $permissions);
        }

        if ( ! $this->db->table_exists('role_permissions')) {
            $this->dbforge->add_field(
            	array(
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false
					),
					'permission_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false
					),
				)
            );
            $this->dbforge->add_key('role_id', true);
            $this->dbforge->add_key('permission_id', true);
            $this->dbforge->create_table('role_permissions');

            //Adiciona todas as Permissions por default para o admin
            $admin_role_permissions = array();
            foreach ($permissions as $key => $value) {
            	$admin_role_permissions[] = array('role_id' => '1','permission_id' => $key+1);
            }
            $this->db->insert_batch('role_permissions', $admin_role_permissions);
			$this->db->insert_batch('role_permissions', array(
				//Cadastros
				array('role_id' => '2','permission_id' => '1'),
				array('role_id' => '2','permission_id' => '3'),
				array('role_id' => '2','permission_id' => '10'),

			));
        }

        if ( ! $this->db->table_exists('roles')) {
            $this->dbforge->add_field(
            	array(
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'role_name' => array(
						'type' => 'VARCHAR',
						'constraint' => 60,
			            'null' => false
					),
					'description' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => true
					),
					'default' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'can_delete' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 1
					),
					'login_destination' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => '/'
					),
					'deleted' => array(
						'type' => 'INT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'default_context' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => 'content'
					)
				)
            );
            $this->dbforge->add_key('role_id', true);
            $this->dbforge->create_table('roles');
            $this->db->insert_batch('roles', array(
            	array('role_name' => 'Root', 'description' => 'Tem acesso a TUDO.', 'default' => '0', 'can_delete' => '0', 'login_destination' => '/manager', 'deleted' => '0', 'default_context' => 'content'),
				array('role_name' => 'Cadastros', 'description' => 'Tem acesso aos cadastros e algumas configurações básicas.', 'default' => '1', 'can_delete' => '1', 'login_destination' => '/manager', 'deleted' => '0', 'default_context' => 'content'),
			));
        }

        /* Não necessário pois é feito ná classe de migrations
        // Vamos deixar aqui apenas para ficar visível que é criada a tabela
		if ( ! $this->db->table_exists('schema_version')) {
            $this->dbforge->add_field(
            	array(
					'type' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false
					),
					'version' => array(
						'type' => 'INT',
						'constraint' => 4,
			            'null' => false,
			            'default' => 0
					)
				)
            );
            $this->dbforge->add_key('type', true);
            $this->dbforge->create_table('schema_version');
            $this->db->insert_batch('schema_version', array(
            	array('type' => 'core', 'version' => '0')
            ));
        }
        */

		if ( ! $this->db->table_exists('schema_crud_version')) {
            $this->dbforge->add_field(
            	array(
					'module' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false
					),
					'checksum' => array(
						'type' => 'VARCHAR',
						'constraint' => 255
					)
				)
            );
            $this->dbforge->add_key('module', TRUE);
            $this->dbforge->create_table('schema_crud_version');
        }

		if ( ! $this->db->table_exists('sessions')) {
            $this->dbforge->add_field(
            	array(
					'session_id' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => 0
					),
					'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => 45,
			            'null' => false,
			            'default' => 0
					),
					'user_agent' => array(
						'type' => 'VARCHAR',
						'constraint' => 120,
			            'null' => false
					),
					'last_activity' => array(
						'type' => 'INT',
						'constraint' => 10,
						'unsigned' => true,
			            'null' => false,
			            'default' => 0
					),
					'user_data' => array(
						'type' => 'TEXT',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('session_id', true);
            $this->dbforge->create_table('sessions');
        }
		
		if ( ! $this->db->table_exists('settings')) {
            $this->dbforge->add_field(
            	array(
					'name' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'module' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
			            'null' => false
					),
					'value' => array(
						'type' => 'TEXT',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('name', true);
            $this->dbforge->create_table('settings');
            $this->db->insert_batch('settings', array(
            	array('name' => 'auth.allow_name_change', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.allow_register', 				'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.allow_remember', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.do_login_redirect', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.login_type', 					'module' => 'core', 	'value' => 'username'),
				array('name' => 'auth.name_change_frequency', 		'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.name_change_limit', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.password_force_mixed_case', 	'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_force_numbers', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_force_symbols', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_min_length', 		'module' => 'core', 	'value' => '8'),
				array('name' => 'auth.password_show_labels', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.remember_length', 			'module' => 'core', 	'value' => '1209600'),
				array('name' => 'auth.user_activation_method', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.use_extended_profile', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.use_usernames', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'mailpath', 						'module' => 'email', 	'value' => '/usr/sbin/sendmail'),
				array('name' => 'mailtype', 						'module' => 'email', 	'value' => 'html'),
				array('name' => 'password_iterations', 				'module' => 'users', 	'value' => '8'),
				array('name' => 'protocol', 						'module' => 'email', 	'value' => 'mail'),
				array('name' => 'sender_email', 					'module' => 'email', 	'value' => ''),
				array('name' => 'site.languages', 					'module' => 'core', 	'value' => 'a:1:{i:0;s:2:"pt";}'),
				array('name' => 'site.show_front_profiler', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'site.show_profiler', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'site.status', 						'module' => 'core', 	'value' => '1'),
				array('name' => 'site.system_email', 				'module' => 'core', 	'value' => ''),
				array('name' => 'site.title', 						'module' => 'core', 	'value' => 'WTchê Framework'),
				array('name' => 'smtp_host', 						'module' => 'email', 	'value' => ''),
				array('name' => 'smtp_pass', 						'module' => 'email', 	'value' => ''),
				array('name' => 'smtp_port', 						'module' => 'email', 	'value' => '587'),
				array('name' => 'smtp_timeout', 					'module' => 'email', 	'value' => '30'),
				array('name' => 'smtp_user', 						'module' => 'email', 	'value' => ''),
				array('name' => 'smtp_crypto', 						'module' => 'email', 	'value' => 'null'),
				array('name' => 'site.list_limit', 					'module' => 'core', 	'value' => '25'),
            ));
        }

		if ( ! $this->db->table_exists('user_cookies')) {
            $this->dbforge->add_field(
            	array(
					'user_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
			            'null' => false
					),
					'token' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('token', true);
            $this->dbforge->create_table('user_cookies');

        }

		if ( ! $this->db->table_exists('user_meta')) {
            $this->dbforge->add_field(
            	array(
					'meta_id' => array(
						'type' => 'INT',
						'constraint' => 20,
						'unsigned' => true,
			            'null' => false,
			            'auto_increment' => true,
					),
					'user_id' => array(
						'type' => 'INT',
						'constraint' => 20,
						'unsigned' => true,
			            'null' => false,
			            'default' => 0,
					),
					'meta_key' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => ''
					),
					'meta_value' => array(
						'type' => 'TEXT'
					)
				)
            );
            $this->dbforge->add_key('meta_id', true);
            $this->dbforge->create_table('user_meta');

        }

        
		if ( ! $this->db->table_exists('users')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 10,
						'unsigned' => true,
			            'null' => false,
			            'auto_increment' => true,
					),
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 2,
					),
					'email' => array(
						'type' => 'VARCHAR',
						'constraint' => 120,
			            'null' => false
					),
					'username' => array(
						'type' => 'VARCHAR',
						'constraint' => 30,
			            'null' => false,
			            'default' => ''
					),
					'password_hash' => array(
						'type' => 'CHAR',
						'constraint' => 60,
			            'null' => false
					),
					'reset_hash' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => true
					),
					'last_login' => array(
						'type' => 'DATETIME',
			            'null' => false,
			            'default' => '0000-00-00 00:00:00'
					),
					'last_ip' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => '0'
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false,
			            'default' => '0000-00-00 00:00:00'
					),
					'created_by' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
			            'null' => TRUE,
					),
					'modified_by' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
			            'null' => TRUE
					),
					'deleted' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'reset_by' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'null' => true
					),
					'banned' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'ban_message' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => true
					),
					'display_name' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => ''
					),
					'display_name_changed' => array(
						'type' => 'DATE',
			            'null' => true
					),
					'timezone' => array(
						'type' => 'CHAR',
						'constraint' => 4,
			            'null' => false,
			            'default' => 'UM3'
					),
					'language' => array(
						'type' => 'VARCHAR',
						'constraint' => 20,
			            'null' => false,
			            'default' => 'pt'
					),
					'active' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'activate_hash' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => ''
					),
					'password_iterations' => array(
						'type' => 'INT',
						'constraint' => 4,
			            'null' => false
					),
					'force_password_reset' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					)
				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->add_key('email');
            $this->dbforge->create_table('users');

            $this->db->insert_batch('users', array(
            	array(
					'role_id'				=> 1,
					'email'					=> 'suporte@wtagencia.com.br',
					'username'				=> 'admin',
					'active'    			=> 1,
					'password_hash' 		=> '$2a$08$/HEzvwL3NlhDKOiB.Nbpqes.vBoHX4xuJEoCRCF0SPlueoq6gYf2K',
					'password_iterations' 	=> 0,
					'created_on' 			=> date('Y-m-d H:i:s'),
					'display_name' 			=> "WT Agência",
				)
            ));

        }

	}
	
	public function down() 
	{
		
		//Boom! =D

	}

}