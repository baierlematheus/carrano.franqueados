<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtche.uy/#license
 * @link      http://www.wtche.uy
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Language
 *
 * Metodos para auxílio nos sites multi-idiomas
 *
 * @package    WTchê
 * @subpackage Libraries
 * @category   Libraries
 * @author     WTchê Dev Team
 * @link       http://www.wtche.uy/#guides
 *
 */
class WT_Language
{

    /**
     * Retorna a LANG atual do Site
     *
     */
    public static function lang_key() {

        $wtche =& get_instance();

        $key = $wtche->uri->segment(1);
        if (!$key || !in_array($key, $wtche->config->item('available_languages'))) {
            $key = $wtche->config->item('language');
        }

        return $key;

    }

    /**
     * Retorna a URL com o lang já setado
     *
     */
    public static function lang_url($path = null) {

        $wtche =& get_instance();

        if (self::lang_key() == $wtche->config->item('language')) {
            $url = site_url($path);
        } else {
            $url = site_url(self::lang_key() . '/' . $path);
        }
        return $url . (substr($url, -1) == '/' ? '' : '/');
    }

}//end class

/* End of file : ./libraries/WT_Language.php */