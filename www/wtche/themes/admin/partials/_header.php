<!doctype html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php e($this->settings_lib->item('site.title')) ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="robots" content="noindex" />
	
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

	<script type="text/javascript">
  		var site_url = "<?php echo site_url(); ?>";
  		var admin_url = "<?php echo site_url(ADMIN_AREA); ?>";
  		var base_url = "<?php echo base_url(); ?>";
  	</script>

  	<?php echo Assets::external_js(js_path() . 'jquery.js'); ?>

  	<script type="text/javascript">

		$(function(){
	  		//Adiciona o hash de csrf_protection em todas as requisições ajax
	  		$.ajaxSetup({
			   	beforeSend: function(jqXHR, settings) {
			   		if (typeof(settings.data) == 'string') {
			   			settings.data = settings.data + "&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>";
			   		} else if (typeof(settings.data) == 'object') {
			   			settings.data.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
			   		}
			   	}
			});
		});
	</script>

  	<?php

		//Assets::clear_cache();

		Assets::add_css( array(
			'bootstrap.css',
			'londinium-theme.css',
			'styles.css',
			'icons.css',
			'validationEngine.jquery.css',
			'bootstrap-switch.css',
			'wtche.css',
			'summernote.css',
		));

	?>

	<?php echo Assets::css(); ?>

</head>


 
