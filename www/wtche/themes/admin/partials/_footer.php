	
	<?php 

	Assets::add_js( array( 

		'jquery-ui.js',

		'bootstrap.js',

		'plugins/forms/jquery.uniform.js',
		'plugins/forms/select2.js',
		'plugins/forms/jquery.autosize.js',
		'plugins/forms/jquery.inputlimiter.js',
		'plugins/forms/listbox.js',
		'plugins/forms/multiselect.js',
		'plugins/forms/tags.min.js',
		'plugins/forms/bootstrap-switch.js',

		'plugins/summernote/summernote.js',
		'plugins/summernote/lang/summernote-pt-BR.js',

		'plugins/interface/daterangepicker.js',
		'plugins/interface/fancybox.js',
		'plugins/interface/moment.js',
		'plugins/interface/jgrowl.js',
		'plugins/interface/jquery.dataTables.js',
		'plugins/interface/jquery.tableTools.js',
		'plugins/interface/fullcalendar.min.js',
		'plugins/interface/timepicker.min.js',
		'plugins/interface/collapsible.min.js',
		'plugins/interface/mousewheel.js',

		'jquery.mask.js',
		'jquery.validationEngine.js',
		'jquery.validationEngine-pt_BR.js',

		'fileupload/load-image.js',
		'fileupload/load-image-ios.js',
		'fileupload/load-image-orientation.js',
		'fileupload/load-image-meta.js',
		'fileupload/load-image-exif.js',
		'fileupload/load-image-exif-map.js',

		'fileupload/jquery.fileupload.js',
		'fileupload/jquery.iframe-transport.js',
		'fileupload/jquery.fileupload-process.js',
		'fileupload/jquery.fileupload-image.js',
		//'fileupload/jquery.fileupload-audio.js',
		//'fileupload/jquery.fileupload-video.js',
		'fileupload/jquery.fileupload-validate.js',

		'bootstrap.file-input.js',

		'bootbox.js',
		
		'application.js',

	));

	?>

	<?php echo Assets::js(); ?>

    </div>
		<!-- /page content -->

	</div>
	<!-- /content -->

	<div id="debug"><!-- Stores the Profiler Results --></div>

</body>
</html>
