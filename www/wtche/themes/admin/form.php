<?php echo theme_view('partials/_header'); ?>
<body class="full-width page-condensed">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
	 	<div class="page-content">

	 		<?php Template::block('sub_nav', ''); ?>

			<?php echo Template::message(); ?>

			<div id="ui_messages">
				<?php
				if (validation_errors()) :
				?>
					<div class="callout callout-error fade in">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h5><?php echo lang('module_form_validation_error_title') ?></h5>
						<?php echo validation_errors('','<br>'); ?>
					</div>
				<?php endif; ?>
			</div>

			<?php echo isset($content) ? $content : Template::content(); ?>

<?php echo theme_view('partials/_footer'); ?>