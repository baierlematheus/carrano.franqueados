<?php // Change the css classes to suit your needs
	if( isset($permissions) ) {
		$permissions = (array)$permissions;
	}
	$id = isset($permissions['permission_id']) ? "/".$permissions['permission_id'] : '';

	$errorClass     = empty($errorClass) ? 'has-error' : $errorClass;
?>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-bordered"'); ?>

	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('permissions_details') ?></h6></div>
	    <div class="panel-body">

			<div class="form-group <?php echo iif(form_error('name'), $errorClass); ?>">
				<label for="name" class="col-sm-2 control-label text-right"><?php echo lang('permissions_name') ?></label>
		    	<div class="col-sm-10">
		    	    <input id="name" type="text" name="name" class="form-control" maxlength="100" value="<?php echo set_value('name', isset($permissions['name']) ? $permissions['name'] : ''); ?>"  />
		    	    <span class="help-inline"><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo iif(form_error('description'), $errorClass); ?>">
				<label for="description" class="col-sm-2 control-label text-right"><?php echo lang('permissions_description') ?></label>
		        <div class="col-sm-10">
			        <input id="description" type="text" name="description" class="form-control" maxlength="100" value="<?php echo set_value('description', isset($permissions['description']) ? $permissions['description'] : ''); ?>"  />
			        <span class="help-inline"><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group">
				<label for="status" class="col-sm-2 control-label text-right"><?php echo lang('permissions_status') ?></label>
				<div class="col-sm-10">
					<select name="status" id="status" class="select2">
						<option value="active" <?php echo set_select('status', 'active') ?>><?php echo lang('permissions_active') ?></option>
						<option value="inactive" <?php echo set_select('status', 'inactive') ?>><?php echo lang('permissions_inactive') ?></option>
					</select>
				</div>
			</div>


		</div>
	</div>
	<div class="form-actions text-right">
		<?php echo anchor(ADMIN_AREA .'/settings/permissions', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
		<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('permissions_save');?>" />
	</div>

<?php echo form_close(); ?>

<br>