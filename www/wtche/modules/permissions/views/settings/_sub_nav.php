<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
</div>

<ul class="nav nav-tabs">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/settings/permissions') ?>"><?php echo lang('bf_action_list'); ?></a>
	</li>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/settings/permissions/create') ?>" id="create_new"><?php echo lang('bf_action_create'); ?></a>
	</li>
	<li>
		<a href="<?php echo site_url(ADMIN_AREA .'/settings/roles/permission_matrix') ?>" ><?php echo lang('permissions_matrix'); ?></a>
	</li>
</ul>

<br>
