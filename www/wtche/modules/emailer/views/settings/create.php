<?php

$validation_errors = validation_errors();
$num_users_columns = 7;

?>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-bordered"'); ?>
	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title">General Settings</h6></div>
	    <div class="panel-body">

				<div class="form-group">
					<label class="col-sm-2 control-label text-right" for="mailtype">Subject:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" size="50" name="email_subject" id="email_subject" value="<?php if (isset($email_subject)) { e($email_subject); } ?>">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label text-right" for="mailtype">Content:</label>
					<div class="col-sm-10">
						<textarea name="email_content" id="email_content" class="form-control" rows="15"><?php
							if (isset($email_content)) { e($email_content); }
						?></textarea>
					</div>
				</div>

				<div class="panel panel-default">
				    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('bf_users') ?></h6></div>
				    <div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th class="column-check"><input class="check-all styled" type="checkbox" /></th>
									<th style="width: 3em"><?php echo lang('bf_id'); ?></th>
									<th><?php echo lang('bf_username'); ?></th>
									<th><?php echo lang('bf_display_name'); ?></th>
									<th><?php echo lang('bf_email'); ?></th>
									<th style="width: 11em"><?php echo lang('us_last_login'); ?></th>
									<th style="width: 10em"><?php echo lang('us_status'); ?></th>
								</tr>
							</thead>
							<?php
							if (isset($users) && is_array($users) && count($users)) :
								$has_checked = isset($checked) && is_array($checked) && count($checked);

							?>
							<tbody>
								<?php foreach ($users as $user) : ?>
								<tr>
									<td class='column-check'>
										<input type="checkbox" class="styled" name="checked[]" value="<?php echo $user->id; ?>"<?php echo $has_checked && in_array($user->id, $checked) ? ' checked="checked"' : ''; ?> />
									</td>
									<td><?php echo $user->id; ?></td>
									<td>
										<a href="<?php echo site_url(ADMIN_AREA . '/settings/users/edit/' . $user->id); ?>"><?php echo $user->username; ?></a>
										<?php echo $user->banned ? '<span class="label label-warning">Banned</span>' : ''; ?>
									</td>
									<td><?php echo $user->display_name; ?></td>
									<td><?php echo empty($user->email) ? '' : mailto($user->email); ?></td>
									<td><?php echo $user->last_login != '0000-00-00 00:00:00' ? date('M j, y g:i A', strtotime($user->last_login)) : '---'; ?></td>
									<td>
									<?php if ($user->active) : ?>
										<span class="label label-success"><?php echo lang('us_active'); ?></span>
									<?php else : ?>
										<span class="label label-warning"><?php echo lang('us_inactive'); ?></span>
									<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<?php else: ?>
							<tbody>
								<tr>
									<td colspan="<?php echo $num_users_columns; ?>">No users found that match your selection.</td>
								</tr>
							</tbody>
							<?php endif; ?>
						</table>
					</div>
				</div>
		</div>
	</div>
	<div class="form-actions text-right">
		<?php echo anchor(ADMIN_AREA . '/settings/emailer/queue', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
		<input type="submit" name="create" class="btn btn-primary" value="<?php echo lang('em_create_email') ?>">
	</div>
<?php echo form_close(); ?>
<br>