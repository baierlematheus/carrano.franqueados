<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_test_result_header'); ?></h6></div>
    <div class="panel-body">

		<?php if ($success !== false) :?>
			<div class="alert alert-info fade in">
				<?php echo lang('em_test_success'); ?>
			</div>
		<?php else : ?>
			<div class="alert alert-warning fade in">
				<?php echo lang('em_test_error'); ?>
			</div>
		<?php endif; ?>

	</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_test_debug_header'); ?></h6></div>
    <div class="panel-body">
    	<?php echo $debug; ?>
    </div>
</div>
