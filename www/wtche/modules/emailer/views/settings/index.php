
<?php echo form_open(ADMIN_AREA .'/settings/emailer', 'class="form-horizontal form-bordered"'); ?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title">General Settings</h6></div>
    <div class="panel-body">

    	<div class="form-group <?php echo form_error('sender_email') ? 'has-error' : '' ?>">
			<label class="col-sm-2 control-label text-right" for="sender_email"><?php echo lang('em_system_email'); ?></label>
			<div class="col-sm-10">
				<input type="email" name="sender_email" id="sender_email" class="form-control" value="<?php echo set_value('sender_email', $sender_email)  ?>" />
				<?php if (form_error('sender_email')) echo '<span class="help-inline">'. form_error('sender_email') .'</span>'; ?>
				<p class="help-block"><?php echo lang('em_system_email_note'); ?></p>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label text-right" for="mailtype"><?php echo lang('em_email_type'); ?></label>
			<div class="col-sm-10">
				<select name="mailtype" id="mailtype" class="select2">
					<option value="text" <?php echo set_select('mailtype', 'text', $mailtype == 'text'); ?>>Text</option>
					<option value="html" <?php echo set_select('mailtype', 'html', $mailtype == 'html'); ?>>HTML</option>
				</select>
			</div>
		</div>

		<div class="form-group <?php echo form_error('protocol') ? 'has-error' : ''; ?>">
			<label class="col-sm-2 control-label text-right" for="server_type"><?php echo lang('em_email_server'); ?></label>
			<div class="col-sm-10">
				<select name="protocol" id="server_type" class="select2">
					<option <?php echo set_select('protocol', 'mail', $protocol == 'mail'); ?>>mail</option>
					<option <?php echo set_select('protocol', 'sendmail', $protocol == 'sendmail'); ?>>sendmail</option>
					<option value="smtp" <?php echo set_select('protocol', 'smtp', $protocol == 'smtp'); ?>>SMTP</option>
				</select>
	    	    <span class="help-inline"><?php echo form_error('protocol'); ?></span>
			</div>
		</div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_settings'); ?></h6></div>
    <div class="panel-body">
    	<!-- PHP Mail -->
		<div id="mail" class="control-group">
			<p class="intro"><?php echo lang('em_settings_note'); ?></p>
		</div>

		<!-- Sendmail -->
		<div id="sendmail" class="control-group <?php echo form_error('mailpath') ? 'has-error' : ''; ?>">
			<label class="col-sm-2 control-label text-right" for="mailpath">Sendmail <?php echo lang('em_location'); ?></label>
			<div class="col-sm-10">
				<input type="text" name="mailpath" id="mailpath" class="form-control" value="<?php echo set_value('mailpath', $mailpath) ?>" />
				<span class="help-inline"><?php echo form_error('mailpath'); ?></span>
			</div>
		</div>

		<!-- SMTP -->
		<div id="smtp">

			<div class="form-group <?php echo form_error('smtp_host') ? 'has-error' : ''; ?>">
				<label class="col-sm-2 control-label text-right" for="smtp_host">SMTP <?php echo lang('em_server_address'); ?></label>
				<div class="col-sm-10">
					<input type="text" name="smtp_host" id="smtp_host" class="form-control" value="<?php echo set_value('smtp_host', $smtp_host) ?>" />
		    	    <span class="help-inline"><?php echo form_error('smtp_host'); ?></span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="smtp_user">SMTP <?php echo lang('bf_username'); ?></label>
				<div class="col-sm-10">
					<input type="text" name="smtp_user" id="smtp_user" class="form-control" value="<?php echo set_value('smtp_user', $smtp_user) ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="smtp_pass">SMTP <?php echo lang('bf_password'); ?></label>
				<div class="col-sm-10">
					<input type="password" name="smtp_pass" id="smtp_pass" class="form-control" value="<?php echo set_value('smtp_pass', $smtp_pass) ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="smtp_port">SMTP <?php echo lang('em_port'); ?></label>
				<div class="col-sm-10">
					<input type="text" name="smtp_port" id="smtp_port" class="form-control" value="<?php echo set_value('smtp_port', $smtp_port) ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="smtp_crypto"><?php echo lang('em_smtp_crypto'); ?> (null|ssl|tls)</label>
				<div class="col-sm-10">
					<input type="text" name="smtp_crypto" id="smtp_crypto" class="form-control" value="<?php echo set_value('smtp_crypto', $smtp_crypto) ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="smtp_timeout">SMTP <?php echo lang('em_timeout_secs'); ?></label>
				<div class="col-sm-10">
					<input type="text" name="smtp_timeout" id="smtp_timeout" class="form-control" value="<?php echo set_value('smtp_timeout', $smtp_timeout) ?>" />
				</div>
			</div>
		</div>
    </div>
</div>

<div class="form-actions text-right">
	<input type="submit" name="save" class="btn btn-success" value="<?php e(lang('em_save_settings')); ?>" />
</div>

<?php echo form_close(); ?>

<br><br>
<!-- Test Settings -->
<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_test_header'); ?></h6></div>
    <div class="panel-body">
    	<?php echo form_open(ADMIN_AREA .'/settings/emailer/test', array('class' => 'form-horizontal form-bordered', 'id'=>'test-form')); ?>

			<p class="intro"><?php echo lang('em_test_intro'); ?></p>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="test-email"><?php echo lang('bf_email'); ?></label>
				<div class="col-sm-10">
					<div class="input-group">
						<input type="email" class="form-control" name="email" id="test-email" value="<?php echo set_value('test_email', settings_item('site.system_email')) ?>" />
						<span class="input-group-btn">
							<input type="submit" name="test" class="btn btn-warning" value="<?php echo lang('em_test_button'); ?>" />
						</span>
					</div>
					
					
				</div>
			</div>

			<div id="test-ajax"></div>

		<?php echo form_close(); ?>
    </div>
</div>
