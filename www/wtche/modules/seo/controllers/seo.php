<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo extends Base_Controller
{

	public function index()
	{

		$this->load->model('seo/seo_model');

		//Busca do SEO de acordo com a URL
		$seo = $this->seo_model->get(uri_string());

		//Se não encontrou vai pegar no Default
		if (!$seo) {
			$settings = $this->settings_lib->find_all();
			$seo = new stdClass();
			$seo->title = $settings['seo.default_title'];
			$seo->description = $settings['seo.default_description'];
			$seo->keywords = $settings['seo.default_keywords'];
		}

		//Caso o título ainda não tenha sido preenchido então pegamos do Settings
		if (!$seo->title) {
			$seo->title = $settings['site.title'];
		}

		$data['seo'] = $seo;

        $this->load->view('seo/tags', $data);
	}

}