<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_Fields_Seo extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'uri' => array(
			'type' => 'VARCHAR',
			'constraint' => 255
		),
		'title' => array(
			'type' => 'VARCHAR',
			'constraint' => 255
		),
		'description' => array(
			'type' => 'VARCHAR',
			'constraint' => 100
		),
		'keywords' => array(
			'type' => 'VARCHAR',
			'constraint' => 255
		)
	);

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = get_module_name(__FILE__);

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}