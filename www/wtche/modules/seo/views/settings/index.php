<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-bordered"'); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
        <div class="panel-body">

        	<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('seo_default_title') ?></label>
				<div class="col-sm-10">
					<input type="text" name="seo_default_title" class="form-control" value="<?php echo set_value('seo_default_title', isset($settings['seo.default_title']) ? $settings['seo.default_title'] : '') ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('seo_default_keywords') ?></label>
				<div class="col-sm-10">
					<input type="text" name="seo_default_keywords" class="form-control" value="<?php echo set_value('seo_default_keywords', isset($settings['seo.default_keywords']) ? $settings['seo.default_keywords'] : '') ?>">
					<p class="help-block"><?php echo lang('seo_default_keywords_help') ?></p>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('seo_default_description') ?></label>
				<div class="col-sm-10">
					<textarea name="seo_default_description" class="form-control"><?php echo set_value('seo_default_description', isset($settings['seo.default_description']) ? $settings['seo.default_description'] : '') ?></textarea>
				</div>
			</div>

        </div>
    </div>

    <div class="form-actions text-right">
		<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('bf_action_save') . ' ' . lang('bf_context_settings'); ?>" />
	</div>

<?php echo form_close(); ?>