<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo_model extends WT_Module_Model {

	public $auto_forge = FALSE;

	public function setupCrud() {

		$this->lang->load($this->module_name,'',FALSE,TRUE,'',$this->module_name);
		
		$this->wtche->form->addField('char')
			->setFieldName('uri')
			->setLabel('URL')
			->setValidationRules('required|trim');

		$this->wtche->form->addField('char')
			->setFieldName('title')
			->setLabel('Título')
			->setValidationRules('required');

		$this->wtche->form->addField('char')
			->setFieldName('description')
			->setLabel('Descrição')
			->setMaxLength(255);

		$this->wtche->form->addField('char')
			->setFieldName('keywords')
			->setLabel('Palavras Chave')
			->setValidationRules('trim');

	}

	public function get($uri)
	{
		if (empty($uri)) return FALSE;

		return $this->find_by('uri', $uri);

	}

}