<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

$lang["seo_configuration"] 			= "Configurações do SEO Padrões";
$lang["seo_default_title"] 			= "Título";
$lang["seo_default_description"] 	= "Descrição";
$lang["seo_default_keywords"] 		= "Palavras Chave";
$lang["seo_default_keywords_help"] 	= "Algo entre 10 e menos que 30 palavras, separadas por vírgula.";