<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(

	'description'	=> '',
	'name'			=> 'SEO',
	'version'		=> '',
	'menus'	=> array(
		'settings'	=> 'seo/settings/menu'
	),
	'author'		=> 'WT Prime',

);