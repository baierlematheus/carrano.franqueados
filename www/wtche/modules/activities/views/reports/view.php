<div class="page-header row">
	<div class="col-md-4 col-md-offset-8">
		<?php
			echo form_open(ADMIN_AREA . '/reports/activities/' . $vars['which'], 'class="form-horizontal"');
			$form_help = sprintf(lang('activity_filter_note'),($vars['view_which'] == ucwords(lang('activity_date')) ? 'from before':'only for'),strtolower($vars['view_which']));
			$form_data = array('name' => $vars['which'].'_select', 'id' => $vars['which'].'_select', 'class' => 'select2 pull-left' );
			echo form_dropdown($form_data, $select_options, $filter, $form_help, 'onChange="$(this).closest(\'form\').trigger(\'submit\')"');
			unset ( $form_data, $form_help);
			//echo form_submit('filter', lang('activity_filter'), 'class="btn btn-primary pull-right"');
			echo form_close(); 
		?>
	</div>
</div>

<br style="clear: both">

<div class="block">

	<h6 class="heading-hr"><i class="icon-eye"></i> <?php echo sprintf(lang('activity_view'),($vars['view_which'] == ucwords(lang('activity_date')) ? $vars['view_which'] . ' before' : $vars['view_which']),$vars['name']); ?></h6>
	<?php if (!isset($activity_content) || empty($activity_content)) : ?>
	<div class="alert alert-error fade in">
		<a class="close" data-dismiss="alert">&times;</a>
		<h4 class="alert-heading"><?php echo lang('activity_not_found'); ?></h4>
	</div>
	<?php else : ?>

	<div id="user_activities">
		<table class="table table-striped table-bordered" id="flex_table">
			<thead>
				<tr>
					<th><?php echo lang('activity_user'); ?></th>
					<th><?php echo lang('activity_activity'); ?></th>
					<th><?php echo lang('activity_module'); ?></th>
					<th><?php echo lang('activity_when'); ?></th>
				</tr>
			</thead>

			<tfoot></tfoot>

			<tbody>
				<?php foreach ($activity_content as $activity) : ?>
				<tr>
					<td><i class="icon-user">&nbsp;</i>&nbsp;<?php e($activity->username); ?></td>
					<td><?php echo $activity->activity; ?></td>
					<td><?php echo $activity->module; ?></td>
					<td><?php echo date('M j, Y g:i A', strtotime($activity->created)); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

	<?php echo $this->pagination->create_links(); ?>
	<?php endif; ?>
