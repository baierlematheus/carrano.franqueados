<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
</div>

<ul class="nav nav-tabs">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/translate') ?>"><?php echo lang('tr_translate'); ?></a>
	</li>
	<li <?php echo $this->uri->segment(4) == 'export' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/translate/export') ?>"><?php echo lang('tr_export_short'); ?></a>
	</li>
</ul>

<br>