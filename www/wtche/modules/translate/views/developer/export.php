<div class="bg-info with-padding block-inner"><?php echo lang('tr_export_note'); ?></div>

<?php echo form_open(current_url(), 'class="form-horizontal"'); ?>
	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
	    <div class="panel-body">
			<div class="form-group">
				<label for="export_lang" class="col-sm-2 control-label text-right"><?php echo lang('tr_language') ?></label>
				<div class="col-sm-10">
					<select name="export_lang" id="export_lang" class="select2">
					<?php foreach ($languages as $lang) :?>
						<option value="<?php e($lang) ?>" <?php echo isset($trans_lang) && $trans_lang == $lang ? 'selected="selected"' : '' ?>><?php e(ucfirst($lang)) ?></option>
					<?php endforeach; ?>
						<option value="other"><?php e(lang('tr_other')); ?></option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label text-right"><?php echo lang('tr_include') ?></label>
				<div class="col-sm-10">
					<label for="include_core">
						<input class="styled" type="checkbox" id="include_core" name="include_core" value="1" checked="checked" />
						<?php echo lang('tr_include_core'); ?>
					</label>
					<br>
					<label for="include_mods">
						<input class="styled" type="checkbox" id="include_mods" name="include_mods" value="1" />
						<?php echo lang('tr_include_mods'); ?>
					</label>
				</div>
			</div>
		</div>
	</div>


	<div class="form-actions text-right">
		<input type="submit" name="export" class="btn btn-success" value="<?php e(lang('tr_export_short')); ?>" />
	</div>

<?php echo form_close(); ?>
