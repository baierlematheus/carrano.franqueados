<?php echo form_open(current_url(), 'class="form-horizontal" id="translate_form"'); ?>
	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('tr_language') .': '. ucfirst($trans_lang) ?> | <?php echo lang('tr_translate_file') .": $lang_file"  ?></h6></div>
	    <div class="panel-body">

			<?php if (isset($orig) && is_array($orig) && count($orig)) : ?>

					<input type="hidden" name="trans_lang" value="<?php e($trans_lang) ?>" />
				
					<?php foreach ($orig as $key => $val) : ?>
						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="lang<?php echo $key ?>"><?php e($val) ?></label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="lang[<?php echo $key ?>]" id="lang<?php echo $key ?>" value="<?php e(isset($new[$key]) ? $new[$key] : '') ?>" />
							</div>
						</div>
					<?php endforeach; ?>
					

			<?php else : ?>

			<?php endif; ?>
		</div>
	</div>
	<div class="form-actions text-right">
		<a class="btn btn-warning" href="<?php echo site_url(ADMIN_AREA .'/developer/translate/index/'. $trans_lang); ?>">
			<?php e(lang('bf_action_cancel'), 'class="btn btn-danger"'); ?>
		</a>
		<input type="submit" name="save" class="btn btn-success" value="<?php e(lang('bf_action_save')) ?>" />
	</div>
</form>
