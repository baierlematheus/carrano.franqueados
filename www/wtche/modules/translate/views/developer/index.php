
<div class="well">
	
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label text-right" for="role_name"><?php e(lang('tr_current_lang')); ?></label>
			<div class="col-sm-7">
				<select name="trans_lang" id="trans_lang" class="select2">
					<?php foreach ($languages as $lang) :?>
						<option value="<?php e($lang) ?>" <?php echo isset($trans_lang) && $trans_lang == $lang ? 'selected="selected"' : '' ?>><?php e(ucfirst($lang)) ?></option>
					<?php endforeach; ?>
					<option value="other"><?php e(lang('tr_other')); ?></option>
				</select>
				<input type="text" class="form-control" name="new_lang" id="new_lang" style="display: none" value="" />
			</div>
			<input type="submit" name="select_lang" class="btn btn-small btn-primary col-sm-2 " value="<?php e(lang('tr_select_lang')); ?>" />
		</div>
		
	</form>
</div>

<br>

<!-- Modules -->
<div class="panel panel-default">
    <div class="table-heading"><h6 class="panel-title"><?php echo lang('tr_modules') ?></h6></div>
    <div class="table-responsive">

		<table class="table table-striped">
			<tbody>
			<?php if (isset($modules) && is_array($modules) && count($modules)) : ?>
			<?php foreach ($modules as $file) :?>
				<tr>
					<td>
						<a href="<?php echo site_url(ADMIN_AREA .'/developer/translate/edit/'. $trans_lang .'/'. $file) ?>">
							<?php e($file); ?>
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			<?php else : ?>
				<tr>
					<td>
						<div class="alert alert-info fade in">
							<a class="close" data-dismiss="alert">&times;</a>		
							<?php echo lang('tr_no_modules'); ?>
						</div>
					</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<!-- Core -->
<div class="panel panel-default">
    <div class="table-heading"><h6 class="panel-title"><?php echo lang('tr_core') ?></h6></div>
    <div class="table-responsive">

		<table class="table table-striped">

			<tbody>
			<?php foreach ($lang_files as $file) :?>
				<tr>
					<td>
						<a href="<?php echo site_url(ADMIN_AREA .'/developer/translate/edit/'. $trans_lang .'/'. $file) ?>">
							<?php e($file); ?>
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
