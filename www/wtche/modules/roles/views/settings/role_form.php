<?php 
$errorClass     = empty($errorClass) ? 'has-error' : $errorClass;
?>


<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('role_details') ?></h6></div>
    <div class="panel-body">

		<div class="form-group <?php echo iif(form_error('rola_name'), $errorClass); ?>">
			<label class="col-sm-2 control-label text-right" for="role_name"><?php echo lang('role_name'); ?></label>
			<div class="col-sm-10">
				<input type="text" name="role_name" id="role_name" class="form-control" value="<?php echo set_value('role_name', isset($role) ? $role->role_name : '') ?>" />
				<span class="help-block"><?php echo form_error('role_name'); ?></span>
			</div>
		</div>

		<div class="form-group <?php echo iif(form_error('description'), $errorClass); ?>" style="vertical-align: top">
			<label class="col-sm-2 control-label text-right" for="description"><?php echo lang('bf_description'); ?></label>
			<div class="col-sm-10">
				<textarea name="description" id="description" rows="3" class="form-control"><?php echo set_value('description', isset($role) ? $role->description : '') ?></textarea>
				<span class="help-block"><?php echo form_error('description') ? form_error('description') : lang('role_max_desc_length'); ?></span>
			</div>
		</div>

		<div class="form-group <?php echo iif(form_error('login_destination'), $errorClass); ?>">
			<label class="col-sm-2 control-label text-right" for="login_destination"><?php echo lang('role_login_destination'); ?>?</label>
			<div class="col-sm-10">
				<input type="text" name="login_destination" id="login_destination" class="form-control" value="<?php echo set_value('login_destination', isset($role) ? $role->login_destination : '') ?>"  />
				<span class="help-block"><?php echo form_error('login_destination') ? form_error('login_destination') : lang('role_destination_note'); ?></span>
			</div>
		</div>

        <div class="form-group">
            <label class="col-sm-2 control-label text-right" for="default_context"><?php echo lang('role_default_context') ?></label>
            <div class="col-sm-10">
                <select name="default_context" id="default_context" class="select2">
                    <?php if (isset($contexts) && is_array($contexts) && count($contexts)):?>
                    <?php foreach($contexts as $context):?>
                        <option value="<?php echo $context;?>" <?php echo set_select('default_context', $context, (isset($role) && $role->default_context == $context) ? TRUE : FALSE) ?>><?php echo ucfirst($context) ?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
                <span class="help-block"><?php echo form_error('default_context') ? form_error('default_context') : lang('role_default_context_note'); ?></span>
            </div>
        </div>

		<div class="form-group <?php echo iif(form_error('default'), $errorClass); ?>">
			<label class="col-sm-2 control-label text-right" for="default"><?php echo lang('role_default_role')?></label>
			<div class="col-sm-10">
				<label class="checkbox" for="default" >
					<input class="styled" type="checkbox" name="default" id="default" value="1" <?php echo set_checkbox('default', 1, isset($role) && $role->default == 1 ? TRUE : FALSE) ?> />
					<?php echo lang('role_default_note'); ?>
				</label>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label text-right" id="can_delete_label"><?php echo lang('role_can_delete_role'); ?>?</label>
			<div class="col-sm-10" aria-labelledby="can_delete_label" role="group">
				<label class="radio" for="can_delete_yes">
					<input class="styled" type="radio" name="can_delete" id="can_delete_yes" value="1" <?php echo set_radio('can_delete', 1, isset($role) && $role->can_delete == 1 ? TRUE : FALSE) ?> /> Yes
				</label>
				<label class="radio" for="can_delete_no">
					<input class="styled" type="radio" name="can_delete" id="can_delete_no" value="0" <?php echo set_radio('can_delete', 0, isset($role) && $role->can_delete == 0 ? TRUE : FALSE) ?> /> No
				</label>
				<span class="help-block" style="display: inline"><?php echo lang('role_can_delete_note'); ?></span>
			</div>
		</div>

	</div>
</div>
<!-- Permissions -->
<?php if (has_permission('WTche.Permissions.Manage')) : ?>
	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('role_permissions'); ?></h6></div>
	    <div class="panel-body">
			<?php echo modules::run('roles/settings/matrix'); ?>
	    </div>
	</div>
<?php endif; ?>
<div class="form-actions text-right">
	<?php if(isset($role) && $role->can_delete == 1 && has_permission('WTche.Roles.Delete')):?>
		<button type="submit" name="delete" class="btn btn-danger" onclick="return confirm('<?php e(js_escape(lang('role_delete_confirm').' '.lang('role_delete_note'))) ?>')"><?php echo lang('role_delete_role'); ?></button>
	<?php endif;?>
 	<?php echo anchor(ADMIN_AREA .'/settings/roles', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
	<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('role_save_role'); ?>" />
</div>
<?php echo form_close(); ?>
