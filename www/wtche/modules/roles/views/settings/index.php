<?php if (isset($role_counts) && is_array($role_counts) && count($role_counts)) : ?>
<div class="panel panel-default">
    <div class="table-heading"><h6 class="panel-title"><?php e(lang('role_intro')) ?></h6></div>
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="width: 10em"><?php echo lang('role_account_type'); ?></th>
					<th class="text-center" style="width: 5em"><?php echo lang('bf_users'); ?></th>
					<th><?php echo lang('role_description') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($roles as $role) : ?>
				<tr>
					<td><?php echo anchor(ADMIN_AREA .'/settings/roles/edit/'. $role->role_id, $role->role_name) ?></td>
					<td class="text-center"><?php
							$count = 0;
							foreach ($role_counts as $r)
							{
								if ($role->role_name == $r->role_name)
								{
									$count = $r->count;
								}
							}

							echo $count;
						?>
					</td>
					<td><?php e($role->description) ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif; ?>
