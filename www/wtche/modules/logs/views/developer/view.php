<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><span style="font-weight: normal">Viewing:</span> <?php echo $log_file_pretty; ?></h6></div>
    <div class="panel-body">

			<?php if (!isset($log_content) || empty($log_content)) : ?>
				<div class="alert alert-warning fade in">
					<a class="close" data-dismiss="alert">&times;</a>
					<?php echo lang('log_not_found'); ?>
				</div>
				<br>
			<?php else : ?>

			<select class="select2">
				<option value="all"><?php echo lang('log_show_all_entries'); ?></option>
				<option value="error"><?php echo lang('log_show_errors'); ?></option>
			</select>

			<div id="log">
				<?php foreach ($log_content as $row) : ?>
				<?php
				// Log files start with PHP guard header
				// (apparently we don't trust .htaccess)
				if (strpos($row, '<?php') === 0)
				{
					continue;
				}

				$class = '';

				if (strpos($row, 'ERROR') !== false)
				{
					$class="alert-error";
				} else
					if (strpos($row, 'DEBUG') !== false)
					{
						$class="alert-warning";
					}
				?>
				<div style="border-bottom: 1px solid #CCC; padding: 10px 18px; color: #222;" <?php echo 'class="'. $class .'"' ?>>
					<?php e($row); ?>
				</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php if (has_permission('WTche.Logs.Manage')) : ?>
	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('log_delete1_button') ?></h6></div>
	    <div class="panel-body">
			<?php echo form_open(ADMIN_AREA .'/developer/logs'); ?>
			<div class="alert alert-warning fade in">
				<a class="close" data-dismiss="alert">&times;</a>
				<?php echo sprintf(lang('log_delete1_note'),$log_file_pretty); ?>
			</div>
			<br>
			<div class="form-actions">
				<input type="hidden" name="checked[]" value="<?php e($log_file); ?>" />

				<button type="submit" name="delete" class="btn btn-danger" onclick="return confirm('<?php e(js_escape(lang('log_delete_confirm'))) ?>')"><?php echo lang('log_delete1_button'); ?></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
<?php endif; ?>