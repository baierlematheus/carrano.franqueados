<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
</div>

<ul class="nav nav-tabs">
	<li  <?php echo $this->uri->segment(4) != 'settings' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/logs') ?>"><?php echo lang('log_logs'); ?></a>
	</li>
	<li <?php echo $this->uri->segment(4) == 'settings' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/logs/settings') ?>"><?php echo lang('log_settings'); ?></a>
	</li>
</ul>

<br>
