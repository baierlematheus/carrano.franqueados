<?php defined('BASEPATH') || exit('No direct script access allowed');

class Favicon_model extends WT_Module_Model {
	
	public function setupCrud() {

		$this->wtche->form->addField('file_upload')
			->setFieldName($this, 'icon')
			->setLabel('Icone Web Principal')
			->setLabelHelper('PNG 32x32')
			->setMaxSize(1000)
			->setMaxFiles(1)
			->setAllowedTypes('png')
			->setValidationRules('required');

		$this->wtche->form->addField('file_upload')
			->setFieldName($this, 'icon_apple')
			->setLabel('Icone Apple')
			->setLabelHelper('PNG 152x152')
			->setMaxSize(1000)
			->setMaxFiles(1)
			->setAllowedTypes('png');
		
		$this->wtche->form->addField('char')
			->setFieldName('tile_color')
			->setLabel('Tile Color Windows Phone')
			->setMaxLength(6);

		$this->wtche->form->addField('file_upload')
			->setFieldName($this, 'icon_windows_phone')
			->setLabel('Icone Windows Phone')
			->setLabelHelper('PNG 144x144')
			->setMaxSize(1000)
			->setMaxFiles(1)
			->setAllowedTypes('png');
			
	}

	public function getFavicon() {

		return $this->db->select('tile_color, icon.file_name icon, icon_apple.file_name icon_apple, icon_windows_phone.file_name icon_windows_phone')
			->join("{$this->db->dbprefix('favicon_icon')} icon", "{$this->table_name}.id = icon.favicon_id", 'left')
			->join("{$this->db->dbprefix('favicon_icon_apple')} icon_apple", "{$this->table_name}.id = icon_apple.favicon_id", 'left')
			->join("{$this->db->dbprefix('favicon_icon_windows_phone')} icon_windows_phone", "{$this->table_name}.id = icon_windows_phone.favicon_id", 'left')
			->get($this->table_name)->row();

	}

}
