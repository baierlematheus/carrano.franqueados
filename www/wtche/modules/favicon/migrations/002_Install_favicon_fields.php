<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Favicon_Fields extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'tile_color' => array(
			'type' => 'VARCHAR',
			'constraint' => 6,
			'null' => true
		),
	);

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = 'favicon';

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);

		$this->load->dbforge();

		$fields = array(
          	'id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'favicon_id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_name_original' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_order' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'is_valid' => array(
				'type' => 'SMALLINT',
				'default' => 0
			),
			'image_preview' => array(
				'type' => 'LONGTEXT',
        		'null' => TRUE
			),
			'image_width' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'image_height' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			)
        );
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('favicon_icon');

		$this->db->query('ALTER TABLE ' . 'favicon_icon' . '
			ADD CONSTRAINT `' . md5('favicon_icon' . 'favicon' . 'favicon_id') . '`
			FOREIGN KEY (' . 'favicon_id' . ')
			REFERENCES `' . 'favicon' . '` (' . 'id' . ')
			ON DELETE CASCADE
			ON UPDATE CASCADE');
	


		$this->load->dbforge();

		$fields = array(
          	'id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'favicon_id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_name_original' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_order' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'is_valid' => array(
				'type' => 'SMALLINT',
				'default' => 0
			),
			'image_preview' => array(
				'type' => 'LONGTEXT',
        		'null' => TRUE
			),
			'image_width' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'image_height' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			)
        );
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('favicon_icon_apple');

		$this->db->query('ALTER TABLE ' . 'favicon_icon_apple' . '
			ADD CONSTRAINT `' . md5('favicon_icon_apple' . 'favicon' . 'favicon_id') . '`
			FOREIGN KEY (' . 'favicon_id' . ')
			REFERENCES `' . 'favicon' . '` (' . 'id' . ')
			ON DELETE CASCADE
			ON UPDATE CASCADE');
	


		$this->load->dbforge();

		$fields = array(
          	'id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'favicon_id' => array(
				'type' => 'INT',
				'constraint' => 10,
	            'unsigned' => TRUE
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_name_original' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_order' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'is_valid' => array(
				'type' => 'SMALLINT',
				'default' => 0
			),
			'image_preview' => array(
				'type' => 'LONGTEXT',
        		'null' => TRUE
			),
			'image_width' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'image_height' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			)
        );
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('favicon_icon_windows_phone');

		$this->db->query('ALTER TABLE ' . 'favicon_icon_windows_phone' . '
			ADD CONSTRAINT `' . md5('favicon_icon_windows_phone' . 'favicon' . 'favicon_id') . '`
			FOREIGN KEY (' . 'favicon_id' . ')
			REFERENCES `' . 'favicon' . '` (' . 'id' . ')
			ON DELETE CASCADE
			ON UPDATE CASCADE');
			
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}