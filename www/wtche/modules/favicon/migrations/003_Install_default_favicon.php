<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Default_Favicon extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = 'favicon';

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->load->model('favicon/favicon_model');
		$this->db->insert($this->table_name, array('tile_color' => 'FFFFFF'));
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		$this->db->truncate($this->table_name); 
	}

	//--------------------------------------------------------------------

}