<?php if ($favicon): ?>
	<!-- Default -->
	<?php if ($favicon->icon): ?>
		<link rel="icon" href="<?php echo site_url('uploads/favicon_icon/' . $favicon->icon) ?>" sizes="32x32">
	<?php endif ?>

	<!-- Touch icon for iOS 2.0+ and Android 2.1+: -->
	<?php if ($favicon->icon_apple): ?>
		<link rel="apple-touch-icon-precomposed" href="<?php echo site_url('uploads/favicon_icon/' . $favicon->icon_apple) ?>">
	<?php endif ?>

	<!-- IE 10 Metro tile icon (Metro equivalent of apple-touch-icon): -->
	<?php if ($favicon->tile_color): ?>
		<meta name="msapplication-TileColor" content="#<?php echo $favicon->tile_color ?>">
	<?php endif ?>
	<?php if ($favicon->icon_windows_phone): ?>
		<meta name="msapplication-TileImage" content="<?php echo site_url('uploads/favicon_icon/' . $favicon->icon_windows_phone) ?>">
	<?php endif ?>
<?php endif ?>