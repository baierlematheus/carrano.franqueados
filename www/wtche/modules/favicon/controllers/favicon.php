<?php defined('BASEPATH') || exit('No direct script access allowed');

class Favicon extends Front_Controller
{
    public function index()
	{

		$this->load->model('favicon/favicon_model');

		$data['favicon'] = $this->favicon_model->getFavicon();

        $this->load->view('favicon/tags', $data);
	}
}