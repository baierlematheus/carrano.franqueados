<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe responsável pela configuração dos Fields
 * Os métodos e atributos contidos aqui são disponíveis a todos os Fields
 */
class Field {

	/**
	 * $this->wtche =& get_instance(); //on constructor
	 *
	 * @var CI Object
	 * @access public
	 */
	public $wtche;

	/**
	 * Guarda o nome do Field (Vai coincidir com o nome do banco)
	 * @var String
	 */
	protected $field_name = null;
	public $field_forge_config = null;
	/*
	array(
		'type' => 'INT|VARCHAR|TEXT|DATE|DATETIME|DECIMAL|TINYINT|...',
		'constraint' => 5|255|'10,2'|1|..., 
		'unsigned' => TRUE,
		'auto_increment' => TRUE,
        'default' => 'Valor Default',
        'null' => TRUE
  	)
	*/
	public function setFieldName($field_name, $field_forge_config = null) {
		$this->field_name = $field_name;
		$this->setLabel(humanize($field_name));
		if ($field_forge_config) {
			$this->field_forge_config = $field_forge_config;
		}
		return $this;
	}
	public function fieldName() {
		return $this->field_name;
	}
	public function fieldForgeConfig() {
		return $this->field_forge_config;
	}

	/**
	 * Guarda o valor do campo
	 * Caso o campo seja um Relation esse valor será um array(KEY,VALUE)
	 **/
	protected $value = null;
	protected $value_callback = null;
	public function setValue($value) {
		$this->value = $value;
		if ($this->field_forge_config) {
			$this->field_forge_config['default'] = $value;
		}
		return $this;
	}
	public function setValueCallback($value_callback) {
		$this->value_callback = $value_callback;
		return $this;
	}
	//rawValue é utilizado quando o campo tem algum tipo de formatação especial e se necessita pegar o valor sem formatar (Datetime por ex.)
	public function value($row = null, $rawValue = false) {
		if (!$rawValue && is_callable($this->value_callback)) {
			return $this->value_callback($row, $rawValue);
		}
		return $this->value;
	}

	/**
	 * Tamanho máximo de caracteres de um campo
	 **/
	protected $max_length = 0; //0 = Sem limite
	public function setMaxLength($max_length) {
		$this->max_length = $max_length;
		if ($this->field_forge_config) {
			$this->field_forge_config['constraint'] = $max_length;
		}
		return $this;
	}
	public function maxLength() {
		return $this->max_length;
	}

	/**
	 * Caso o campo não deva ser adicionado em Banco deve-se setar esse valor para TRUE
	 **/
	protected $static = false;
	public function setStatic() {
		if ($this->isInlineEdit()) {
			throw new Exception("Campo InlineEdit não pode ser Static!", 1);
		}
		$this->static = TRUE;
		return $this;
	}
	public function isStatic() {
		return ($this->static === true);
	}

	/**
	 * Guarda as views de onde o campo será escondido
	 **/
	protected $hide_from = array();
	public function hideFrom($from) {
		array_push($this->hide_from, $from);
		return $this;
	}
	public function isHideFrom($from) {
		return in_array($from, $this->hide_from);
	}

	/**
	 * Label utilizada para impressão nas telas
	 **/
	protected $label = '';
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}
	public function label() {
		return $this->label;
	}

	/**
	 * Texto de help que fica abaixo do label
	 **/
	protected $label_helper = '';
	public function setLabelHelper($label_helper) {
		$this->label_helper = $label_helper;
		return $this;
	}
	public function labelHelper() {
		return $this->label_helper;
	}

		/**
	 * Texto de help que fica abaixo do Field
	 **/
	protected $field_helper = '';
	public function setFieldHelper($field_helper) {
		$this->field_helper = $field_helper;
		return $this;
	}
	public function fieldHelper() {
		return $this->field_helper;
	}

	/**
	 * Tipo do campo
	 **/
	protected $field_type;
	public function fieldType() {
		return $this->field_type;
	}
	public function isHidden() {

		return ($this->field_type == 'hidden');
	}

	/**
	 * Habilita Edição na Listagem
	 **/
	protected $inline_edit = false;
	public function setInlineEdit($inline_edit = true) {
		if ($this->isStatic()) {
			throw new Exception("Campo Static não pode ser InlineEdit!", 1);
		}
		$this->inline_edit = $inline_edit;
		return $this;
	}
	public function isInlineEdit() {

		return $this->inline_edit;
	}

	/**
	 * Possível Ordenar na Listagem?
	 **/
	protected $orderable = true;
	public function orderable() {
		return $this->orderable;
	}

	/**
	 * Validation Rules
	 **/
	protected $validation_rules = '';
	public function setValidationRules($validation_rules) {
		$this->validation_rules = $validation_rules;
		if ($this->field_forge_config) {
			$this->field_forge_config['null'] = !$this->isRequired();
		}
		return $this;
	}
	public function validationRules() {

		return $this->validation_rules;
	}

	public function isRequired() {
		return strpos($this->validation_rules, 'required') !== false;
	}

	/**
	 * Máscara
	 **/
	protected $mask = false;
	public function setMask($mask) {
		$this->mask = $mask;
		return $this;
	}
	public function mask() {

		return $this->mask;
	}
	/**
	 * Configuração Máscara (Em um futuro distante deverá ser criada uma classe PHP para isso)
	 **/
	protected $mask_config = "{}";
	public function setMaskConfig($mask_config) {
		$this->mask_config = $mask_config;
		return $this;
	}
	public function maskConfig() {

		return $this->mask_config;
	}

	/**
	 * Possibilita adicionar novas classes CSS ao input renderizado
	 * @var String
	 */
	protected $css_classes = array();
	public function addCssClass($class) {
		$this->css_classes[] = $class;
		return $this;
	}
	public function cssClasses($as_array = false) {
		if ($as_array) {
			return $this->css_classes;
		}
		return implode(' ', $this->css_classes);
	}

	/**
	 * Construtor
	 **/
	public function __construct()
	{

		$this->wtche =& get_instance();

	}

	/**
	 * Carrega a classe necessária para criação do Field
	 * @param  String $field_type Tipo do Field a ser carregado
	 * @return Field Retorna um objeto Field
	 */	
	public function create($field_type) {

		//Caso algum Field necessite de tratamento especial no load deve ser adicionado ao switch
		switch ($field_type) {
			
			//Os Fields que podem ser carregados de forma padrão são carregados aqui
			default:

					
					$this->wtche->load->library('forms/Fields/' . ucfirst($field_type));
					
					//Verifica se a Classe existe
					if (class_exists(ucfirst($field_type))) {
						$field = new $field_type;
						break;
					}

					throw new Exception("Classe " . ucfirst($field_type) . " inexistente no arquivo!", 1);

				break;

		}

		$field->field_type = $field_type;

		return $field;

	}

	/**
	 * Função que renderiza o Field dentro do HTML
	 * @param  String $action A view com a estrutura do Field será carregada de acordo com essa variável
	 * @return HTML Retorna o HTML contendo a estrutura do Field
	 */
	public function render($view, $form_data = null) {

		return $this->wtche->load->view('forms/fields/' . $this->field_type . '/' . $view, array('field'=>$this,'form_data'=>$form_data), true);

	}

	/**
	 * Observer Functions
	 *
	 * $before_XXXXXX contains the name function within the extending field
	 * which will be called before the XXXXXX method.
	 *
	 * @var array
	 * @access protected
	 */
	protected $before_insert = array();
	public function setBeforeInsert($before_insert) {
		$this->before_insert[] = $before_insert;
		return $this;
	}
	protected $after_insert	= array();
	public function setAfterInsert($after_insert) {
		$this->after_insert[] = $after_insert;
		return $this;
	}
	protected $before_update = array();
	public function setBeforeUpdate($before_update) {
		$this->before_update[] = $before_update;
		return $this;
	}
	protected $after_update	= array();
	public function setAfterUpdate($after_update) {
		$this->after_update[] = $after_update;
		return $this;
	}
	protected $before_delete = array();
	public function setBeforeDelete($before_delete) {
		$this->before_delete[] = $before_delete;
		return $this;
	}
	protected $after_delete	= array();
	public function setAfterDelete($after_delete) {
		$this->after_delete[] = $after_delete;
		return $this;
	}
	//helpers
	public function setBeforeSave($before_save) {
		$this->setBeforeInsert($before_save);
		$this->setBeforeUpdate($before_save);
		return $this;
	}
	public function setAfterSave($after_save) {
		$this->setAfterInsert($after_save);
		$this->setAfterUpdate($after_save);
		return $this;
	}
	/**
	 * Triggers a field-specific event and call each of it's observers.
	 *
	 * @param string 	$event 	The name of the event to trigger
	 * @param mixed 	$data 	The data to be passed to the callback functions.
	 *
	 * @return mixed
	 */
	public function trigger($event, $data = null, $row_id = null, $event_index = 0)
	{

		if (isset($this->{$event}[$event_index+1]) && is_callable($this->{$event}[$event_index+1])) {
			$data = $this->trigger($event, $data, $row_id, ($event_index+1)); //Recursivo
		}

		if (isset($this->{$event}[$event_index]) && is_callable($this->{$event}[$event_index])) {
			return $this->{$event}[$event_index]($data, $row_id);
		}

		return $data;
	}

	public function __call($method, $args) {
		if(isset($this->$method) && is_callable($this->$method)) {
			return call_user_func_array(
				$this->$method, 
				$args
			);
		}
	}

}

/* End of file Field.php */