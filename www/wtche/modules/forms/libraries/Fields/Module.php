<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campo tipo Module
 */
class Module extends Field {

	public function __construct()
	{
		$this->orderable = FALSE;
		$this->static = TRUE;
		$this->hideFrom('create');
		parent::__construct();
	}

	/**
	 * Definie o módulo filho
	 **/
	protected $module = null;
	protected $context = null;
	public function setModule($module, $context = null) {
		$this->module = $module;
		if ($context) {
			$this->context = $context;
		}
		return $this;
	}
	public function module() {
		return $this->module;
	}
	public function context() {
		return $this->context;
	}

	/**
	 * Definie o texto ou html do botão
	 **/
	protected $button_text = null;
	public function setButtonText($button_text) {
		$this->button_text = $button_text;
		return $this;
	}
	public function buttonText() {
		return $this->button_text;
	}

	/**
	 * Definie o field usado pra relação do módulo pai
	 **/
	protected $parent_field = null;
	public function setParentField($parent_field) {
		$this->parent_field = $parent_field;
		$this->setFieldName($parent_field);
		return $this;
	}
	public function parentField() {
		return $this->parent_field;
	}


	/**
	 * OVERWRITE
	 * Função que renderiza o Field dentro do HTML
	 * @param  String $action A view com a estrutura do Field será carregada de acordo com essa variável
	 * @return HTML Retorna o HTML contendo a estrutura do Field
	 */
	public function render($action) {

		if (!$this->module()) {
			throw new Exception("Field Module sem módulo setado!", 1);
		}
		if (!$this->parentField()) {
			throw new Exception("Field Module sem parent field setado!", 1);
		}
		return parent::render($action);

	}

	public function setValue($fake_value, $value) {
		//Seta o value para ficar "URLizável" pois é passado para o form via QueryParam
		$this->value = base64_encode(serialize(array($this->parentField() => $value)));
	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um Field Module //
	///////////////////////////////////////////////////////////////////////////

	public function setMask($mask_config) {
		throw new Exception("Campo Module não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo Module não pode ser definido com Máscara!", 1);
	}

}

/* End of file Module.php */