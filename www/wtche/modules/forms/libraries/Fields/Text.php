<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe para o Field Text
 */
class Text extends Field {

	public $config_rich;

	public function __construct() {

		parent::__construct();

		$this->field_forge_config = array(
			'type' => 'LONGTEXT',
	        'null' => TRUE
	  	);

	  	

	}

	/**
	 * Definie a utilização do Editor Rich Text para este Field
		
			$type = '' = rich completo;

			$type = 'simple' = ['style', ['bold', 'italic', 'underline', 'clear']],
							   ['para', ['paragraph']],
							   ['link', ['linkDialogShow', 'unlink']];
							   
			$type = 'personalized' = 'buscar em https://summernote.org;'				   

	 **/
	protected $rich = FALSE;
	public function setRich($type = '', $config = '') {

		if($type == ''){
			$class = 'rich';
		}else if($type == 'simple'){
			$class = 'richsimple';
		}else if($type == 'personalized'){
			$class = 'richpersonalized';
			$this->setConfig($config);

		}

		$this->addCssClass($class);
		$this->rich = TRUE;

		return $this;
	}

	public function isRich() {
		return $this->rich;
	}

	private function setConfig($config) {
		$this->config_rich = $config;
	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um Field Text //
	///////////////////////////////////////////////////////////////////////////
	
	public function setInlineEdit($inline_edit = true) {
		throw new Exception("Campo Text não pode ser definido como InlineEdit!", 1);
	}

	public function setMask($mask_config) {
		throw new Exception("Campo Text não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo Text não pode ser definido com Máscara!", 1);
	}

}

/* End of file Text.php */