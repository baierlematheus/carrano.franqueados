<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select extends Field {

	private $arr_items = array();

	/**
	 * Adiciona um array de itens para ser usado no select.
	 * @param array $arr_itens. 
	 */
	public function setItemsArray($arr_items){
		if(!is_array($arr_items)):
			throw new Exception("Esperado array e recebido ".gettype($arr_items)." no arquivo ".__FILE__." na linha ".__LINE__.".", 1);
		endif;

		foreach ($arr_items as $key => $value) {
			if (!is_object($value)) {
				$value = (object) array('text' => $value);
			}
			$this->arr_items[$key] = $value;
		}

		//Forge
		if (!$this->relation() && !$this->relationMultiple()) {
			$this->field_forge_config = array(
				'type' => 'ENUM("' . implode('","', array_keys($this->arr_items)) . '")',
		        'null' => TRUE
		  	);
		}

		return $this;
	}

	/**
	 * Seta o texto para o primeiro item (sem valor). Ex: "Selecione o XXXXXX"
	 * @param string $key nome do campo. 
	 * @param string $value valor que deve ter. 
	 */
	protected $first_item_text = 'Selecione';
	public function setFirstItemText($first_item_text) {
		$this->first_item_text = $first_item_text;
		return $this;
	}
	public function firstItemText() {
		return $this->first_item_text;
	}

	/**
	 * Possibilita aplicar filtros aos fields que buscam dados do banco
	 * @param string $key nome do campo. 
	 * @param string $value valor que deve ter. 
	 */
	protected $where_key = null;
	protected $where_value = null;
	public function where($key, $value) {
		$this->where_key = $key;
		$this->where_value = $value;
		return $this;
	}

	/**
	 * Retorna os itens do select.
	 * @return array
	 */
	public function items() {

		//Se tiver relation vamos popular os Itens de acordo com a tabela relacionada
		if (!$this->arr_items && ($this->relation() || $this->relationMultiple())) {

			if ($this->relation()) {
				$table = $this->relation['table'];
				$key = $this->relation['key'];
				$field = $this->relation['field'];
			} else {
				$table = $this->relation_multiple['table'];
				$key = $this->relation_multiple['key'];
				$field = $this->relation_multiple['field'];
			}

			$this->wtche->load->model("{$table}/{$table}_model");
			if ($this->dependency_key) {
				$this->wtche->{$table.'_model'}->select("{$this->dependency_key},{$key}, {$field}");
			} else {
				$this->wtche->{$table.'_model'}->select("{$key}, {$field}");
			}

			if ($this->where_key) {
				$this->wtche->{$table.'_model'}->where($this->where_key, $this->where_value);
			}

			$result = $this->wtche->{$table.'_model'}->find_all();

	        $items = array();
	        foreach ($result as $v) {
	        	$item = new stdClass();
	        	$item->text = $v->{$field};
	        	if ($this->dependency_key) {
	        		$item->{$this->dependency_key} = $v->{$this->dependency_key};
	        	}
	        	$items[$v->{$key}] = $item;
	        }

	        $this->setItemsArray($items);

		}

		return $this->arr_items;
	}

	/**
	 * Utilizado para relações entre tabelas (1 para 1)
	 * @var Mixed (false || array())
	 */
	protected $relation = false;
	public function setRelation($key, $table, $field) {
		if ($this->items()) {
			throw new Exception("Este campo já esta com Itens definidos!", 1);
		}
		if ($this->relationMultiple()) {
			throw new Exception("Este campo já esta com relação múltipla a outra tabela!", 1);
		}
		$this->relation['key']   = $key;
		$this->relation['table'] = $table;
		$this->relation['field'] = $field;

		$this->field_forge_config = array(
			'type' => 'INT',
			'constraint' => 10,
            'unsigned' => TRUE,
            'null' => TRUE
	  	);

		return $this;
	}
	public function relation() {
		return $this->relation;
	}

	/**
	 * Utilizado para relações entre tabelas (N para M)
	 * @var Mixed (false || array())
	 */
	protected $relation_multiple = false;
	public function setRelationMultiple($model, $key, $table, $field) {
		if ($this->items()) {
			throw new Exception("Este campo já esta com Itens definidos!", 1);
		}
		if ($this->relation()) {
			throw new Exception("Este campo já esta com relação a outra tabela!", 1);
		}
		if ($this->dependencyField()) {
			throw new Exception("Campo com setDependency não pode ser definido como relationMultiple!", 1);
		}

		$this->orderable = FALSE;

		$this->relation_multiple['table_aux']    = $model->get_table() . '_' . $table;
		$this->relation_multiple['field_child']  = $model->get_table() . '_' . $model->get_key();
		$this->relation_multiple['field_parent'] = $table . '_' . $key;
		$this->relation_multiple['key']          = $key;
		$this->relation_multiple['table']        = $table;
		$this->relation_multiple['field']        = $field;

		/**
		 * Salva os dados do Select Multiplo no banco
		 * @return bool
		 */
		$field = $this;

		$this->setAfterSave(function($post_data, $row_id) use ($field) {

            $relation_multiple = $field->relationMultiple();

            $relation_data = null;

            if (isset($post_data[$field->fieldName()])) {

            	//Remove o valor do Hidden
                foreach ($post_data[$field->fieldName()] as $key => $item) {
                    if ($item == 0) {
                        unset($post_data[$field->fieldName()][$key]);
                    }
                }

	            //Deleta todas as referencias na tabela auxiliar 
	            $field->wtche->db
	                ->where($relation_multiple['field_child'], $row_id)
	                ->delete($relation_multiple['table_aux']);

                foreach ($post_data[$field->fieldName()] as $value) {
                    $relation_data[] = array(
                        $relation_multiple['field_child'] => $row_id,
                        $relation_multiple['field_parent'] => $value
                    );
                }
                if ($relation_data) {
	                if (!$field->wtche->db->insert_batch($relation_multiple['table_aux'], $relation_data)) {
	                	return FALSE;
	                }
                }
            }
            
	        return $post_data;

		});

		return $this;
	}
	public function relationMultiple() {
		return $this->relation_multiple;
	}

	public function forgeDB($table) {

		if ($this->relation()) {

			$ck = md5($this->relation['table'] . '_' . $this->fieldName() . '_' . $this->relation['key']);

            $constraint = $this->wtche->db->query("SELECT * 
                FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
                WHERE CONSTRAINT_NAME = '{$ck}'")->row();

            if (!$constraint) {
				return ["
					\$this->db->query('ALTER TABLE ' . '{$table}' . '
						ADD CONSTRAINT `' . '{$ck}' . '`
						FOREIGN KEY (' . '{$this->fieldName()}' . ')
						REFERENCES `' . '{$this->relation['table']}' . '` (' . '{$this->relation['key']}' . ')
						ON DELETE CASCADE
						ON UPDATE CASCADE');
				","

				"];
			}

			return null;

		} elseif ($this->relationMultiple()) {

			$up = "";

			if (!$this->wtche->db->table_exists($this->relation_multiple['table_aux'])) {
			
				$up .= "\$this->load->dbforge();\n\n";

				if ($this->relation_multiple['field_child'] == $this->relation_multiple['field_parent']) {
					$this->relation_multiple['field_parent'] .= '_parent'; //Apenas para garantir que não sao dois campos como mesmo nome (Auto-relacionamento)
				}

				$up .= "\$fields = array(
		          	'{$this->relation_multiple['field_child']}' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
					),
					'{$this->relation_multiple['field_parent']}' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
					)
		        );
				\$this->dbforge->add_field(\$fields);
				\$this->dbforge->create_table('{$this->relation_multiple['table_aux']}');

				\$this->db->query('ALTER TABLE ' . '{$this->relation_multiple['table_aux']}' . '
					ADD CONSTRAINT `' . md5('{$this->relation_multiple['table_aux']}' . '_' . '{$this->fieldName()}' . '_' . '{$this->relation_multiple['field_child']}') . '`
					FOREIGN KEY (' . '{$this->relation_multiple['field_child']}' . ')
					REFERENCES `' . '{$table}' . '` (' . '{$this->relation_multiple['key']}' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');

				\$this->db->query('ALTER TABLE ' . '{$this->relation_multiple['table_aux']}' . '
					ADD CONSTRAINT `' . md5('{$this->relation_multiple['table_aux']}' . '_' . '{$this->fieldName()}' . '_' . '{$this->relation_multiple['field_parent']}') . '`
					FOREIGN KEY (' . '{$this->relation_multiple['field_parent']}' . ')
					REFERENCES `' . '{$this->relation_multiple['table']}' . '` (' . '{$this->relation_multiple['key']}' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');";
				
			}

			return $up?[$up,""]:null;

		}

	}

	/**
	 * Para o Select os values são tratados diferente dos demais Fields
	 * @param [type] $value [description]
	 */
	public function setValue($value, $row_id = null) {

		if ($this->items()) {

			if ($this->relationMultiple()) {

				$relation_multiple = $this->relationMultiple();

		        $result_aux = $this->wtche->db
		                    ->select($relation_multiple['field_parent'])
		                    ->from($this->wtche->db->dbprefix($relation_multiple['table_aux']))
		                    ->where($relation_multiple['field_child'],$row_id)
		                    ->get()->result_object();

		        $value = array();

		        foreach ($result_aux as $row_aux) {
		            
		            $result = $this->wtche->db
		                    ->select($relation_multiple['field'])
		                    ->from($this->wtche->db->dbprefix($relation_multiple['table']))
		                    ->where($relation_multiple['key'],$row_aux->{$relation_multiple['field_parent']})
		                    ->get();

		            $row = current($result->result_object());

		            @$value[$row_aux->{$relation_multiple['field_parent']}] = $row->{$relation_multiple['field']};

		        }

		        $this->value = $value;

			} else {

				$items = $this->items();
				if (isset($items[$value])) {
					$value_text = $items[$value];
				} else {
					$value_text = $value;
				}
				$this->value = array(
					$value => $value_text
				);

			}
			
			return $this;
		}

	}
	public function value($row = null, $raw_value = false) {

		if ($raw_value && is_array($this->value)) {
			return (string)key($this->value);
		}

		return $this->value;

	}

	/**
	 * Define dependência a outro campo (ex: estados e cidades)
	 */
	protected $dependency_field = null;
	protected $dependency_key = null;
	public function setDependency($dependency_field, $dependency_key) {
		if ($this->isInlineEdit()) {
			throw new Exception("setDependency não pode ser usado em fields com inlineEdit!", 1);
		}
		if ($this->relationMultiple()) {
			throw new Exception("setDependency não pode ser usado em fields com relationMultiple!", 1);
		}
		$this->dependency_field = $dependency_field;
		$this->dependency_key = $dependency_key;
		return $this;
	}
	public function dependencyField() {
		return $this->dependency_field;
	}
	public function dependencyKey() {
		return $this->dependency_key;
	}

	//Resgata o valor do campo de vinculo com o pai
	public function dependencyValue() {
		if (!is_array($this->value())) {
			return null;
		}
		$value = reset($this->value());
		if (!is_object($value)) {
			return null;
		}
		return $value->{$this->dependencyKey()};
	}

	//Informa que o select foi carregado pelo pai
	protected $dependency_loaded = false;
	public function setDependencyLoaded() {
		$this->dependency_loaded = true;
		return $this;
	}
	public function dependencyLoaded() {
		return $this->dependency_loaded;
	}

	//Texto de ajuda para quando o select estiver bloqueado por não ter selecionado o pai ainda
	protected $dependency_text_helper = '';
	public function setDependencyTextHelper($dependency_text_helper) {
		$this->dependency_text_helper = $dependency_text_helper;
		return $this;
	}
	public function dependencyTextHelper() {
		return $this->dependency_text_helper;
	}

	//Apenas para certificar que não vai usar inlineEdit com dependency
	public function setInlineEdit($inline_edit = true) {
		if ($this->dependencyField()) {
			throw new Exception("Campo com setDependency não pode ser definido como InlineEdit!", 1);
		}
		return parent::setInlineEdit($inline_edit);
	}

}

/* End of file Select.php */
/* Location: ./application/libraries/Form/Fields/Select.php */