<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campo tipo Generic
 */
class Generic extends Field {

	public function __construct()
	{
		$this->orderable = FALSE;
		parent::__construct();
	}

	/**
	 * Guarda o nome da pasta de onde será carregado o template html do field
	 * @var String
	 */
	protected $field_view_folder = null;
	public function setFieldViewFolder($field_view_folder) {
		$this->field_view_folder = $field_view_folder;
		return $this;
	}
	public function fieldViewFolder() {
		return $this->field_view_folder;
	}

	/**
	 * Função que renderiza o Field dentro do HTML
	 * @param  String $action A view com a estrutura do Field será carregada de acordo com essa variável
	 * @return HTML Retorna o HTML contendo a estrutura do Field
	 */
	public function render($view) {

		if (!$this->fieldViewFolder()) {
			throw new Exception("Campo Generic sem view folder configurado! Utilize o método setFieldViewFolder().", 1);
		}

		return $this->wtche->load->view($this->fieldViewFolder() . '/' . $view, array('field'=>$this), true);

	}

}

/* End of file Generic.php */