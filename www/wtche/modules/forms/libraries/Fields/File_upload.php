<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_upload extends Field {

	public function __construct()
	{
		$this->orderable = FALSE;
		parent::__construct();
	}

	/**
	 * Definie o limite de arquivos
	 **/
	protected $max_files = 1;
	public function setMaxFiles($max_files) {
		$this->max_files = $max_files;
		return $this;
	}
	public function maxFiles() {
		return $this->max_files;
	}

	/**
	 * Definie o limite de tamanho dos arquivos em K
	 **/
	protected $max_size = '500'; //padrão 500K
	public function setMaxSize($max_size) {
		$this->max_size = $max_size;
		return $this;
	}
	public function maxSize() {
		return $this->max_size;
	}

	/**
	 * Definie os formatos de arquivos aceitos (Separados por | (pipe))
	 **/
	protected $allowed_types = '*'; //* = Todos
	public function setAllowedTypes($allowed_types) {
		$this->allowed_types = $allowed_types;
		return $this;
	}
	public function allowedTypes() {
		return $this->allowed_types;
	}

	/**
	 * Define submodulos para o campo
	 **/
	protected $submodules = array();
	public function addSubmodule($module, $parent_field_name, $title = '', $icon = 'icon-menu2') {

		$module = (object)array(
			'module_name' => $module,
			'parent_field_name' => $parent_field_name,
			'title' => $title,
			'icon' => $icon,
		);

		array_push($this->submodules, $module);
		return $this;
	}
	public function submodules() {
		return $this->submodules;
	}

	/**
	 * Para setar o field_name de um upload precisamos do model correspondente também
	 * pois vamos guardar um nome de tabela auxiliar
	 */
	protected $table_name;
	protected $fk;
	public $model;
	public function setFieldName($model, $field_name) {
		
		$this->table_name = $model->get_table() . '_' . strtolower($field_name);
		$this->fk = $model->get_table() . '_' . $model->get_key();
		$this->model = $model;

		/**
		 * Salva os dados do Upload no banco
		 * @return bool
		 */
		$field = $this;
		$this->setAfterSave(function($post_data, $row_id) use ($field) {

	        if (isset($post_data[$field->fieldName()]) && $post_data[$field->fieldName()]) {

	        	$order = 1; //inicia a ordem em 1

	            foreach ($post_data[$field->fieldName()] as $file_name => $file) {
	                $file_data = array(
	                    $field->fk() => $row_id,
	                    'file_name' => $file_name,
	                    'file_name_original' => $file['orig_name'],
	                    'file_order' => $order++,
	                    'is_valid' => 1
	                );

	                //Captura o registro existente no banco
	                $file_db = $field->wtche->db->where('file_name', $file_name)->get($field->get_table())->row();
	                $path = 'uploads/' . $field->get_table();
	                if ($file_db) {
	                	if (!$field->wtche->db->where('file_name', $file_name)->update($field->get_table(), $file_data)) {
	                		return FALSE;
	                	}
	                } elseif ($field->wtche->db->insert($field->get_table(), $file_data)) {
	                	//Move o arquivo para a posição correta
	                	@rename($path . '/_temp/' . $file_name, $path . '/' . $file_name);
	                } else {
		            	return FALSE;
		            }

		            //Cria o preview em base 64
		            $file_db = $field->wtche->db->where('file_name', $file_name)->get($field->get_table())->row();
		            if (!$file_db->image_preview) {
	                	$field->wtche->load->helper('file');
	                	$original_image = $path . '/' . $file_name;
	                	$file_type = get_mime_by_extension($original_image);
	                	if ($file_type == 'image/jpeg' || $file_type == 'image/png') {
	                		list($original_width, $original_height) = getimagesize($original_image);
	                		$ratio = $original_width / $original_height;
	                		$preview_width = 48;
							$preview_height = ceil($preview_width / $ratio);
							$preview = imagecreatetruecolor($preview_width, $preview_height);
							if ($file_type == 'image/jpeg') {
								$source = imagecreatefromjpeg($original_image);
							} else {
								$source = imagecreatefrompng($original_image);
							}
							imagecopyresized($preview, $source, 0, 0, 0, 0, $preview_width, $preview_height, $original_width, $original_height);

							ob_start(); 
							imagejpeg($preview);
							$preview_data = ob_get_contents(); 
							imagedestroy($preview);
							ob_end_clean(); 

							$image_data = [
								'image_preview' => base64_encode($preview_data),
								'image_width' => $original_width,
								'image_height' => $original_height,
							];

							$field->wtche->db->where('file_name', $file_name)->update($field->get_table(), $image_data);
	                	}
		            }
	            }
	        }

	        return $post_data;

		});

		return parent::setFieldName($field_name);
	}

	public function forgeDB() {
		//Array com duas posições: 0=up 1=down
		if (!$this->wtche->db->table_exists($this->table_name)) {
			return ["
				\$this->load->dbforge();

				\$fields = array(
		          	'{$this->model->get_key()}' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'{$this->fk}' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'unsigned' => TRUE
					),
					'file_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_name_original' => array(
						'type' => 'VARCHAR',
						'constraint' => '255'
					),
					'file_order' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'is_valid' => array(
						'type' => 'SMALLINT',
						'default' => 0
					),
					'image_preview' => array(
						'type' => 'LONGTEXT',
		        		'null' => TRUE
					),
					'image_width' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					),
					'image_height' => array(
						'type' => 'INT',
						'constraint' => 11,
						'default' => 0
					)
		        );
				\$this->dbforge->add_field(\$fields);
				\$this->dbforge->add_key('{$this->model->get_key()}', TRUE);
				\$this->dbforge->create_table('{$this->table_name}');

				\$this->db->query('ALTER TABLE ' . '{$this->table_name}' . '
					ADD CONSTRAINT `' . md5('{$this->table_name}' . '{$this->model->get_table()}' . '{$this->fk}') . '`
					FOREIGN KEY (' . '{$this->fk}' . ')
					REFERENCES `' . '{$this->model->get_table()}' . '` (' . '{$this->model->get_key()}' . ')
					ON DELETE CASCADE
					ON UPDATE CASCADE');
			","
				
			"];
		}

	}

	public function get_table() {
		return $this->table_name;
	}
	public function fk() {
		return $this->fk;
	}

	/**
	 * Para o Upload os values são tratados diferente dos demais Fields
	 * @param [type] $value [description]
	 */
	public function setValue($value, $row_id) {

		$result = $this->wtche->db
                ->from($this->get_table())
                ->where($this->fk,$row_id)
                ->order_by('file_order')
                ->get()->result_object();

        $this->value = $result;

        return $this;

	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um File Upload //
	///////////////////////////////////////////////////////////////////////////

	public function setMask($mask_config) {
		throw new Exception("Campo File Upload não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo File Upload não pode ser definido com Máscara!", 1);
	}

}

/* End of file File_Upload.php */
/* Location: ./application/libraries/Form/Fields/File_Upload.php */