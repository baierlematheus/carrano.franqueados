<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campo tipo Char
 */
class Char extends Field {

	public function __construct() {

		parent::__construct();

		$this->field_forge_config = array(
			'type' => 'VARCHAR',
			'constraint' => 255, 
	        'null' => TRUE
	  	);

	}

	protected $type = 'text';
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	public function type() {
		return $this->type;
	}

	/**
	 * Habilita Data e Hora
	 **/
	protected $database_date_mask = null;
	protected $view_date_mask = null;
	protected $datepicker_format = null;
	public function setDateField() {

		$this->setMaxLength(10);
		$this->addCssClass('datepicker');
		$this->datepicker_format = 'dd/mm/yy';
		$this->setMask('00/00/0000');
		$this->setLabelHelper('dd/mm/aaaa');
		$this->setValidationRules(($this->validationRules()?$this->validationRules().'|':'') . 'matches_pattern[^([0-9]{4}-[0-9]{2}-[0-9]{2})?$]');
		$this->view_date_mask = 'd/m/Y';
		$this->database_date_mask = 'Y-m-d';

		$this->field_forge_config = array(
			'type' => 'DATE'
	  	);

	  	if (!$this->isRequired()) {
	  		$this->field_forge_config['null'] = TRUE;
	  	}

		return $this;
	}
	public function setDateTimeField() {

		$this->setMaxLength(19);
		$this->addCssClass('datepicker');
		$this->datepicker_format = 'dd/mm/yy 00:00:00';
		$this->setMask('00/00/0000 00:00:00');
		$this->setLabelHelper('dd/mm/aaaa hh:mm:ss');
		$this->setValidationRules(($this->validationRules()?$this->validationRules().'|':'') . 'matches_pattern[^([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})?$]');
		$this->view_date_mask = 'd/m/Y H:i:s';
		$this->database_date_mask = 'Y-m-d H:i:s';

		$this->field_forge_config = array(
			'type' => 'DATETIME'
	  	);

	  	if (!$this->isRequired()) {
	  		$this->field_forge_config['null'] = TRUE;
	  	}

		return $this;
	}
	public function setDatabaseDateMask($database_date_mask) {
		$this->database_date_mask = $database_date_mask;
		return $this;
	}
	public function setViewDateMask($view_date_mask) {
		$this->view_date_mask = $view_date_mask;
		return $this;
	}
	public function datepickerFormat() {
		return $this->datepicker_format;
	}

	/**
	 * Define o valor do campo
	 * Caso tenha alguma regra especial é feita aqui
	 **/
	public function setValue($value) {

		parent::setValue($value);

		//Se não estiver no formato desejado para o banco, vamos formatar:
		if ($this->view_date_mask && $date = DateTime::createFromFormat($this->view_date_mask, $value)) {
			$this->value = $date->format($this->database_date_mask);
		} else {
			if ($this->view_date_mask && empty($value)) {
				$this->value = null;
			} else {
				$this->value = $value;
			}
		}

		return $this;
	}
	public function value($row = null, $rawValue = false) {

		if ($rawValue) {
			return $this->value;
		}

		if (is_callable($this->value_callback)) {
			return $this->value_callback($row);
		}

		//Se não estiver no formato desejado para mostrar, vamos formatar:
		if ($this->database_date_mask && $date = DateTime::createFromFormat($this->database_date_mask, $this->value)) {
			return $date->format($this->view_date_mask);
		} else {
			return $this->value;
		}

	}

}

/* End of file Char.php */