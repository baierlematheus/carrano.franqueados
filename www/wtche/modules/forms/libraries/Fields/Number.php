<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campo tipo Number
 */
class Number extends Field {

	public function __construct()
	{
		parent::__construct();
		
		//Seta a Máscara padrão para somente Números
		$this->setDecimalDigits(0);

		$this->field_forge_config = array(
			'type' => 'INT',
			'constraint' => 11, 
	        'null' => TRUE
	  	);

	}

	/**
	 * Quantidade de dígitos após a "virgula" para valor decimal (dinheiro, etc)
	 **/
	protected $decimal_digits = 0;
	public function setDecimalDigits($decimal_digits) {
		$this->decimal_digits = $decimal_digits;
		if ($decimal_digits) {
			$this->setMask("#{$this->thousandViewSeparator()}##0{$this->decimalViewSeparator()}" . str_repeat("0", $decimal_digits));
			$this->setMaskConfig('{reverse: true}');
		} else {
			$this->setMask('9#');
			$this->setMaskConfig('{maxlength: false}');
		}

		$this->field_forge_config['type'] 		= "DECIMAL";
		$this->field_forge_config['constraint'] = "10,{$decimal_digits}";

		return $this;
	}
	public function decimalDigits() {
		return $this->decimal_digits;
	}

	/**
	 * Separador decimal para visualização
	 **/
	protected $decimal_view_separator = ',';
	public function setDecimalViewSeparator($decimal_view_separator) {
		$this->decimal_view_separator = $decimal_view_separator;
		return $this;
	}
	public function decimalViewSeparator() {
		return $this->decimal_view_separator;
	}

	/**
	 * Separador de milhares para visualização
	 **/
	protected $thousand_view_separator = '.';
	public function setThousandViewSeparator($thousand_view_separator) {
		$this->thousand_view_separator = $thousand_view_separator;
		return $this;
	}
	public function thousandViewSeparator() {
		return $this->thousand_view_separator;
	}

	/**
	 * Separador decimal para banco
	 **/
	protected $decimal_database_separator = '.';
	public function setDecimalDatabaseSeparator($decimal_database_separator) {
		$this->decimal_database_separator = $decimal_database_separator;
		return $this;
	}
	public function decimalDatabaseSeparator() {
		return $this->decimal_database_separator;
	}

	/**
	 * Separador de milhares para banco
	 **/
	protected $thousand_database_separator = '';
	public function setThousandDatabaseSeparator($thousand_database_separator) {
		$this->thousand_database_separator = $thousand_database_separator;
		return $this;
	}
	public function thousandDatabaseSeparator() {
		return $this->thousand_database_separator;
	}

	/**
	 * Guarda o valor do campo
	 * No caso dos campos Number vamos fazer umas tretas com a máscara
	 * pra poder salvar direito no banco
	 **/
	public function setValue($value) {

		parent::setValue($value);

		//Se não estiver no formato desejado para o banco, vamos formatar:
		if ($this->decimalDigits() && !preg_match("/^([0-9]|" . $this->thousandDatabaseSeparator() . ")+\\" . $this->decimalDatabaseSeparator() . "([0-9]{" . $this->decimalDigits() . "})$/", $value)) {
			$source = array($this->thousandViewSeparator(), $this->decimalViewSeparator());
	        $replace = array($this->thousandDatabaseSeparator(), $this->decimalDatabaseSeparator());
			$this->value = str_replace($source, $replace, $value);
			if (!is_numeric($this->value)) {
				$this->value = null;
			}
		} else {
			if (is_numeric($value)) {
				$this->value = $value;
			} else {
				$this->value = null;
			}
		}

		return $this;
	}

}

/* End of file Number.php */