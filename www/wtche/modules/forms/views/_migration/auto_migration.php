<?php 
$module_name = ucwords($module_name);

$fields = json_encode($forge_fields);

$up_fields = "";
if (!empty($forge_fields)) {
	$up_fields = "
		\$this->fields = json_decode('{$fields}', true);
		\$this->dbforge->add_column('{$table_name}', \$this->fields);
	";
}

$down_fields = "";
if (!empty($forge_fields)) {
	$down_fields = "
		\$this->fields = json_decode('{$fields}', true);
		foreach (\$this->fields as \$key => \$field)
		{
			\$this->dbforge->drop_column('{$table_name}', \$key);
		}
	";
}

$up = "";
foreach ($forge_fields_internal as $forge_field) {
	$up .= $forge_field[0] . "\n\n";
}

$down = "";
foreach ($forge_fields_internal as $forge_field) {
	$down .= $forge_field[1] . "\n\n";
}

echo "<?php defined('BASEPATH') || exit('No direct script access allowed');

//Generated at: {$generated_at} 

class Migration_Crud_{$module_name}_Auto_{$context}_{$generated_at} extends Migration
{

	private \$fields = array();

	public function up()
	{
		{$up_fields}
		{$up}
	}

	public function down()
	{
		{$down_fields}
		{$down}
	}
	
}";