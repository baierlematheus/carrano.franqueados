<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label() . ($field->isRequired()?'<strong class="text-danger"> *</strong>':'') .'<span class="help-block">' . $field->labelHelper() . '</span>', $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<input <?php echo $field->isStatic()?'disabled':'' ?> <?php echo ($field->datepickerFormat())?"data-datepicker-format='{$field->datepickerFormat()}'":"" ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?>" class="form-control <?php echo $field->cssClasses() ?>" type="<?php echo $field->type(); ?>" value="<?php echo set_value($field->fieldName(), $field->value($form_data)); ?>" />
		<?php echo '<span class="help-block">' . $field->fieldHelper() . '</span>'; ?>
	</div>

</div>

<script type="text/javascript">
	$(function(){

		var field = $('input[name="<?php echo $field->fieldName() ?>"]');
		
		<?php if ($field->mask()): ?>
			field.mask('<?php echo $field->mask() ?>', <?php echo $field->maskConfig() ?>);
		<?php endif ?>

		<?php if ($field->maxLength()): ?>
			field.inputlimiter({
				limit: <?php echo $field->maxLength(); ?>,
				boxAttach: false
			});
		<?php endif ?>

	});
</script>