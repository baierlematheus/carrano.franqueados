<div class="input-group">
	<input <?php echo (!$field->isInlineEdit() || !$can_edit)?'readonly':'' ?> <?php echo ($field->datepickerFormat())?"data-datepicker-format='{$field->datepickerFormat()}'":"" ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?>" data-key="{{key}}" class="form-control <?php echo $field->cssClasses() ?>" type="<?php echo $field->type(); ?>" style="min-width: 100px;" value="{{data}}" />
</div>

<script type="text/javascript">

	(function() { 

		var field = $('input[name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

		<?php if ($field->isInlineEdit() && $can_edit): ?>

			field.on('change', function( event, ui ) {
				if (!field.validationEngine('validate')) { //Sim! É ao contrário mesmo...
					$.ajax({
						type: "POST",
						url: '<?php echo site_url($field->wtche->uri->uri_string."/save_ajax") ?>/{{key}}',
						data: {
							<?php echo $field->fieldName() ?>: field.val()
						},
						success: function(data){
							_field_update_status(field, 'Campo <?php echo $field->label() ?> atualizado', 'success');
						},
						error: function(data){
							$.each(data.responseJSON, function(index, message) {
								_field_update_status(field, message, 'error');
							});
						},
						dataType: "json"
					});
				}
			});

			<?php if ($field->maxLength()): ?>
				field.inputlimiter({
					limit: <?php echo $field->maxLength(); ?>,
					boxAttach: false
				});
			<?php endif ?>

		<?php endif ?>

		<?php if ($field->mask()): ?>
			field.mask('<?php echo $field->mask() ?>', <?php echo $field->maskConfig() ?>);
		<?php endif ?>

	})();

</script>