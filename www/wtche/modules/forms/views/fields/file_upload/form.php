<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label() . ($field->isRequired()?'<strong class="text-danger"> *</strong>':'') .'<span class="help-block">' . $field->labelHelper() . '</span>', $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<div class="file_upload">
			<ul class="files">
				
			</ul>
			<input type="file" name="<?php echo $field->fieldName() ?>" class="<?php echo $field->cssClasses() ?>" multiple>
		</div>
		<?php echo '<span class="help-block" style="float: left; width: 100%;">' . $field->fieldHelper() . '</span>'; ?>
	</div>

</div>

<script type="text/javascript">

	/*Usadas APENAS para criar o filtro dos sumodulos*/
	function serialize(mixed_value) {var val, key, okey, ktype = '', vals = '', count = 0, _utf8Size = function(str) {var size = 0, i = 0, l = str.length, code = ''; for (i = 0; i < l; i++) {code = str.charCodeAt(i); if (code < 0x0080) {size += 1; } else if (code < 0x0800) {size += 2; } else {size += 3; } } return size; }, _getType = function(inp) {var match, key, cons, types, type = typeof inp; if (type === 'object' && !inp) {return 'null'; } if (type === 'object') {if (!inp.constructor) {return 'object'; } cons = inp.constructor.toString(); match = cons.match(/(\w+)\(/); if (match) {cons = match[1].toLowerCase(); } types = ['boolean', 'number', 'string', 'array']; for (key in types) {if (cons === types[key]) {type = types[key]; break; } } } return type; }, type = _getType(mixed_value); switch (type) {case 'function': val = ''; break; case 'boolean': val = 'b:' + (mixed_value ? '1' : '0'); break; case 'number': val = (Math.round(mixed_value) === mixed_value ? 'i' : 'd') + ':' + mixed_value; break; case 'string': val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"'; break; case 'array': case 'object': val = 'a'; for (key in mixed_value) {if (mixed_value.hasOwnProperty(key)) {ktype = _getType(mixed_value[key]); if (ktype === 'function') {continue; } okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key); vals += this.serialize(okey) + this.serialize(mixed_value[key]); count++; } } val += ':' + count + ':{' + vals + '}'; break; case 'undefined': default: val = 'N'; break; } if (type !== 'object' && type !== 'array') {val += ';'; } return val; };
	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

	(function() { 

		var previewSize = 102;
		var field = 'input[name="<?php echo $field->fieldName() ?>"]';
		var form = $(field).closest('form');

		$(function(){

			var obj_data = [];

			<?php if ($field->value()): ?>
				obj_data = $.parseJSON('<?php echo json_encode($field->value()) ?>');
			<?php endif ?>

			$.each(obj_data, function(i, data) {
				
				getFileBox(data, function(filebox) {
					$(field).parents('.file_upload').find('.files').append(filebox);
				}, form);

			});

			$(field).data('numberOfFiles', $(field).closest('.file_upload').find('ul.files li').size());

			$(field).fileupload({
				url: '<?php echo site_url(ADMIN_AREA . "/" . $context_name . "/" . $module_name) ?>/do_upload',
				sequentialUploads: true,
				dropZone: field,
				<?php if ($field->allowedTypes() != '*'): ?>
					acceptFileTypes: /(\.|\/)(<?php echo $field->allowedTypes() ?>)$/i,
				<?php endif ?>
				maxFileSize: <?php echo $field->maxSize()*1000 ?>,
				maxNumberOfFiles: <?php echo $field->maxFiles() ?>,
				getNumberOfFiles: function() {
					console.log('getNumberOfFiles: ' + $(field).data('numberOfFiles'));
					return $(field).data('numberOfFiles');
				},
				//Configs dos Previews
				previewMaxWidth: previewSize,
		        previewMaxHeight: previewSize,
		        previewCrop: true,
			    //Form Data
		        formData: {
		        	<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
		        	key: '<?php echo (isset(${$module_name}))?${$module_name}->{$key}:"" ?>',
		        	field_name: '<?php echo $field->fieldName() ?>'
		        }
		    }).on('fileuploadadd', function (e, data) {

		    	var form = $(field).closest('form');

		    	data.context = renderFileBox(null, form);
		    	$(field).parents('.file_upload').find('.files').append(data.context);

		    }).on('fileuploadprocessalways', function (e, data) {

		        var index = data.index,
		            file = data.files[index];

		        if (file.preview) {
		            data.context.find('.canvas')
		                .prepend($(file.preview).css('border-radius', 2));
		        } else {
					data.context.find('.canvas')
						.prepend($('<img>')
							.attr('src', '<?php echo site_url("preview") ?>/<?php echo $field->get_table() ?>/0/' + previewSize + '/' + file.name)
							.css('border-radius', 2)
						);
		        }

		        if (!data.files[0].error) {
		        	$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) + 1);
		        } else if(data.files[0].error) {
		        	setFileError(data, data.files[0].error);
		        }

		    }).on('fileuploadprogress', function (e, data) {

		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        data.context.find('.progress .progress-bar')
		        	.width(progress+'%')
		        	.attr({
		        		'aria-valuenow': progress
		        	});

		    }).on('fileuploaddone', function (e, data) {

		    	data.context.find('.progress').remove();
		    	data.context.find('.canvas').fadeTo('400', 1);
		    	getFileBox(data.result, function(filebox) {
					data.context.replaceWith(filebox);
				}, form);
		    	

		    }).on('fileuploadfail', function (e, data) {
		    	
		    	$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) - 1);
		    	setFileError(data, data.jqXHR.responseJSON.error);

		    });

		    $('.file_upload .files').sortable({
				update: function(event, ui) {
					setFilesOrder($(this));
				}
			});

			function setFilesOrder(files_obj) {
				files_obj.find('li').each(function(index, el) {
					var input = $('input[type="hidden"][name="' + $(el).attr('data-field-name') + '[' + $(el).attr('data-file-name') + '][orig_name]"]');
					console.log(input);
					input.appendTo(form);
				});
			}

			function setFileError(data, error_message) {
				data.context
					.attr('data-original-title',error_message).tooltip()
					.append($('<i>')
						.addClass('icon-close')
						.css({
			    			color: '#E48561',
			    			position: 'absolute',
			    			right: 3,
			    			top: 3,	
			    			'font-size': 10,
			    			cursor: 'pointer'
			    		})
			    		.click(function(){
			    			data.context.tooltip("destroy");
			    			data.context.remove();
			    		})
			    	)
					.find('.progress').remove();
			    _field_update_status(null, error_message, 'error');
			}

			function getFileBox(file, callback, form) {

				if ($.isPlainObject(file)) {
					callback(renderFileBox(file, form));
				} else if (file != null) {
					$.ajax({
						type: "GET",
						url: '<?php echo site_url($field->wtche->uri->uri_string."/get_file/".$field->fieldName()) ?>/' + file,
						success: function(data){
							callback(renderFileBox(data, form));
						},
						error: function(data){
							//?
						},
						dataType: "json"
					});
				}

			}

			function renderFileBox(file, form) {

				if (file == null) {
					file = {};
				}

				//Cria o box principal
				var box = $('<li>')
						.attr('data-key', file.<?php echo $field->model->get_key() ?>)
						.attr('data-field-name', "<?php echo $field->fieldName() ?>")
						.attr('data-file-name', file.file_name || '')
						.addClass('context')
						.css({
							width: previewSize,
							height: previewSize
						})
						.append($('<div>')
							.addClass('canvas')
							.css({
								width: previewSize,
								height: previewSize
							})
						);

				//Se tiver arquivo já ele cria o box completo
			    if (file.file_name) {

			    	if (file.<?php echo $field->model->get_key() ?>) {
			    		box.find('.canvas').wrap(
			    			$('<a>')
		    					.attr('href','<?php echo site_url("preview") ?>/<?php echo $field->get_table() ?>/0/0/' + file.file_name)
		    					.addClass('lightbox')
	    						.attr('target', '_blank')
			    				.fancybox()
						).append($('<img>')
			        		.attr('src','<?php echo site_url("preview") ?>/<?php echo $field->get_table() ?>/0/' + previewSize + '/' + file.file_name)
			        		.css({
								width: previewSize,
								height: previewSize,
								'border-radius': 2
							})
			        	);
			    	} else {
			    		box.find('.canvas').wrap(
			    			$('<a>')
		    					.attr('href','<?php echo site_url("preview") ?>/<?php echo $field->get_table() ?>/_temp/0/' + file.file_name)
		    					.addClass('lightbox')
	    						.attr('target', '_blank')
			    				.fancybox({
									padding: 1
								})
						).append($('<img>')
			        		.attr('src','<?php echo site_url("preview") ?>/<?php echo $field->get_table() ?>/_temp/' + previewSize + '/' + file.file_name)
			        		.css({
								width: previewSize,
								height: previewSize,
								'border-radius': 2
							})
			        	);
			    	}

			    	
			    	box.append($('<a>')
						.addClass('btn btn-icon btn-danger icon-close tip')
						.attr('data-original-title', 'Remover')
						.click(function(){
							bootbox.dialog({
							  message: "Tem certeza que deseja deletar este arquivo?",
							  title: "Atenção",
							  buttons: {
							    main: {
							      label: "Cancelar",
							      className: "btn-default"
							    },
							    danger: {
							      label: "Deletar",
							      className: "btn-danger",
							      callback: function() {
							      	$.ajax({
										type: "POST",
										url: '<?php echo site_url(ADMIN_AREA . '/' . $context_name.'/'.$module_name."/delete_upload") ?>',
										data: {
											'file_name': file.file_name,
											'field_name': '<?php echo $field->fieldName() ?>'
										},
										success: function(data){
											box.remove();
											$('input[type="hidden"][name="<?php echo $field->fieldName() ?>[' + file.file_name + '][orig_name]"]').remove();
											$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) - 1);
											_field_update_status(null, 'Arquivo deletado', 'success');
										},
										error: function(data){
											_field_update_status(null, 'Erro ao deletar o arquivo', 'error');
										},
										dataType: "json"
									});
							      }
							    }
							  }
							});
						})
			    	);

			    	<?php foreach ($field->submodules() as $submodule): ?>
			    		<?php 
			    			if (!$this->auth->has_permission(ucfirst($submodule->module_name) . '.' . ucfirst($context_name) . '.View')) {
			    				continue;
			    			}
			    		?>

			    		var filter = [];
			    		filter['<?php echo $submodule->parent_field_name ?>'] = file.id;

			    		filter = serialize(filter);
			    		filter = Base64.encode(filter);
			    		if (file.id) {
			    			box.append($('<a>')
								.addClass('btn btn-icon btn-default tip')
				    			.attr('href', '<?php echo site_url(ADMIN_AREA . "/{$context_name}/{$submodule->module_name}?template=form&filter=") ?>' + filter)
				    			.addClass('lightbox fancybox.iframe')
								.attr('data-original-title', '<?php echo $submodule->title ?>')
								.addClass('<?php echo $submodule->icon ?>')
					    	);
			    		}
			    	<?php endforeach ?>
			    	
			    	form.append($('<input>')
			    		.attr('type','hidden')
			    		.attr('value',file.orig_name || file.file_name_original)
			    		.attr('name','<?php echo $field->fieldName() ?>[' + file.file_name + '][orig_name]')
			    	);

			    //Senão ele cria uma barra de progresso pois está fazendo upload
			    } else {
			    	box.find('.canvas').css('opacity','0.5');
					box.append($('<div>')
						.addClass('progress progress-micro')
						.append($('<div>')
							.addClass('progress-bar progress-bar-info')
							.attr({
								'role': 'progressbar',
								'aria-valuenow': '0',
								'aria-valuemin': '0',
								'aria-valuemax': '100'
							})
						)
					);
			    }
							
			    return box;
			}

		});

	})();

</script>