<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label() . ($field->isRequired()?'<strong class="text-danger"> *</strong>':'') .'<span class="help-block">' . $field->labelHelper() . '</span>', $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<textarea <?php echo $field->isStatic()?'disabled':'' ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" rows="10" class="form-control <?php echo $field->cssClasses() ?>" name="<?php echo $field->fieldName() ?>"><?php echo set_value($field->fieldName(), $field->value($form_data)); ?></textarea>
		<?php echo '<span class="help-block">' . $field->fieldHelper() . '</span>'; ?>

	</div>

</div>

<script type="text/javascript">
	$(function(){

		var field = $('textarea[name="<?php echo $field->fieldName() ?>"]');

		<?php if ($field->maxLength()): ?>
		field.inputlimiter({
			limit: <?php echo $field->maxLength(); ?>,
			boxAttach: false
		});
		<?php endif ?>


		<?php
				if ($field->config_rich != ''){
		?>		
			$('textarea.richpersonalized').summernote({
				<?php echo $field->config_rich; ?>
			});
		<?php
		    	}
		?>

});
</script>