<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label() . ($field->isRequired()?'<strong class="text-danger"> *</strong>':'') .'<span class="help-block">' . $field->labelHelper() . '</span>', $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<a href="<?php echo site_url(ADMIN_AREA .'/'. ($field->context()?:$context_name) .'/'. $field->module() . '?template=form&filter=' . $field->value()) ?>" class="btn lightbox fancybox.iframe <?php echo $field->cssClasses() ?>"><?php echo $field->buttonText()?$field->buttonText():$field->label() ?></a>
	</div>

</div>