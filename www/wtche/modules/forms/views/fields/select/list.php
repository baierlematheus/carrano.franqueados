<div>
	<select <?php echo ($field->dependencyKey())?"data-dependency_field='{$field->dependencyField()}'":'' ?> <?php echo iif($field->relationMultiple(), 'multiple="multiple"') ?> <?php echo iif(!$field->isInlineEdit() || ($field->dependencyKey() && !$field->dependencyLoaded()), 'disabled') ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?>" data-key="{{key}}" class="select2 <?php echo $field->cssClasses() ?>">
		<?php if ($field->dependencyKey() && !$field->dependencyLoaded()): ?>
			<option><?php echo $field->dependencyTextHelper() ?></option>
		<?php else: ?>
			<option><?php echo $field->firstItemText() ?></option>
		<?php endif ?>
		<?php foreach ($field->items() as $key => $value): ?>
			<option <?php echo ($field->dependencyKey())?"data-dependency_value='{$value->{$field->dependencyKey()}}'":'' ?> value="<?php echo $key ?>"><?php echo $value->text ?></option>
		<?php endforeach ?>
	</select>

	<script type="text/javascript">

		$(function(){

			var field = $('select[name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

			var obj_data = [];

			if ('{{data}}') {
				obj_data = $.parseJSON('{{data}}');
			}

			field.change(function(){
				var child = $('select[data-key="{{key}}"][data-dependency_field="<?php echo $field->fieldName() ?>"]');
				if (child.is('select') && !child.val()) {
					field.val(child.find('option:selected').attr('data-dependency_value')).trigger('change');
				} else {
					var dependency_field = $('select[data-key="{{key}}"][name="<?php echo $field->dependencyField() ?>"]');
					dependency_field.val(field.find('option:selected').attr('data-dependency_value')).trigger('change');
				}
			});

			$.each(obj_data, function(i, data) {
				field.find('option[value="' + i + '"]').attr('selected', true).trigger('change');
			});

			<?php if($field->isInlineEdit()): ?>
				field.on('change', function( event, ui ) {
					if (!field.validationEngine('validate')) { //Sim! É ao contrário mesmo...
						$.ajax({
							type: "POST",
							url: '<?php echo site_url($field->wtche->uri->uri_string."/save_ajax") ?>/{{key}}',
							data: {
								<?php echo $field->fieldName() ?>: field.val()
							},
							success: function(data){
								_field_update_status(field, 'Campo <?php echo $field->label() ?> atualizado', 'success');
							},
							error: function(data){
								$.each(data.responseJSON, function(index, message) {
									_field_update_status(field, message, 'error');
								});
							},
							dataType: "json"
						});
					}
				});
			<?php endif ?>

		});

	</script>

</div>

