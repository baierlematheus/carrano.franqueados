<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">
	<?php if ($field->relationMultiple()): ?>
		<input type="hidden" name="<?php echo $field->fieldName() ?>[]" value="0" />
	<?php endif ?>
	<?php echo form_label($field->label() . ($field->isRequired()?'<strong class="text-danger"> *</strong>':'') .'<span class="help-block">' . $field->labelHelper() . '</span>', $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<select <?php echo $field->isStatic()?'disabled':'' ?> <?php echo ($field->dependencyField())?"data-dependency_field='{$field->dependencyField()}'":'' ?> <?php echo iif($field->relationMultiple(), 'multiple="multiple"') ?> <?php echo ($field->dependencyKey() && !$field->dependencyLoaded())?'disabled':'' ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?><?php echo iif($field->relationMultiple(), '[]') ?>" class="select2 <?php echo $field->cssClasses() ?>">
            <?php if ($field->dependencyKey() && !$field->dependencyLoaded()): ?>
				<option value=""><?php echo $field->dependencyTextHelper() ?></option>
			<?php else: ?>
				<?php if (!$field->relationMultiple()): ?>
					<option value=""><?php echo $field->firstItemText() ?></option>
				<?php endif ?>
			<?php endif ?>
			<?php foreach ($field->items() as $key => $value): ?>
				<option <?php echo ($field->dependencyKey())?"data-dependency_value='{$value->{$field->dependencyKey()}}'":'' ?> <?php echo set_select($field->fieldName(),$key,($field->value() && array_key_exists($key, $field->value())));?> value="<?php echo $key ?>"><?php echo $value->text ?></option>
			<?php endforeach ?>
        </select>
	</div>

	<script type="text/javascript">

		$(function(){

			var field = $('select[name="<?php echo $field->fieldName() ?>"]');
			var dependency_field = $('select[name="<?php echo $field->dependencyField() ?>"]');

			$(document).off('seleciona','select[name="<?php echo $field->fieldName() ?>"]').on('seleciona', 'select[name="<?php echo $field->fieldName() ?>"]', function(){
				dependency_field.val(field.find('option:selected').attr('data-dependency_value')).trigger('change');
			});

			<?php if ($field->dependencyField() && $field->value(null, true) && !$field->dependencyLoaded()): ?>
				if (dependency_field.is('select')) {
					dependency_field.val(field.find('option:selected').attr('data-dependency_value')).trigger('seleciona');
				}
			<?php endif ?>

			<?php if ($field->dependencyField()): ?>
				
				$(document).off('change','select[name="<?php echo $field->dependencyField() ?>"]').on('change', 'select[name="<?php echo $field->dependencyField() ?>"]', function(){
					
					//Busca o novo ele mesmo quando o pai trocar de valor
					$.ajax({
					  	url: '<?php echo site_url(ADMIN_AREA . "/" . $field->wtche->context_name . "/" . $field->wtche->module_name) ?>/get_select/form',
						data: {
							field_name: '<?php echo $field->fieldName() ?>',
							filter_key: '<?php echo $field->dependencyKey() ?>',
							filter_value: $('select[name="<?php echo $field->dependencyField() ?>"]').find('option:selected').attr('value'),
							field_value: '<?php echo $field->value(null, true) ?>',
						}
					}).done(function(res) {
						var current_value = field.find('option:selected').attr('value');
						field.closest('.form-group').replaceWith(res);
						field = $('select[name="<?php echo $field->fieldName() ?>"]');
						if (current_value && field.find('option[value="' + current_value + '"]').is('option')) {
							field.val(current_value);
						}
						field.trigger('change');
						_ui();
					});
					
				});
			<?php endif ?>

		});

	</script>
</div>

