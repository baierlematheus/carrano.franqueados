<div class="page-header">
	<div class="page-title">
		<h1>
			<?php echo $module_title ?>
			<small>
				<h5><?php echo $description ?></h5>
			</small>
		</h1>
	</div>
	<div class="pull-right range">
	
		<a class="btn btn-default btn-large <?php echo $this->uri->segment(4) == '' ? 'active disabled' : '' ?>" href="<?php echo site_url(ADMIN_AREA .'/' . $context_name . '/' . $module_name . (($query_string)?('?'.$query_string):'')) ?>" id="list">
			<?php echo lang('module_list'); ?>
		</a>
		<?php if ($this->auth->has_permission(ucfirst($module_name) . '.' . $context_name . '.Create')) : ?>
			<a class="btn btn-success <?php echo $this->uri->segment(4) == 'create' ? 'active disabled' : '' ?>" href="<?php echo site_url(ADMIN_AREA .'/' . $context_name . '/' . $module_name . '/create' . (($query_string)?('?'.$query_string):'')) ?>" id="create_new">
				<?php echo lang('module_new'); ?>
			</a>
		<?php endif; ?>

	</div>
</div>