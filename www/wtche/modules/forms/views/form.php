<?php echo form_open($this->uri->uri_string() . (($query_string)?('?'.$query_string):''), 'class="form-horizontal form-bordered"'); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
        <div class="panel-body">

        	<?php foreach ($fields as $field): ?>
        		<?php echo $field->render('form', $form_data) ?>
			<?php endforeach ?>

        </div>
    </div>

    <div class="form-actions text-right">
		<?php echo anchor(ADMIN_AREA .'/' . $context_name . '/' . $module_name . (($query_string)?('?'.$query_string):''), lang('module_cancel'), 'class="btn btn-warning"'); ?>
    	<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('module_action_save'); ?>"  />
    </div>

<?php echo form_close(); ?>