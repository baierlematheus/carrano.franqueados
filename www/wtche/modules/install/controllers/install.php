<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        //Troca o Assets Base Folder
        $this->config->set_item('assets.base_folder', $this->config->item('assets.base_folder') . '/_admin');

        // Load basics since we are not relying on
        // Base_Controller here...
        $this->load->library('template');
        $this->load->library('assets');
        $this->load->library('events');
        $this->load->helper('application');
        $this->lang->load('application');

    }

    //--------------------------------------------------------------------

    public function index()
    {

        $this->load->library('installer_lib');

        $data = array();

        $data['environment'] = ENVIRONMENT;

        // PHP Version Check
        $data['php_min_version']    = '5.5';
        $data['php_acceptable']     = $this->installer_lib->php_acceptable($data['php_min_version']);
        $data['php_version']        = $this->installer_lib->php_version;

        // Curl Enabled?
        $data['curl_enabled']       = $this->installer_lib->cURL_enabled();
        
        // GD Enabled?
        $data['gd_enabled']         = $this->installer_lib->gd_enabled();

        // JSON Enabled?
        $data['json_enabled']       = $this->installer_lib->json_enabled();

        // Mcrypt Enabled?
        $data['mcrypt_enabled']     = $this->installer_lib->mcrypt_enabled();

        // Files/Folders writeable?
        $data['folders']            = $this->installer_lib->check_folders();
        $data['files']              = $this->installer_lib->check_files();

        $this->load->view('index', $data);
    }

    //--------------------------------------------------------------------

    public function import()
    {
        $this->load->library('installer_lib');
        $this->load->helper('form');

        $data = array();

        if (!$this->installer_lib->is_installed() && $this->input->post('importar'))
        {

            $config['upload_path'] = APPPATH . '../uploads/';
            $config['allowed_types'] = '*';
            $config['file_name'] = 'import.sql';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                $data['import_error'] = $this->upload->display_errors();
            }
            else
            {
                redirect('install/do_install/import', 'refresh');
            }

        }
        else
        {
            $data['installed'] = 'O WTchê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".';
        }
    
        $this->load->view('import', $data);

    }

    public function create_db() {
        $this->load->library('installer_lib');
        $this->installer_lib->create_db();
        redirect(site_url('install/import'),'refresh');
    }

    //--------------------------------------------------------------------

    /**
     * Handles the basic installation of the migrations into the database
     * if available, and displays the current status.
     *
     * @return void
     */
    public function do_install($import = false)
    {

        $this->load->library('installer_lib');


        if ($this->installer_lib->is_installed())
        {
            die('O WTchê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".');
        }

        if (!$import) {
            // Does the database table even exist?
            if ($this->installer_lib->db_settings_exist === FALSE)
            {
                show_error("Erro nas configurações de banco de dados!");
            }
        }

        if ($this->installer_lib->setup($import, $this->input->post())) {
            define('BF_DID_INSTALL', true);
        } else {
            define('BF_DID_INSTALL', false);
        }

        $this->load->view('do_install');
    }

}