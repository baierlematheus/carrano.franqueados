<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends Base_Controller {

    public function index()
    {

    	$this->config->set_item('migrate.auto_core', true);
    	$this->config->set_item('migrate.auto_app', true);

        $this->load->library('migrations/migrations');
        $this->migrations->auto_latest();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode("OK"));

    }

}