<?php

    //Assets::clear_cache();

    Assets::add_module_css('install', array(
        'bootstrap.css',
        'install.css',
    ));

    Assets::add_module_js('install', array( 
        'bootstrap.js',
    ), 'external', true);
?>

<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Instalação do WTchê</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="robots" content="noindex" />
    
    <?php echo Assets::css(); ?>

    <?php echo Assets::js(); ?>

</head>

<body>

    <div class="container">

        <div class="header">

            <ul class="nav nav-pills pull-right">
              <?php if (ENVIRONMENT == 'development'): ?>
                <span class="label label-info" style="font-size: 27px;">Em Desenvolvimento</span>
              <?php elseif (ENVIRONMENT == 'testing'): ?>
                <span class="label label-warning" style="font-size: 27px;">Em Teste</span>
              <?php elseif (ENVIRONMENT == 'production'): ?>
                <span class="label label-danger" style="font-size: 27px;">Em Produção</span>
              <?php endif ?>
            </ul>

            <h1 class="text-muted">WTchê Framework</h1>
            
        </div>

