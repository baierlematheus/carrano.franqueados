<?php echo $this->load->view('includes/header'); ?>

    <?php if (BF_DID_INSTALL): ?>
        <div class="jumbotron">
            <h1>Mas que taL!</h1>
            <p class="lead">Tu instalou o WTchê!</p>
            <p><a class="btn btn-lg btn-success" href="<?php echo site_url('manager') ?>" role="button">Ir para o Manager</a></p>
        </div>
    <?php else: ?>
        <div class="jumbotron">
            <h1 class="text-danger">Tchê! Agora me caiu os butiá do bolso...</h1>
            <p class="lead">Deu algo errado na instalação! Larga o mate e resolve...</p>
            <p><a class="btn btn-lg btn-danger" href="<?php echo site_url('install') ?>" role="button">Voltar</a></p>
        </div>
    <?php endif ?>

<?php echo $this->load->view('includes/footer'); ?>