<?php echo $this->load->view('includes/header'); ?>

    <div class="jumbotron">
        <h1>Daí Tchê!</h1>
        <p>
        	Dá um confere nos apetrecho e vê se não falta nada pra rodar esse projeto macanudo!
        </p>
    </div>

    <div class="well">

        <table class="table table-hover" style="width: 100%;">
            <tbody>
                <!-- PHP Version -->
                <tr>
                    <td>Versão do PHP <?php echo $php_min_version ?>+</td>
                    <td style="width: 10em"><?php echo $php_acceptable ? '<span class="label label-success">' : '<span class="label label-danger">'; ?><?php echo $php_version ?></span></td>
                </tr>
                <tr>
                    <td>cURL</td>
                    <td><?php echo $curl_enabled ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>'; ?></td>
                </tr>
                <tr>
                    <td>GD</td>
                    <td><?php echo $gd_enabled ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>'; ?></td>
                </tr>
                <tr>
                    <td>JSON</td>
                    <td><?php echo $json_enabled ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>'; ?></td>
                </tr>
                <tr>
                    <td>Mcrypt</td>
                    <td><?php echo $mcrypt_enabled ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>'; ?></td>
                </tr>
                <?php
                    if (!$curl_enabled || !$php_acceptable || !$gd_enabled || !$json_enabled || !$mcrypt_enabled) {
                        $can_install = false;
                    }
                ?>
                <!-- Folders -->
                <tr><td colspan="2" class="text-muted" style="text-align: center">Pastas (Permissão de escrita)</td></tr>

                <?php foreach ($folders as $folder => $perm) :?>
                    <?php 
                        if (!$perm) {
                            $can_install = false;
                        }
                    ?>
                <tr>
                    <td><?php echo $folder ?></td>
                    <td><?php echo $perm ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>' ?></td>
                </tr>
                <?php endforeach; ?>

                <!-- Files -->
                <?php if ($files): ?>
                	
                    <tr><td colspan="2" class="text-muted" style="text-align: center">Arquivos (Permissão de escrita)</td></tr>

                    <?php foreach ($files as $file => $perm) :?>
                        <?php 
                            if (!$perm) {
                                $can_install = false;
                            }
                        ?>
	                    <tr>
	                        <td><?php echo str_replace('{{ENV}}', ENVIRONMENT, $file) ?></td>
	                        <td><?php echo $perm ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>' ?></td>
	                    </tr>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>

    </div>

    <?php if (@$can_install === false): ?>
        <div class="alert alert-danger" role="alert">
            Por favor verifique os problemas acima e recarregue a página!
        </div>
    <?php else: ?>
        <?php if (ENVIRONMENT === 'development'): ?>
            <?php redirect(site_url('install/create_db')) ?>
        <?php else: ?>
            <?php redirect(site_url('install/import')); ?>
        <?php endif ?>
        
    <?php endif ?>


<?php echo $this->load->view('includes/footer'); ?>