<?php
$show_extended_settings = ! empty($extended_settings);
?>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
	<div class="tabbable">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#main-settings" data-toggle="tab"><?php echo lang('set_tab_settings') ?></a>
			</li>
			<li>
				<a href="#security" data-toggle="tab"><?php echo lang('set_tab_security') ?></a>
			</li>
		<?php if (has_permission('Site.Developer.View')) : ?>
			<li>
				<a href="#developer" data-toggle="tab"><?php echo lang('set_tab_developer') ?></a>
			</li>
		<?php endif;
			if ($show_extended_settings) :
		?>
			<li>
				<a href="#extended" data-toggle="tab"><?php echo lang('set_tab_extended') ?></a>
			</li>
		<?php endif; ?>
		</ul>
		<br>
		<div class="tab-content" style="border: 0;">
			<!-- Start of Main Settings Tab Pane -->
			<div class="tab-pane active" id="main-settings">
				<div class="panel panel-default">
				    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('bf_site_information') ?></h6></div>
				    <div class="panel-body">

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="title"><?php echo lang('bf_site_name') ?></label>
							<div class="col-sm-10">
								<input type="text" name="title" id="title" class="form-control" value="<?php echo set_value('site.title', isset($settings['site.title']) ? $settings['site.title'] : '') ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="system_email"><?php echo lang('bf_site_email') ?></label>
							<div class="col-sm-10">
								<input type="text" name="system_email" id="system_email" class="form-control" value="<?php echo set_value('site.system_email', isset($settings['site.system_email']) ? $settings['site.system_email'] : '') ?>" />
								<p class="help-block"><?php echo lang('bf_site_email_help') ?></p>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="status"><?php echo lang('bf_site_status') ?></label>
							<div class="col-sm-10">
								<select name="status" id="status" class="select2">
									<option value="1" <?php echo isset($settings) && $settings['site.status'] == 1 ? 'selected="selected"' : set_select('site.status', '1') ?>><?php echo lang('bf_online') ?></option>
									<option value="0" <?php echo isset($settings) && $settings['site.status'] == 0 ? 'selected="selected"' : set_select('site.status', '1') ?>><?php echo lang('bf_offline') ?></option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="list_limit"><?php echo lang('bf_top_number') ?></label>
							<div class="col-sm-10">
								<input type="text" name="list_limit" id="list_limit" value="<?php echo set_value('list_limit', isset($settings['site.list_limit']) ? $settings['site.list_limit'] : '')  ?>" class="form-control" />
								<p class="help-block"><?php echo lang('bf_top_number_help') ?></p>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="languages"><?php echo lang('bf_language') ?></label>
							<div class="col-sm-10">
								<select name="languages[]" id="languages" multiple="multiple" class="select2">
						<?php
							if (is_array($languages) && count($languages)) :
								foreach ($languages as $language) :
									$selected = in_array($language, $selected_languages) ? TRUE : FALSE;
						?>
									<option value="<?php e($language); ?>" <?php echo set_select('languages', $language, $selected); ?>><?php e(ucfirst($language)); ?></option>
						<?php
								endforeach;
							endif;
						?>
								</select>
								<p class="help-block"><?php echo lang('bf_language_help') ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Start of Security Settings Tab Pane -->
			<div class="tab-pane" id="security">
				<div class="panel panel-default">
				    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('bf_security') ?></h6></div>
				    <div class="panel-body">

						<div class="form-group">
							<label for="allow_register" class="col-sm-2 control-label text-right"><?php echo lang('bf_allow_register') ?></label>
							<div class="col-sm-10">
								<input type="checkbox" class="styled" name="allow_register" id="allow_register" value="1" <?php echo $settings['auth.allow_register'] == 1 ? 'checked="checked"' : set_checkbox('auth.allow_register', 1); ?> />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="user_activation_method"><?php echo lang('bf_activate_method') ?></label>
							<div class="col-sm-10">
								<select name="user_activation_method" id="user_activation_method" class="select2">
									<option value="0" <?php echo $settings['auth.user_activation_method'] == 0 ? 'selected="selected"' : ''; ?>><?php echo lang('bf_activate_none') ?></option>
									<option value="1" <?php echo $settings['auth.user_activation_method'] == 1 ? 'selected="selected"' : ''; ?>><?php echo lang('bf_activate_email') ?></option>
									<option value="2" <?php echo $settings['auth.user_activation_method'] == 2 ? 'selected="selected"' : ''; ?>><?php echo lang('bf_activate_admin') ?></option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="login_type"><?php echo lang('bf_login_type') ?></label>
							<div class="col-sm-10">
								<select name="login_type" id="login_type" class="select2">
									<option value="email" <?php echo $settings['auth.login_type'] == 'email' ? 'selected="selected"' : ''; ?>><?php echo lang('bf_login_type_email') ?></option>
									<option value="username" <?php echo $settings['auth.login_type'] == 'username' ? 'selected="selected"' : ''; ?>><?php echo lang('bf_login_type_username') ?></option>
									<option value="both" <?php echo $settings['auth.login_type'] == 'both' ? 'selected="selected"' : ''; ?>><?php echo lang('bf_login_type_both') ?></option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" id="use_usernames_label"><?php echo lang('bf_use_usernames') ?></label>
							<div class="col-sm-10" aria-labelledby="use_usernames_label" role="group">
								<label class="radio" for="use_username">
									<input type="radio" class="styled" id="use_username" name="use_usernames" value="1" <?php echo $settings['auth.use_usernames'] == 1 ? 'checked="checked"' : set_radio('auth.use_usernames', 1); ?> />
									<span><?php echo lang('bf_username') ?></span>
								</label>
								<label class="radio" for="use_email">
									<input type="radio" class="styled" id="use_email" name="use_usernames" value="0" <?php echo $settings['auth.use_usernames'] == 0 ? 'checked="checked"' : set_radio('auth.use_usernames', 0); ?> />
									<span><?php echo lang('bf_email') ?></span>
								</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right"><?php echo lang('bf_display_name'); ?></label>
							<div class="col-sm-10">
								<label class="checkbox" for="allow_name_change">
									<input type="checkbox" class="styled" name="allow_name_change" id="allow_name_change" <?php echo isset($settings['auth.allow_name_change']) && $settings['auth.allow_name_change'] == 1 ? 'checked="checked"' : set_checkbox('auth.allow_remember', 1); ?> >
									<?php echo lang('set_allow_name_change_note'); ?>
								</label>

								<div class="row" id="name-change-settings" style="<?php if (!$settings['auth.allow_name_change']) echo 'display: none'; ?>">
									<div class="col-sm-4">
										<div class="input-group">
											<input class="form-control" type="number" name="name_change_frequency" value="<?php echo $settings['auth.name_change_frequency'] ?>">
											<span class="input-group-addon"><?php echo lang('set_name_change_frequency') ?></span>
										</div>										
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input class="form-control" type="number" name="name_change_limit" value="<?php echo $settings['auth.name_change_limit'] ?>">
											<span class="input-group-addon"><?php echo lang('set_days') ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="allow_remember"><?php echo lang('bf_allow_remember') ?></label>
							<div class="col-sm-10">
								<input class="styled" type="checkbox" name="allow_remember" id="allow_remember" value="1" <?php echo $settings['auth.allow_remember'] == 1 ? 'checked="checked"' : set_checkbox('auth.allow_remember', 1); ?> />
							</div>
						</div>

						<div class="form-group" id="remember-length" style="<?php if (!$settings['auth.allow_remember']) echo 'display: none'; ?>">
							<label class="col-sm-2 control-label text-right" for="remember_length"><?php echo lang('bf_remember_time') ?></label>
							<div class="col-sm-10">
								<select name="remember_length" id="remember_length" class="select2">
									<option value="604800"  <?php echo $settings['auth.remember_length'] == '604800' ?  'selected="selected"' : '' ?>>1 <?php echo lang('bf_week') ?></option>
									<option value="1209600" <?php echo $settings['auth.remember_length'] == '1209600' ? 'selected="selected"' : '' ?>>2 <?php echo lang('bf_weeks') ?></option>
									<option value="1814400" <?php echo $settings['auth.remember_length']== '1814400' ? 'selected="selected"' : '' ?>>3 <?php echo lang('bf_weeks') ?></option>
									<option value="2592000" <?php echo $settings['auth.remember_length'] == '2592000' ? 'selected="selected"' : '' ?>>30 <?php echo lang('bf_days') ?></option>
								</select>
							</div>
						</div>

						<div class="form-group" id="password-strength">
							<label class="col-sm-2 control-label text-right" for="password_min_length"><?php echo lang('bf_password_strength') ?></label>
							<div class="col-sm-10">
								<input type="number" class="form-control" name="password_min_length" id="password_min_length" value="<?php echo set_value('password_min_length', isset($settings['auth.password_min_length']) ? $settings['auth.password_min_length'] : '') ?>" class="span1" />
								<p class="help-block"><?php echo lang('bf_password_length_help') ?></p>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right"><?php echo lang('set_option_password'); ?></label>
							<div class="col-sm-10">
								<label class="checkbox" for="password_force_numbers">
									<input class="styled" type="checkbox" name="password_force_numbers" id="password_force_numbers" value="1" <?php echo set_checkbox('password_force_numbers', 1, isset($settings['auth.password_force_numbers']) && $settings['auth.password_force_numbers'] == 1 ? TRUE : FALSE); ?> />
									<?php echo lang('bf_password_force_numbers') ?>
								</label>
								<label class="checkbox" for="password_force_symbols">
									<input class="styled" type="checkbox" name="password_force_symbols" id="password_force_symbols" value="1" <?php echo set_checkbox('password_force_symbols', 1, isset($settings['auth.password_force_symbols']) && $settings['auth.password_force_symbols'] == 1 ? TRUE : FALSE); ?> />
									<?php echo lang('bf_password_force_symbols') ?>
								</label>
								<label class="checkbox" for="password_force_mixed_case">
									<input class="styled" type="checkbox" name="password_force_mixed_case" id="password_force_mixed_case" value="1" <?php echo set_checkbox('password_force_mixed_case', 1, isset($settings['auth.password_force_mixed_case']) && $settings['auth.password_force_mixed_case'] == 1 ? TRUE : FALSE); ?> />
									<?php echo lang('bf_password_force_mixed_case') ?>
								</label>
								<label class="checkbox" for="password_show_labels">
									<input class="styled" type="checkbox" name="password_show_labels" id="password_show_labels" value="1" <?php echo set_checkbox('password_show_labels', 1, isset($settings['auth.password_show_labels']) && $settings['auth.password_show_labels'] == 1 ? TRUE : FALSE); ?> />
									<?php echo lang('bf_password_show_labels') ?>
								</label>
							</div>
						</div>

						<div class="form-group">
							<label for="password_iterations" class="col-sm-2 control-label text-right"><?php echo lang('set_password_iterations') ?></label>
							<div class="col-sm-10">
								<select name="password_iterations" class="select2">
									<option <?php echo set_select('password_iterations', 2, $settings['password_iterations'] == 2) ?>>2</option>
									<option <?php echo set_select('password_iterations', 4, $settings['password_iterations'] == 4) ?>>4</option>
									<option <?php echo set_select('password_iterations', 8, $settings['password_iterations'] == 8) ?>>8</option>
									<option <?php echo set_select('password_iterations', 16, $settings['password_iterations'] == 16) ?>>16</option>
									<option <?php echo set_select('password_iterations', 31, $settings['password_iterations'] == 31) ?>>31</option>
								</select>
								<span class="help-block"><?php echo lang('bf_password_iterations_note'); ?></span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('set_force_reset') ?></label>
							<div class="col-sm-10">
								<a href="<?php echo site_url(ADMIN_AREA .'/settings/users/force_password_reset_all'); ?>" class="btn btn-danger" onclick="return confirm('<?php echo lang('set_password_reset_confirm') ?>');">
									<?php echo lang('set_reset'); ?>
								</a>
								<span class="help-block"><?php echo lang('set_reset_note'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php if (has_permission('Site.Developer.View')) : ?>
			<!-- Start of Developer Settings Tab Pane -->
			<div class="tab-pane" id="developer">
				<div class="panel panel-default">
				    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('set_option_developer'); ?></h6></div>
				    <div class="panel-body">

						<div class="form-group">
							<div class="controls" style="padding: 10px 30px;">
								<label class="checkbox" for="show_profiler">
									<input class="styled" type="checkbox" name="show_profiler" id="show_profiler" value="1" <?php echo  $settings['site.show_profiler'] == 1 ? 'checked="checked"' : set_checkbox('auth.use_extended_profile', 1); ?> />
									<span><?php echo lang('bf_show_profiler') ?></span>
								</label>
								<label class="checkbox" for="show_front_profiler">
									<input class="styled" type="checkbox" name="show_front_profiler" id="show_front_profiler" value="1" <?php echo  $settings['site.show_front_profiler'] == 1 ? 'checked="checked"' : set_checkbox('site.show_front_profiler', 1); ?> />
									<span><?php echo lang('bf_show_front_profiler') ?></span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Developer Tab Options Pane -->
		<?php endif;
			if ($show_extended_settings) :
		?>
			<!-- Start of Extended Settings Tab Pane -->
			<div class="tab-pane" id="extended">
				<div class="panel panel-default">
				    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('set_option_extended'); ?></h6></div>
				    <div class="panel-body">
						<?php
							foreach ($extended_settings as $field)
							{
								if ( empty($field['permission'])
									|| $field['permission'] === FALSE
									|| ( ! empty($field['permission']) && has_permission($field['permission']))
									)
								{
									$form_error_class = form_error($field['name']) ? ' error' : '';
									$field_control = '';

									if ($field['form_detail']['type'] == 'dropdown')
									{
										echo form_dropdown($field['form_detail']['settings'], $field['form_detail']['options'], set_value($field['name'], isset($settings['ext.' . $field['name']]) ? $settings['ext.' . $field['name']] : ''), $field['label']);
									}
									elseif ($field['form_detail']['type'] == 'checkbox')
									{
										$field_control = form_checkbox($field['form_detail']['settings'], $field['form_detail']['value'], isset($settings['ext.' . $field['name']]) && $field['form_detail']['value'] == $settings['ext.' . $field['name']] ? TRUE : FALSE);
									}
									elseif ($field['form_detail']['type'] == 'state_select')
									{
										if ( ! is_callable('state_select'))
										{
											$this->load->config('address');
											$this->load->helper('address');
										}
										$field_control = state_select(isset($settings['ext.' . $field['name']]) ? $settings['ext.' . $field['name']] : 'CA', 'CA', 'US', $field['name'], 'span6 chzn-select');
									}
									elseif ($field['form_detail']['type'] == 'country_select')
									{
										if ( ! is_callable('country_select'))
										{
											$this->load->config('address');
											$this->load->helper('address');
										}
										$field_control = country_select(set_value($field['name'], isset($settings['ext.' . $field['name']]) ? $settings['ext.' . $field['name']] : 'US'), 'US', $field['name'], 'span6 chzn-select');
									}
									else
									{
										$form_method = 'form_' . $field['form_detail']['type'];
										if (is_callable($form_method))
										{
											echo $form_method($field['form_detail']['settings'], set_value($field['name'], isset($settings['ext.' . $field['name']]) ? $settings['ext.' . $field['name']] : ''), $field['label']);
										}
									}

									if ( ! empty($field_control)) :
								?>
										<div class="control-group<?php echo $form_error_class; ?>">
											<label class="control-label" for="<?php echo $field['name']; ?>"><?php echo $field['label']; ?></label>
											<div class="controls">
												<?php echo $field_control; ?>
											</div>
										</div>
								<?php
									endif;
								}
							}
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>

	<div class="form-actions text-right">
		<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('bf_action_save') . ' ' . lang('bf_context_settings'); ?>" />
	</div>

<?php echo form_close(); ?>