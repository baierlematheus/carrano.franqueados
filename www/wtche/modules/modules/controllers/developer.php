<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtche.uy/#license
 * @link      http://www.wtche.uy
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Builder controller
 *
 * O módulo que deixa a peãozada loca de facera!.
 *
 * @package    WTchê
 * @subpackage Modules_Database
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtche.uy/#guides
 *
 */
class Developer extends Admin_Controller
{

	//---------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Site.Developer.View');
		$this->auth->restrict('WTche.Modules.View');

		$this->lang->load('modules');

		Template::set('toolbar_title', lang('modules_title'));

		Template::set_block('sub_nav', 'developer/_sub_nav');

	}//end __construct()

	//---------------------------------------------------------------

	public function index() {
		
		//Lista apenas os módulos da pasta application
		$modules = module_list(true); 

		foreach ($modules as &$module) {
			$m = new stdClass();
			$m->folder = $module; 
			$m->config = module_config($module);
			$module = $m;
		}

		Template::set('modules', $modules);

		Template::render();

	}

	public function create() {
		
		if ($this->input->post('create')) {

			$this->form_validation->set_rules('module_folder', lang('modules_module_folder'), 'required|callback_unique_module');
			$this->form_validation->set_rules('module_name', lang('modules_module_name'), 'required|convert_quotes_to_entities');
			
			if ($this->form_validation->run() == FALSE) {
				Template::render();
			} else {

				$this->load->library('modulebuilder');

				$builder_status = $this->modulebuilder->buildModule($this->input->post());

				if ($builder_status['error'] === false) {
					Template::set_message(lang('modules_module_create_success'), 'success');
					redirect(ADMIN_AREA .'/developer/modules');
				} else {
					Template::set_message(lang('modules_module_create_error'), 'error');
					logit($builder_status);
					Template::render();
				}
			}

		} else {
			Template::render();
		}



	}

	public function unique_module($module_folder) {
		if (in_array($module_folder, module_list())) {
			$this->form_validation->set_message('unique_module', lang('modules_module_folder_duplicated'));
			return FALSE;
		} else {
			return TRUE;
		}
	}



	/* TO DO
	public function delete($module) {
		
		if ($this->input->post('confirm')) {
			
			$this->load->library('migrations/migrations');

			//Realiza o migration
			$migrations = $this->migrations->version(0, $module."_");
			if ($migrations !== FALSE && strlen($this->migrations->error) == 0) {
				if ($migrations === 0) {
					log_activity($this->auth->user_id(), 'Migrate Module: '. $module .' Uninstalled from: ' . $this->input->ip_address(), 'migrations');
				}
			}

			if (delete_module($module)) { FALTANDO IMPLEMENTAR ESSA PARTE COM SEGURANÇA!
				log_activity($this->auth->user_id(), 'Delete Module Folder: '. $module .' from: ' . $this->input->ip_address(), 'migrations');
			}

			Template::set_message(lang('modules_module_delete_sucess'), 'success');

			redirect(ADMIN_AREA . '/developer/modules');

		}

		Template::render();

	}*/


}//end class
