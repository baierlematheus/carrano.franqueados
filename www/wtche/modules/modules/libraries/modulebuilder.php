<?php
/**
 * WTchê
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtche.uy/#license
 * @link      http://www.wtche.uy
 * @since     Version 1.0
 * @filesource
 */

/**
 * Module Builder library
 *
 * Faz toda a criação dos arquivos para os módulos
 *
 */
class Modulebuilder
{

    public $CI;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function buildModule($data) {

        $current_user = $this->CI->user_model->find($this->CI->auth->user_id());
        $data['username']          = $current_user->username;

        $content = array();

        $content['config'] = $this->buildConfig($data);

        $content['controllers']['content'] = $this->buildController('content', $data);
        $content['controllers'][$data['module_folder']] = $this->buildController('site', $data);

        $content['acl_migration'] = $this->buildAclMigration($data);

        $content['model'] = $this->buildModel($data);

        $content['view'] = $this->buildView($data);

        $write_status = $this->writeFiles($data['module_folder'], $content);
        $data['error'] = false;
        if (! $write_status['status']) {
            // Write failed.
            $data['error']     = true;
            $data['error_msg'] = $write_status['error'];
        }

        return $data;
    }


    private function buildConfig($data) {
        return $this->CI->load->view('_files/config/config', $data, true);
    }

    private function buildController($type, $data) {
        return $this->CI->load->view('_files/controllers/' . $type, $data, true);
    }

    private function buildAclMigration($data) {
        return $this->CI->load->view('_files/migrations/acl_migration', $data, true);
    }

    private function buildModel($data) {
        return $this->CI->load->view('_files/models/model', $data, true);
    }

    private function buildView($data) {
        return $this->CI->load->view('_files/views/index', $data, true);
    }

    private function writeFiles($module_name, $content) {

        // Load the constants config if DIR_WRITE_MODE is undefined.
        defined('DIR_WRITE_MODE') || $this->CI->load->config('constants');

        $error_msg  = 'Module Builder:';
        $modulePath = APPPATH . "../modules/{$module_name}";

        // Make the $modulePath directory if it does not exist
        if (!is_dir($modulePath) && !mkdir($modulePath, DIR_WRITE_MODE)) {
            $errorMessage = "failed to make directory {$modulePath}";
            log_message('error', $errorMessage);

            return array(
                'status' => false,
                'error'  => "{$error_msg} {$errorMessage}",
            );
        }

        //echo "<pre>";var_dump(is_writable($modulePath));die;

        $ret_val = array('status' => true);

        // Make all of the directories required within the $modulePath
        mkdir("{$modulePath}/config/", DIR_WRITE_MODE);
        mkdir("{$modulePath}/controllers/", DIR_WRITE_MODE);
        mkdir("{$modulePath}/models/", DIR_WRITE_MODE);
        mkdir("{$modulePath}/migrations/", DIR_WRITE_MODE);
        mkdir("{$modulePath}/views/", DIR_WRITE_MODE);

        // Load the file helper (used for write_file() calls in the loop)
        $this->CI->load->helper('file');

        // Loop to save all the files to disk - considered using a db but
        // this makes things more portable and easier for a user to install

        // @todo revise the interior loops for clarity
        foreach ($content as $type => $value) {
            if ($type == 'controllers') {
                foreach ($content[$type] as $name => $value) {
                    if ($value != '') {
                        if (! write_file("{$modulePath}/{$type}/{$name}.php", $value)) {
                            $errorMessage = "failed to write file {$modulePath}/{$type}/{$name}.php";
                            log_message('error', $errorMessage);
                            $ret_val['status']  = false;
                            $ret_val['error']   = "{$error_msg} {$errorMessage}";
                            unset($errorMessage);
                            break;
                        }
                    }
                }
            } elseif ($value != '') {
                // If the content is not blank.
                $file_name = $module_name;
                $path = "{$modulePath}/{$type}s";

                switch ($type) {
                    case 'acl_migration':
                        $file_name = "001_Install_{$file_name}_permissions";
                        $path = "{$modulePath}/migrations";
                        break;
                    case 'model':
                        $file_name .= "_model";
                        break;
                    case 'config':
                        $file_name = "config";
                        $path = "{$modulePath}/config";
                        break;
                    case 'view':
                        $file_name = 'index';
                        $path = "{$modulePath}/views";
                        break;
                    default:
                        break;
                }

                if (! is_dir($path)) {
                    $path = "{$modulePath}";
                }

                $ext = 'php';
                if (! write_file("{$path}/{$file_name}.{$ext}", $value)) {
                    $errorMessage = "failed to write file {$path}/{$file_name}.{$ext}";
                    log_message('error', $errorMessage);
                    $ret_val['status'] = false;
                    $ret_val['error']  = "{$error_msg} {$errorMessage}";
                    break;
                }
            }
        }
        
        return $ret_val;

    }

}