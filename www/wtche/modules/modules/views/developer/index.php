<div class="panel panel-default">
    <div class="table-responsive">

		<?php if (isset($modules) && is_array($modules)) :?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th><?php echo lang('modules_module_folder'); ?></th>
						<th><?php echo lang('modules_module_name'); ?></th>
						<th><?php echo lang('modules_module_description'); ?></th>
						<th><?php echo lang('modules_module_version'); ?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($modules as $module) : ?>
						<tr>
							<td><?php echo $module->folder ?></td>
							<td><?php echo @$module->config['name'] ?></td>
							<td><?php echo @$module->config['description'] ?></td>
							<td><?php echo @$module->config['version'] ?></td>
							<td class="text-right">
								<!-- <a href="<?php echo site_url(ADMIN_AREA . '/developer/modules/delete/' . $module->folder) ?>" class="btn btn-xs btn-danger"><?php echo lang('modules_module_delete'); ?></a> -->
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

		<?php else : ?>
			<br/>
			<div class="alert alert-info fade in ">
				<a class="close" data-dismiss="alert">&times;</a>
				<?php echo lang('modules_empty') ?>
			</div>
		<?php endif; ?>
	</div>
</div>