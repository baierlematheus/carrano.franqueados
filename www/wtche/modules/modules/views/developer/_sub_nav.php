<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
</div>

<ul class="nav nav-tabs">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/modules') ?>"><?php echo lang('modules_list'); ?></a>
	</li>
	<!-- <li <?php echo $this->uri->segment(4) == 'install' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/modules/install') ?>"><?php echo lang('modules_install'); ?></a>
	</li> -->
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/modules/create') ?>"><?php echo lang('modules_create'); ?></a>
	</li>
</ul>
<br>