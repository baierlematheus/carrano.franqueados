<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-bordered"'); ?>
    <div class="panel panel-default">
        <div class="panel-body">

        	<div class="form-group <?php echo form_error('module_folder') ? 'has-error' : ''; ?>">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('modules_module_folder') ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" size="10" name="module_folder" value="<?php echo set_value('module_folder'); ?>">
					<p class="help-block"><?php echo lang('modules_module_folder_help') ?></p>
				</div>
			</div>

			<div class="form-group <?php echo form_error('module_name') ? 'has-error' : ''; ?>">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('modules_module_name') ?></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" size="10" name="module_name" value="<?php echo set_value('module_name'); ?>">
				</div>
			</div>

        </div>
    </div>

    <div class="form-actions text-right">
		<input type="submit" name="create" class="btn btn-success" value="<?php echo lang('modules_module_create') ?>" />
	</div>

<?php echo form_close(); ?>