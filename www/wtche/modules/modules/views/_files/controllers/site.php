<?php 
$controller_name = ucwords($module_folder);
echo "<?php defined('BASEPATH') || exit('No direct script access allowed');

class {$controller_name} extends Front_Controller
{
    public function index() {
		\$this->load->view(__FUNCTION__);
	}
}";