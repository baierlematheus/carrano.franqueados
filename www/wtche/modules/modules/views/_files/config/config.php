<?php
echo "<?php defined('BASEPATH') || exit('No direct script access allowed');" .
PHP_EOL . "
\$config['module_config'] = array(
	'description'	=> '{$module_name}',
	'name'		    => '{$module_name}',
	'version'		=> '0.0.1',
	'author'		=> '{$username}',
);";