<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


$lang['modules_title']					= 'Módulos';
$lang['modules_list']					= 'Módulos Disponíveis';
$lang['modules_install']				= 'Instalar Módulos';
$lang['modules_create']					= 'Criar Módulo';

$lang['modules_empty']					= 'Nenhum módulo instalado';

$lang['modules_module_folder']			= 'Pasta';
$lang['modules_module_folder_help']		= 'Deve ser em minúsculo somente com letras e _';
$lang['modules_module_name']			= 'Nome';
$lang['modules_module_description']		= 'Descrição';
$lang['modules_module_version']			= 'Versão';
$lang['modules_module_create']			= 'Criar';
$lang['modules_module_delete']			= 'Remover';
$lang['modules_module_cancel']			= 'Cancelar';

$lang['modules_module_delete_title']	= 'Muita calma nessa hora!';
$lang['modules_module_delete_message']	= 'Ao confirmar essa ação:';
$lang['modules_module_delete_message_database']	= 'As tabelas relacionadas diretamente ao módulo serão deletadas';
$lang['modules_module_delete_message_folder']	= 'A pasta contendo todos os arquivos do módulo será apagada';

$lang['modules_module_delete_sucess']	= 'Módulo deletado com sucesso!';
$lang['modules_module_create_success']	= 'Módulo criado com sucesso!';
$lang['modules_module_create_error']	= 'Erro ao criar o Módulo! Descrição no Log do Profiler...';

//Validations
$lang['modules_module_folder_duplicated']			= 'Este nome de pasta já existe!';
