<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Analytics extends Migration
{

	/**
	 * Permissions to Migrate
	 *
	 * @var Array
	 */
	private $permission_values = array(); //Pupulado no Construct

	/**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $permissions_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$module_name = get_module_name(__FILE__);

		/**
		 * Permissions to Migrate
		 *
		 * @var Array
		 */
		$this->permission_values = array(
			array(
				'name' => ucfirst($module_name) . '.Settings.View',
				'description' => '',
				'status' => 'active',
			),
			array(
				'name' => ucfirst($module_name) . '.Settings.Manage',
				'description' => '',
				'status' => 'active',
			)
		);

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{

		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->permissions_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);

		$this->db->insert('settings', array('name' => 'analytics.code', 			'module' => 'analytics', 	'value' => ''));

	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->permissions_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->permissions_name, array('name' => $permission_value['name']));
		}

		$this->db->like('name', 'analytics.', 'after');
		$this->db->delete('settings'); 
	}

	//--------------------------------------------------------------------

}