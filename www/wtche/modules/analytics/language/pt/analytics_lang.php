<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");


$lang["analytics_configuration"] 	= "Configurações do Analytics";
$lang["analytics_code"] 			= "Código do Analytics";
$lang["analytics_code_help"] 		= "Cadastrar aqui todo o javascript que deve ser injetado nas páginas";