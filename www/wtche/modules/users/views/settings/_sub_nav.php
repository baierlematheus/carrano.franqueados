<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
	<?php if(has_permission('WTche.Users.Manage')): ?>
		<div class="pull-right range">
			<a class="btn btn-default <?php echo $this->uri->segment(4) == '' ? 'active' : '' ?>" href="<?php echo site_url(ADMIN_AREA .'/settings/users') ?>"><?php echo lang('bf_users'); ?></a>
			<a class="btn btn-success <?php echo $this->uri->segment(4) == 'create' ? 'active' : '' ?>" href="<?php echo site_url(ADMIN_AREA .'/settings/users/create') ?>" id="create_new"><?php echo lang('bf_new') .' '. lang('bf_user'); ?></a>
		</div>
	<?php endif ?>
</div>