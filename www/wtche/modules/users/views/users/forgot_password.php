
<?php echo form_open($this->uri->uri_string(), array('autocomplete' => 'off')); ?>

	<?php echo Template::message(); ?>

	<?php if (validation_errors()) : ?>
		<div class="callout callout-error fade in">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo validation_errors(); ?>
		</div>
	<?php endif; ?>

	<div class="popup-header">
		<span class="text-semibold"><?php echo lang('us_reset_password'); ?></span>
	</div>
	<div class="well">

		<div class="alert alert-info fade in" style="margin-bottom: 10px;">
			<?php echo lang('us_reset_note'); ?>
		</div>

		<div class="form-group has-feedback <?php echo iif( form_error('email') , 'error') ;?>">
			<label><?php echo lang('bf_email'); ?></label>
			<input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control">
			<i class="icon-email form-control-feedback"></i>
		</div>

		<div class="row form-actions">
			<div class="col-xs-5">
				<a href="<?php echo LOGIN_URL ?>" class="btn btn-default"><i class="icon-arrow-left10"></i> Voltar</a>
			</div>
			<div class="col-xs-7">
				<button type="submit" name="send" class="btn btn-success pull-right"><i class="icon-menu2"></i> <?php e(lang('us_send_password')); ?></button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>
