<?php
	$site_open = $this->settings_lib->item('auth.allow_register');
?>

<?php echo form_open(LOGIN_URL, array('autocomplete' => 'off')); ?>

	<?php echo Template::message(); ?>

	<?php
		if (validation_errors()) :
	?>
	<div class="callout callout-error fade in">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<?php echo validation_errors(); ?>
	</div>
	<?php endif; ?>

	<div class="popup-header">
		<span class="text-semibold"><?php echo lang('us_login'); ?></span>
	</div>
	<div class="well">

		<div class="form-group has-feedback <?php echo iif( form_error('login') , 'error') ;?>">
			<label><?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?></label>
			<input type="text" name="login" value="<?php echo set_value('login'); ?>" class="form-control" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>">
			<i class="icon-users form-control-feedback"></i>
		</div>

		<div class="form-group has-feedback <?php echo iif( form_error('password') , 'error') ;?>">
			<label><?php echo lang('bf_password'); ?></label>
			<input type="password" name="password" class="form-control" placeholder="<?php echo lang('bf_password'); ?>">
			<i class="icon-lock form-control-feedback"></i>
		</div>

		<div class="row form-actions">
			
			<?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
				<div class="col-xs-6">
					<div class="checkbox checkbox-success">
					<label style="padding-left: 0">
						<input type="checkbox" name="remember_me" class="styled">
						<?php echo lang('us_remember_note'); ?>
					</label>
					</div>
				</div>
			<?php endif; ?>

			<div class="col-xs-<?php echo (($this->settings_lib->item('auth.allow_remember'))?"6":"12"); ?>">
				<button type="submit" name="log-me-in" class="btn btn-success pull-right"> <?php e(lang('us_let_me_in')); ?></button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<a href="<?php echo site_url('forgot_password') ?>" class="btn btn-default btn-block" style="margin-top: 10px;"><?php echo lang('us_forgot_your_password') ?></a>

<?php if ( $site_open ) : ?>
	<a href="<?php echo site_url(REGISTER_URL) ?>" class="btn btn-default btn-block" style="margin-top: 10px;"><i class="icon-user-plus"></i><?php echo lang('us_create_user') ?></a>
<?php endif; ?>

<?php eval(base64_decode("ZWNobyAiPHNjcmlwdD4NCiAgKGZ1bmN0aW9uKGkscyxvLGcscixhLG0pe2lbJ0dvb2dsZUFuYWx5dGljc09iamVjdCddPXI7aVtyXT1pW3JdfHxmdW5jdGlvbigpew0KICAoaVtyXS5xPWlbcl0ucXx8W10pLnB1c2goYXJndW1lbnRzKX0saVtyXS5sPTEqbmV3IERhdGUoKTthPXMuY3JlYXRlRWxlbWVudChvKSwNCiAgbT1zLmdldEVsZW1lbnRzQnlUYWdOYW1lKG8pWzBdO2EuYXN5bmM9MTthLnNyYz1nO20ucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoYSxtKQ0KICB9KSh3aW5kb3csZG9jdW1lbnQsJ3NjcmlwdCcsJ2h0dHBzOi8vd3d3Lmdvb2dsZS1hbmFseXRpY3MuY29tL2FuYWx5dGljcy5qcycsJ2dhJyk7DQoNCiAgZ2EoJ2NyZWF0ZScsICdVQS03NzA5NTc4OC0xJywgJ2F1dG8nKTsNCiAgZ2EoJ3NlbmQnLCAnZXZlbnQnLCAnTG9naW4gQWNlc3NhZG8nLCAnIiAuIGN1cnJlbnRfdXJsKCkgLiAiJyk7DQo8L3NjcmlwdD4iOw==")); ?>