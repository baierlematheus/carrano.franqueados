<?php echo form_open($this->uri->uri_string(), array('autocomplete' => 'off')); ?>

	<?php echo Template::message(); ?>

	<?php if (validation_errors()) : ?>
		<div class="callout callout-error fade in">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo validation_errors(); ?>
		</div>
	<?php endif; ?>

	<div class="popup-header">
		<span class="text-semibold"><?php echo lang('us_reset_password'); ?></span>
	</div>
	<div class="well">

		<div class="alert alert-info fade in" style="margin-bottom: 10px;">
			<?php echo lang('us_reset_password_note'); ?>
		</div>

		<input type="hidden" name="user_id" value="<?php echo $user->id ?>" />

		<div class="form-group has-feedback <?php echo iif( form_error('password') , 'error') ;?>">
			<label><?php echo lang('bf_password'); ?></label>
			<input type="password" name="password" class="form-control">
			<i class="icon-key form-control-feedback"></i>
			<p class="help-block"><?php echo lang('us_password_mins'); ?></p>
		</div>

		<div class="form-group has-feedback <?php echo iif( form_error('pass_confirm') , 'error') ;?>">
			<label><?php echo lang('bf_password_confirm'); ?></label>
			<input type="password" name="pass_confirm" class="form-control">
		</div>

		<div class="row form-actions">
			<div class="col-xs-12">
				<button type="submit" name="set_password" class="btn btn-success pull-right"><i class="icon-checkmark"></i> <?php e(lang('us_set_password')); ?></button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>