<?php defined('BASEPATH') || exit('No direct script access allowed');

class Graph_model extends WT_Module_Model {
	
    public $auto_forge = FALSE;
    
	public function setupCrud($context) {
		
		$this->wtche->form->addField('char')
            ->setFieldName('uuid')
            ->setLabel('Identificador')
            ->setLabelHelper('Somente letras, números, _ e -')
            ->setValidationRules('required|trim|alpha_dash|unique[graph.uuid,graph.id]')
            ->setMaxLength(255);

        $this->wtche->form->addField('char')
            ->setFieldName('file_path')
            ->setLabel('Pasta do Arquivo')
            ->setLabelHelper('/uploads/[INFORMAR]')
            ->setValidationRules('required|trim|alpha_dash')
            ->setMaxLength(255);

        $this->wtche->form->addField('number')
            ->setFieldName('width')
            ->setValidationRules('trim')
            ->setLabelHelper('
                <button type="button" class="btn btn-mini btn-info" data-container="body" data-toggle="popover" data-placement="top" title="Ajuda" data-original-title="Ajuda" data-content="
                    Setando apenas a largura, o módulo irá calcular a altura necessária para manter a imagem proporcional<br><br>
                    Setando apenas a altura, o módulo irá calcular a largura necessária para manter a imagem proporcional<br><br>
                    Setando largura e altura, o módulo irá realizar o corte de acordo com as configurações de <b>Metodo de Corte</b> abaixo
                "><i class="icon-question4"></i>Ajuda</button>')
            ->setLabel('Largura');

        $this->wtche->form->addField('number')
            ->setFieldName('height')
            ->setValidationRules('trim')
            ->setLabel('Altura');

        $this->wtche->form->addField('number')
            ->setFieldName('quality')
            ->setValidationRules('required|is_natural_no_zero|less_than[101]')
            ->setLabel('Qualidade')
            ->setValue('75')
            ->setLabelHelper('0 - 100');

        $this->wtche->form->addField('select')
            ->setFieldName('resize_method')
            ->setLabel('Metodo de Corte')
            ->setLabelHelper('
                <button type="button" class="btn btn-mini btn-info" data-container="body" data-toggle="popover" data-placement="top" title="Ajuda" data-original-title="Metodo de Corte" data-content="<b>Proporcional</b>: Mantem a altura ou largura proporcional, de acordo com a medida informada (dando prioridade para a largura)
            	<br><br><b>Crop</b>: Se a altura e largura forem setadas e não respeitarem a proporção da imagem, o módulo fará um corte das sobras
            	<br><br><b>Pad</b>: Se a altura e largura forem setadas e não respeitarem a proporção da imagem, o módulo criara um fundo de acordo com o Fundo abaixo"><i class="icon-question4"></i>Ajuda</button>')
            ->setItemsArray(array(
                'proportional' => 'Proporcional',
                'crop' => 'Crop',
                'pad' => 'Pad'
            ))
            ->setValue('proportional');

        $this->wtche->form->addField('char')
            ->setFieldName('background')
            ->setLabel('Fundo')
            ->setLabelHelper('#000000')
            ->setValue('#FFFFFF')
            ->setMask('#HHHHHH')
            ->setValidationRules('trim|max_length[7]|min_length[7]')
            ->setMaskConfig("{translation: {'H': {pattern: /[A-Fa-f0-9]/}, '#': {pattern: /#/, fallback: '#'} } }");

        $this->wtche->form->addField('switcher')
            ->setFieldName('cache')
            ->setLabel('Manter Cache')
            ->setInlineEdit()
            ->setDefaultOn();

	}

}