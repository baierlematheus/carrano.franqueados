<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_Graph_Fields extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name = 'graph';

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		/*
		Campos que serão criados no banco
		*/
		/*
		'nome_do_campo' => array(
			'type' => 'INT|VARCHAR|TEXT|DATE|DATETIME|DECIMAL|TINYINT|...',
			'constraint' => 5|255|'10,2'|1|..., 
			'unsigned' => TRUE,
			'auto_increment' => TRUE,
            'default' => 'Valor Default',
            'null' => TRUE
      	),
		*/
		'uuid' => array(
			'type' => 'VARCHAR',
			'constraint' => 255
      	),
      	'file_path' => array(
			'type' => 'VARCHAR',
			'constraint' => 255
      	),
      	'width' => array(
			'type' => 'INT',
			'constraint' => 11,
			'null' => TRUE
      	),
      	'height' => array(
			'type' => 'INT',
			'constraint' => 11,
			'null' => TRUE
      	),
      	'cache' => array(
			'type' => 'TINYINT',
			'constraint' => 1,
			'default' => 1
      	),
      	'quality' => array(
			'type' => 'INT',
			'constraint' => 3,
			'default' => 75
      	),
      	'background' => array(
			'type' => 'VARCHAR',
			'constraint' => 7,
			'null' => TRUE
      	),
      	'resize_method' => array(
			'type' => 'SET("proportional","crop","pad")',
			'default' => 'proportional'
      	)
	);
	private $foreign_keys = array(
		/*
		Chaves estrangeiras que serão criados no banco
		*/
		/*
		array(
			'target_column' => 'nome_do_campo',
			'reference_table' => 'nome_da_tabela',
			'reference_column' => 'nome_do_campo',
			'on_delete' => 'RESTRICT | CASCADE | SET NULL | NO ACTION',
			'on_update' => 'RESTRICT | CASCADE | SET NULL | NO ACTION',
      	),
		*/
	);

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);
		$this->db->query("ALTER TABLE {$this->table_name} ADD UNIQUE INDEX (uuid)");
		foreach ($this->foreign_keys as $foreign_key) {
			$this->db->query('ALTER TABLE ' . $this->table_name . '
								ADD CONSTRAINT ' . $name . '
								FOREIGN KEY (' . $foreign_key['target_column'] . ')
								REFERENCES ' . $foreign_key['reference_table'] . ' (' . $foreign_key['reference_column'] . ')
								ON DELETE ' . $foreign_key['on_delete'] . '
								ON UPDATE ' . $foreign_key['on_update']);
		}
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}