<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

/**
 * Graph Helpers
 *
 * Provides additional functions for working with graphs.
 *
 * @package    WTchê
 * @subpackage Helpers
 * @category   Helpers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */

if ( ! function_exists('graph_url'))
{

	/**
	 * Retorna a URL de acordo com o lang atual
	 *
	 * @return string
	 */
	function graph_url($uuid, $file_name)
	{
		return site_url("graph/{$uuid}/{$file_name}");
	}//end graph_url()
}