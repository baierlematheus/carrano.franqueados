<?php defined('BASEPATH') || exit('No direct script access allowed');

include_once __DIR__ . "/../vendor/autoload.php";
use Intervention\Image\ImageManager;

class Graph extends Base_Controller
{

	public function __construct() {
		parent::__construct();

		$this->output->enable_profiler(false);

	}

    public function index($uuid, $file, $subpaths = null) {

    	if ($subpaths) {
			$file = urldecode(implode('/', array_slice(func_get_args(), 1)));
		}
		
		$this->load->model('graph/graph_model');

		//Busca os dados do banco de acordo com o uuid
		$graph = $this->graph_model->find_by('uuid', $uuid);

		if (!$graph) {
			die('UUID not found');
		}

		$ext = pathinfo($file, PATHINFO_EXTENSION);
		if (empty($ext)) {
			die('Filename does not include a file extension.');
		}

		$original_file = FCPATH . 'uploads/' . $graph->file_path .'/'. $file;
		
		//Se nao tiver setada altura nem largura retorna o arquivo oriinal
		if (!$graph->width && !$graph->height) {
			$this->output
				->set_content_type($ext)
				->set_output(file_get_contents($original_file));
			return;
		}

		$this->load->helper('file');
		$uuid_dir = FCPATH . "assets/cache/graph/{$uuid}";
		if (!is_dir($uuid_dir)) {
			mkdir($uuid_dir, DIR_WRITE_MODE);
		}
		$time_dir = "{$uuid_dir}/" . md5(strtotime($graph->modified_on)?:strtotime($graph->created_on));
		if (!is_dir($time_dir)) {
			mkdir($time_dir, DIR_WRITE_MODE);
		}

		$new_file = "{$time_dir}/" . str_replace('/', '_', $file);

		//Verifica antes o cache
		if (!is_file($new_file) || !$graph->cache) {
			$image_manager = new ImageManager();
			$image = $image_manager->make($original_file);


			if ($graph->resize_method == 'proportional') {
				if ($graph->width) {
					$image->widen($graph->width);
				} else {
					$image->heighten($graph->height);
				}
			}

			if ($graph->resize_method == 'crop') {
				$image->fit($graph->width, !empty($graph->height) ? $graph->height : null);
			}

			if ($graph->resize_method == 'pad') {
				$image->widen($graph->width);
				if ($image->height() > $graph->height) {
					$image->heighten($graph->height);
				}
				$image->resizeCanvas(!empty($graph->width) ? $graph->width : null, !empty($graph->height) ? $graph->height : null, 'center', false, $graph->background ? : null );
			}

			$image->save($new_file, $graph->quality);
		}

		$this->output
				->set_content_type($ext)
				->set_output(file_get_contents($new_file));

	}

	public function preview($path, $subpath, $size, $file, $subpaths = null) {

		if ($subpaths) {
			$file = urldecode(implode('/', array_slice(func_get_args(), 3)));
		}

		$ext = pathinfo($file, PATHINFO_EXTENSION);
		if (empty($ext)) {
			die('Filename does not include a file extension.');
		}

		if ($subpath) { //Normalmente _temp
			$path .= '/' . $subpath;
		}

		$original_file = FCPATH . 'uploads/' . $path .'/'. $file;

		//Se nao tiver setado o size, retorna o arquivo original
		if (!$size) {
			$this->output
				->set_content_type($ext)
				->set_output(file_get_contents($original_file));
			return;
		}

		//Aqui vamos verificar se nao é outro tipo de arquivo e então retornar apenas um icone que represente a extensão
		if (!is_file($original_file) || !getimagesize($original_file)) {
			$original_file = FCPATH .'assets/_admin/images/extensions/'.$ext.'.png';
			if (!is_file($original_file)) {
				$original_file = FCPATH .'assets/_admin/images/extensions/_blank.png';
			}
			$ext = 'png';
		}


		$image_manager = new ImageManager();
		$image = $image_manager->make($original_file);
		$image->fit($size);
		
		echo $image->response($ext, 70);
		die;
	}

}