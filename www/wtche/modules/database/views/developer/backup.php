<?php echo form_open(ADMIN_AREA .'/developer/database/backup', 'class="form-horizontal"'); ?>

	<div class="panel panel-default">
	    <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
	    	<div class="panel-body">


				<?php if (isset($tables) && is_array($tables) && count($tables) > 0) : ?>
					<?php foreach ($tables as $table) : ?>
						<input type="hidden" name="tables[]" value="<?php e($table) ?>" />
					<?php endforeach; ?>
				<?php endif; ?>

				<div class="alert alert-info">
					<p><?php echo lang('db_backup_warning'); ?></p>
				</div>

				<br>

				<div class="form-group <?php echo form_error('file_name') ? 'has-error' : '' ?>">
					<label for="file_name" class="col-sm-2 control-label text-right"><?php echo lang('db_filename'); ?></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="file_name" id="file_name" value="<?php echo set_value('file_name', isset($file) && !empty($file) ? $file : ''); ?>" />
						<?php if (form_error('file_name')) echo '<span class="help-block">'. form_error('file_name') .'</span>'; ?>
					</div>
				</div>

				<div class="form-group <?php echo form_error('drop_tables') ? 'has-error' : '' ?>">
					<label for="drop_tables" class="col-sm-2 control-label text-right"><?php echo lang('db_drop_question') ?></label>
					<div class="col-sm-10">
						<select class="select2" name="drop_tables" id="drop_tables">
							<option value="0" <?php echo set_select('drop_tables', '0'); ?>><?php echo lang('bf_no'); ?></option>
							<option value="1" <?php echo set_select('drop_tables', '1'); ?>><?php echo lang('bf_yes'); ?></option>
						</select>
						<?php if (form_error('drop_tables')) echo '<span class="help-block">'. form_error('drop_tables') .'</span>'; ?>
					</div>
				</div>

				<div class="form-group <?php echo form_error('add_inserts') ? 'has-error' : '' ?>">
					<label for="add_inserts" class="col-sm-2 control-label text-right"><?php echo lang('db_insert_question'); ?></label>
					<div class="col-sm-10">
						<select class="select2" name="add_inserts" id="add_inserts">
							<option value="0" <?php echo set_select('add_inserts', '0'); ?>><?php echo lang('bf_no'); ?></option>
							<option value="1" <?php echo set_select('add_inserts', '1', TRUE); ?>><?php echo lang('bf_yes'); ?></option>
						</select>
						<?php if (form_error('add_inserts')) echo '<span class="help-block">'. form_error('add_inserts') .'</span>'; ?>
					</div>
				</div>

				<div class="form-group <?php echo form_error('file_type') ? 'has-error' : '' ?>">
					<label for="file_type" class="col-sm-2 control-label text-right"><?php echo lang('db_compress_question'); ?></label>
					<div class="col-sm-10">
						<select class="select2" name="file_type" id="file_type">
							<option value="txt" <?php echo set_select('file_type', 'txt', TRUE); ?>><?php echo lang('bf_none'); ?></option>
							<option value="gzip" <?php echo set_select('file_type', 'gzip'); ?>><?php echo lang('db_gzip'); ?></option>
							<option value="zip" <?php echo set_select('file_type', 'zip'); ?>><?php echo lang('db_zip'); ?></option>
						</select>
						<?php if (form_error('file_type')) echo '<span class="help-block">'. form_error('file_type') .'</span>'; ?>
					</div>
				</div>

				<br />

				<div class="alert alert-warning">
					<?php echo lang('db_restore_note'); ?>
				</div>
				<br>

				<h6 class="heading-hr"><i class="icon-database"></i> <?php echo lang('db_backup') .' '. lang('db_tables'); ?></h6>
				<ul>
					<?php foreach ($tables as $table) : ?>
						<li><?php e($table); ?></li>
					<?php endforeach; ?>
				</ul>

				<br>

			</div>

	</div>
	<div class="form-actions text-right">
		<?php echo anchor(ADMIN_AREA .'/developer/database', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
		<button type="submit" name="backup" class="btn btn-success" ><?php echo lang('db_backup'); ?></button>
	</div>

<?php echo form_close(); ?>
