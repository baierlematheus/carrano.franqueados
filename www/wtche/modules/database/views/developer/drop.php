<?php echo form_open(ADMIN_AREA .'/developer/database/drop'); ?>

	<?php if (isset($tables) && is_array($tables) && count($tables) > 0) : ?>
		<?php foreach ($tables as $table) : ?>
			<input type="hidden" name="tables[]" value="<?php e($table) ?>" />
		<?php endforeach; ?>

		<div class="alert alert-block alert-danger fade in block-inner">
			<h6><i class="icon-database"></i><?php echo lang('bf_action_delete'); ?> <?php echo lang('db_database'); ?> <?php echo lang('db_tables'); ?></h6>
			<hr>
			<h6><?php echo lang('db_drop_confirm'); ?></h6>

			<ul>
			<?php foreach($tables as $file) : ?>
				<li><?php e($file) ?></li>
			<?php endforeach; ?>
			</ul>

			<br>
			<div class="notification attention png_bg">
				<?php echo lang('db_drop_attention'); ?>
			</div>

			<div class="actions">
				<button type="submit" name="drop" class="btn btn-danger"><?php echo lang('bf_action_delete'); ?> <?php echo lang('db_tables'); ?></button>
				<?php echo anchor(ADMIN_AREA .'/developer/database', lang('bf_action_cancel'), 'class="btn btn-primary"'); ?>
			</div>
		</div>

	<?php endif; ?>

<?php echo form_close(); ?>
