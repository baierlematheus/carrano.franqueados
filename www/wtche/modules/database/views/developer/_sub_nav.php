<div class="page-header">
	<div class="page-title">
		<h1><?php echo $toolbar_title ?></h1>
	</div>
</div>

<ul class="nav nav-tabs">
	<li <?php echo $this->uri->segment(4) == '' && $this->uri->segment(3) != 'migrations' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/database') ?>"><?php echo lang('db_maintenance'); ?></a>
	</li>
	<li <?php echo $this->uri->segment(4) == 'backups' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/database/backups') ?>"><?php echo lang('db_backups'); ?></a>
	</li>
	<li <?php echo $this->uri->segment(3) == 'migrations' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(ADMIN_AREA .'/developer/migrations') ?>"><?php echo lang('db_migrations'); ?></a>
	</li>
</ul>

<br>
