<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['si_system_info'] = 'System Information';
$lang['si_system'] = 'System';
$lang['si_modules'] = 'Modules';
$lang['si_php'] = 'PHP';

$lang['si_installed_mods'] = 'Installed Modules';
$lang['si_php_info'] = 'PHP Information';

$lang['sys_mod_name'] = 'Nome do Módulo';
$lang['sys_mod_ver'] = 'Versão';
$lang['sys_mod_desc'] = 'Descrição';
$lang['sys_mod_author'] = 'Autor';
