<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controller responsável pelos formulários do painel de administração do site (Manager)
 */
class Admin_Controller_WT extends Admin_Controller
{

    /**
     * Armazena o nome do módulo que está carregado atualmente
     * @var string
     */
    public $module_name;

    /**
     * Armazena o nome do contexto que está carregado atualmente
     * @var string
     */
    public $context_name;
    
    /**
     * Armazena a instância do CodeIgniter para possibilitar a utilização das libraries compartilhadas entre
     * Models, Views, Controllers e próprias Libraries.
     * 
     * @var CI Object //$this->wtche =& get_instance();
     */
    public $wtche;

    /**
     * Armazena a query string para propagação automatica na navegação
     * @var string
     */
    public $query_string;

    /**
     * Recebe dois parâmetros que devem ser passados pelo módulo que está extendendo-o
     * @param string $context_name
     * @param string $module_name
     */
    public function __construct() {
        
        parent::__construct();

        $reflector = new ReflectionClass(get_class($this));

        $this->context_name = get_class($this);
        $this->module_name = get_module_name($reflector->getFileName());
        
        Template::set('context_name', $this->context_name);
        Template::set('module_name', $this->module_name);
        Template::set('description', $this->input->get('title'));

        $this->module_config = module_config($this->module_name);

        Template::set('module_title', $this->module_config['name']);

        $this->wtche =& get_instance();

        $this->wtche->context_name = $this->context_name;
        $this->wtche->module_name = $this->module_name;

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.View');
        $this->load->model($this->module_name . '/' . $this->module_name . '_model', null, true);

        $this->{$this->module_name.'_model'}->set_table($this->{$this->module_name.'_model'}->getTableForContext($this->wtche->context_name));

        $this->query_string = $_SERVER['QUERY_STRING'];
        Template::set('query_string', $this->query_string);

        $this->query_params = array();
        if ($this->query_string) {
            parse_str($this->query_string, $this->query_params);
        }

        Template::set('key', $this->wtche->form->model->get_key());

        Template::set('can_delete', $this->auth->has_permission(ucfirst($this->module_name) . '.' . ucfirst($this->context_name) . '.Delete'));
        Template::set('can_edit', $this->auth->has_permission(ucfirst($this->module_name) . '.' . ucfirst($this->context_name) . '.Edit'));

        //SubNav Defaut
        Template::set_block('sub_nav', 'forms/_sub_nav');

    }

    /**
     * Tela de listagem do form
     * @return void
     */
    public function index(){

        //Single Row?
        if ($this->{$this->module_name.'_model'}->isSingleRowForContext($this->wtche->context_name)) {
            
            /*
            Vamos verificar se existe algum registro cadastrado e então redirecionar
            para o create (caso não exista) ou edit desse registro (caso exista)
            */

            //Caso tenha algum parent filtrado:
            $filter = unserialize(base64_decode($this->input->get('filter')));
            if ($filter) {
                foreach ($filter as $field => $value) {
                   $this->{$this->module_name.'_model'}->where($field, $value);
                }
            }

            $rows = $this->{$this->module_name.'_model'}->find_all();

            if (count($rows) == 0) {
                redirect(site_url(ADMIN_AREA .'/' . $this->wtche->context_name . '/' . $this->wtche->module_name . '/create' . (($this->query_string)?('?'.$this->query_string):'')));
            } elseif (count($rows) == 1) {
                $row = reset($rows);
                redirect(site_url(ADMIN_AREA .'/' . $this->wtche->context_name . '/' . $this->wtche->module_name . '/edit/' . $row->id . (($this->query_string)?('?'.$this->query_string):'')));
            } //Se tiver mais de 1 registro deve mostrar a lista mesmo

        }

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        //Carrega os fields
        $fields = $this->wtche->form->getFields('list');
        Template::set('fields', $fields);

        Template::set('export_fields', $this->wtche->form->getFields('*'));

        // Deleting anything?
        if (isset($_POST['delete']))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {

                    foreach ($fields as $field) {
                        $field->trigger('before_delete', $pid);
                    }

                    // Log the activity
                    log_activity($this->current_user->id, lang('modules_act_delete_record') .': '. $pid .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($this->{$this->module_name.'_model'}->find($pid)), $this->module_name);
                    
                    $result = $this->{$this->module_name.'_model'}->delete($pid);

                    foreach ($fields as $field) {
                        $field->trigger('after_delete', $pid);
                    }

                }

                if ($result)
                {
                    Template::set_message(count($checked) .' '. lang('module_delete_success'), 'success');
                }
                else
                {
                    Template::set_message(lang('module_delete_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }

        Template::set('toolbar_title', sprintf(lang('module_manage'), $this->module_config['name']));

        Template::set_view('forms/index');

        if ($this->input->get('template')) {
            Template::render($this->input->get('template'));
        } else {
            Template::render();
        }
    }

    /**
     * Retorna os dados do módulo formatados para o DataTables
     * @return void Retorno via AJAX
     */
    public function get_list(){

        $this->output->enable_profiler(FALSE);
        
        $this->load->library('Datatables');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        //Busca os Fields para Listagem
        $fields = $this->wtche->form->getFields('list');

        $this->datatables->select($this->{$this->module_name.'_model'}->get_key());

        //Adicionamos um a um no Select
        foreach ($fields as $field) {
            if ($this->db->field_exists($field->fieldName(), $this->{$this->module_name.'_model'}->get_table())) {
                $this->datatables->select($field->fieldName());
            } elseif ($field->isStatic() && $field->value()) {
                //Caso o campo não seja do Banco vamos adicionar o valor fixo na consulta para poder ser filtrado e tudo mais
                $this->datatables->select('"' . $field->value() . '" as ' . $field->fieldName(), FALSE);
            }
        }

        $this->datatables->from($this->db->dbprefix . $this->{$this->module_name.'_model'}->get_table());

        //Caso tenha algum parent filtrado:
        $filter = unserialize(base64_decode($this->input->get('filter')));
        if ($filter) {
            foreach ($filter as $field => $value) {
               $this->datatables->where($field, $value);
            }
        }

        $this->{$this->module_name.'_model'}->trigger('before_find');

        $datatable = json_decode($this->datatables->generate());

        $this->{$this->module_name.'_model'}->trigger('after_find');

        //Percorre todos os registros para trazer o value de acordo com cada Field
        //Ex: Caso seja um select o value será um array (chave=>valor)
        foreach ($datatable->data as &$row) {
            foreach ($fields as $field) {

                //Caso o atributo não exista (Não é um campo do banco) iremos inclui-lo ao $row
                if (!property_exists($row, $field->fieldName())) {
                    $row = (object) array_merge( (array)$row, array( $field->fieldName() => 0 ) );
                }

                //Joga o value do banco pra dentro do Field
                $field->setValue($row->{$field->fieldName()}, $row->{$this->{$this->module_name.'_model'}->get_key()});

                //E então faz a mágica de acordo com cada Field =)
                $row->{$field->fieldName()} = $field->value($row);

            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($datatable));

    }

    /**
     * Retorna os dados de um registro específico de file (Pode ser usado para mais coisas?)
     * @return void Retorno via AJAX
     */
    public function get_file($field_name, $key){

        $this->output->enable_profiler(FALSE);

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD
        
        $field = $this->wtche->form->getField($field_name);

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->db->from($field->get_table())->where($field->model->get_key(),$key)->get()->row()));

    }

    /**
     * Retorna o render de um field SELECT específico possibilitando chamar via ajax e aplicar filtros aos options
     * @return html do render
     */
    public function get_select($view) {

        $field_name = $this->input->get('field_name');
        $filter_key = $this->input->get('filter_key');
        $filter_value = $this->input->get('filter_value');
        $field_value = $this->input->get('field_value');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        //Busca o Field
        $field = $this->wtche->form->getField($field_name);

        $field->where($filter_key, $filter_value);
        $field->setValue($field_value);
        if ($filter_value) {
            $field->setDependencyLoaded();
        }

        $this->output
                ->set_content_type('text/html')
                ->set_output($field->render($view));

    }

    /**
     * Salva os dados enviados através de Ajax 
     * @param  int $id ID do registro que está sendo salvo
     * @return void     Retorna TRUE em caso de sucesso ou um array de erros em formato json em caso de falha
     */
    public function save_ajax($id) {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        if ($this->save('update', $id, true)) {
            // Log the activity
            log_activity($this->current_user->id, lang('modules_act_edit_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);
            
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));

        } else {

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode($this->form_validation->error_array()));
        
        }

    }

    /**
     * Salva os dados enviados através de Ajax 
     * @param  int $id ID do registro que está sendo salvo
     * @return void     Retorna TRUE em caso de sucesso ou um array de erros em formato json em caso de falha
     */
    public function save_sortable($table) {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        foreach ($this->input->post('order') as $order) {
            
            $update_data = array();
            $update_where = array();
            foreach ($order as $key => $value) {
                if ($key != 'file_order') {
                    $update_where[$key] = $value;
                } else {
                    $update_data[$key] = $value;
                }
            }
            
            if ($update_where) {
                $this->db->update($table, $update_data, $update_where); 
            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));

    }

    /**
     * Exporta os dados de um módulo para arquivos (XLS, CSV, Etc)
     * @return void Serve arquivo
     */
    public function export() {

        if (!is_dir('uploads/_exports')) {
            mkdir('uploads/_exports');
        }

        /** Include PHPExcel */
        require_once( WTPATH . '/third_party/PHPExcel/Classes/PHPExcel.php');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD
        //Busca os Fields
        $fields = $this->wtche->form->getFields('*');

        //Get results
        //To Do: Current page only
        $results = $this->{$this->module_name.'_model'}->getche_all($this->context_name);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator($this->current_user->display_name);
        $objPHPExcel->getProperties()->setLastModifiedBy($this->current_user->display_name);
        $objPHPExcel->getProperties()->setTitle($this->module_config['name']);
        $objPHPExcel->getProperties()->setSubject($this->module_config['name']);

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle($this->module_config['name']);

        $line = 2;
        $column = 'A';
        foreach ($results as $row) {
            foreach ($fields as $field) {
                if ($this->input->post($field->fieldName())) {
                    if ($line === 2) {
                        $objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $field->label());
                    }
                    $column_value = '';
                    switch (get_class($field)) {
                        case 'File_upload':
                                if (is_object($row->{$field->fieldName()})) {
                                    $column_value = base_url("uploads/{$this->module_name}_{$field->fieldName()}/" . $row->{$field->fieldName()}->file_name);
                                } elseif (is_array($row->{$field->fieldName()})) {
                                    $column_value = implode(',',array_map(function($data) use($field){
                                        return base_url("uploads/{$this->module_name}_{$field->fieldName()}/" . $data->file_name);
                                    }, $row->{$field->fieldName()}));
                                }
                            break;
                        case 'Select':
                                if ($row->{$field->fieldName()}) {
                                    $column_value = implode(',', $row->{$field->fieldName()});
                                }
                            break;
                        case 'Module':
                                $column_value = json_encode($row->{$field->fieldName()});
                            break;
                        default:
                                $column_value = $row->{$field->fieldName()};
                            break;
                    }
                    $objPHPExcel->getActiveSheet()->SetCellValue($column++ . $line, $column_value);
                }
            }
            $line++; $column = 'A';
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $file_name = $this->module_config['name'] . '_' . date("Y-m-d_H-i-s") . '.xlsx';
        $objWriter->save('uploads/_exports/' . $file_name);

        redirect('uploads/_exports/' . $file_name);

    }

    /**
     * Carrega o Form para inserção de um novo registro
     * @return void
     */
    public function create()
    {
        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Create');


        if (isset($_POST['save']))
        {

            if ($insert_id = $this->save())
            {
                // Log the activity
                log_activity($this->current_user->id, lang('modules_act_create_record') .': '. $insert_id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);

                Template::set_message(lang('module_create_success'), 'success');
                if (isset($this->query_params['keepstate'])) {
                    redirect(current_url() . (($this->query_string)?('?'.$this->query_string):''));
                } else {
                    redirect(ADMIN_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
                } 
            }
            else
            {
                if (!isset($_POST['just_validate'])) {
                    Template::set_message(lang('module_create_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }

        

        if (!isset($_POST['just_validate'])) {
            Template::set('toolbar_title', lang('module_create') . ' ' . $this->module_config['name']);

            if (!$this->wtche->form->getFields('create')) {
                $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD
            }

            //Carrega os Fields
            $fields = $this->wtche->form->getFields('create');

            //Caso tenha um filter 
            $filter = unserialize(base64_decode($this->input->get('filter')));

            if ($filter || isset($this->query_params['hide'])) {

                if ($filter) {
                    foreach ($fields as $key => &$field) {
                        if (array_key_exists($field->fieldName(), $filter)) {
                            //Remove o field do form
                            $this->wtche->form->removeField($field->fieldName());
                            //adiciona um hidden no lugar
                            $this->wtche->form->addField('hidden')
                                ->setFieldName($field->fieldName())
                                ->setValue($filter[$field->fieldName()]);
                        }
                    }
                }

                if (isset($this->query_params['hide'])) {
                    foreach (explode(',', $this->query_params['hide']) as $field_name) {
                        $this->wtche->form->removeField($field_name);
                    }
                }

                //RE-Carrega os Fields
                $fields = $this->wtche->form->getFields('create');
            }

            Template::set('fields', $fields);
            Template::set('form_data', null);


            Template::set_view('forms/form');

            if ($this->input->get('template')) {
                Template::render($this->input->get('template'));
            } else {
                Template::render();
            }
        }
        
    }

    /**
     * Carrega o Form para edição de um registro
     * @return void
     */
    public function edit($id)
    {
        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        if (empty($id))
        {
            Template::set_message(lang('module_invalid_id'), 'error');
            redirect(ADMIN_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
        }

        Template::set('id', $id);

        ${$this->module_name} = $this->{$this->module_name.'_model'}->find($id);

        if (!${$this->module_name}) {
            Template::set_message(lang('module_invalid_id'), 'error');
            redirect(ADMIN_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
        }

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name, ${$this->module_name});//Inicia o CRUD

        if (isset($_POST['save']))
        {
            $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

            if ($this->save('update', $id))
            {
                // Log the activity
                log_activity($this->current_user->id, lang('modules_act_edit_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);

                Template::set_message(lang('module_edit_success'), 'success');
                if (isset($this->query_params['keepstate'])) {
                    redirect(current_url() . (($this->query_string)?('?'.$this->query_string):''));
                } else {
                    redirect(ADMIN_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
                } 
            }
            else
            {
                if (!isset($_POST['just_validate'])) {
                    Template::set_message(lang('module_edit_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Delete');

            // Log the activity
            log_activity($this->current_user->id, lang('modules_act_delete_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($this->{$this->module_name.'_model'}->find($id)), $this->module_name);

            if ($this->{$this->module_name.'_model'}->delete($id))
            {

                Template::set_message(lang('module_delete_success'), 'success');

                redirect(ADMIN_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
            }
            else
            {
                Template::set_message(lang('module_delete_failure') . $this->{$this->module_name.'_model'}->error, 'error');
            }
        }

        if (!isset($_POST['just_validate'])) {

            Template::set($this->module_name, ${$this->module_name});
            Template::set('toolbar_title', lang('module_edit') .' '.$this->module_config['name']);

            //Carrega os Fields
            $fields = $this->wtche->form->getFields('edit');

            //Caso tenha um filter 
            $filter = unserialize(base64_decode($this->input->get('filter')));

            if ($filter || isset($this->query_params['hide'])) {

                if ($filter) {
                    foreach ($fields as $key => &$field) {
                        if (array_key_exists($field->fieldName(), $filter)) {
                            //Remove o field do form
                            $this->wtche->form->removeField($field->fieldName());
                            //adiciona um hidden no lugar
                            $this->wtche->form->addField('hidden')
                                ->setFieldName($field->fieldName())
                                ->setValue($filter[$field->fieldName()]);
                        }
                    }
                }

                if (isset($this->query_params['hide'])) {
                    foreach (explode(',', $this->query_params['hide']) as $field_name) {
                        $this->wtche->form->removeField($field_name);
                    }
                }
                
                //RE-Carrega os Fields
                $fields = $this->wtche->form->getFields('edit');
            }

            Template::set('fields', $fields);
            Template::set('form_data', ${$this->module_name});

            //Percorre todos os campos para trazer o value de acordo com cada Field
            //Ex: Caso seja um select o value será um array (chave=>valor)
            foreach ($fields as &$field) {

                //Caso o atributo não exista (Não é um campo do banco) iremos inclui-lo ao ${$this->module_name}
                if (!isset(${$this->module_name}->{$field->fieldName()})) {
                    ${$this->module_name} = (object) array_merge( (array)${$this->module_name}, array( $field->fieldName() => 0 ) );
                }

                //Joga o value do banco pra dentro do Field
                $field->setValue(${$this->module_name}->{$field->fieldName()}, $id);

            }


            Template::set_view('forms/form');

            if ($this->input->get('template')) {
                Template::render($this->input->get('template'));
            } else {
                Template::render();
            }
        }
    }

    /**
     * Faz upload dos arquivos para o módulo
     * @return retorna um JSON contendo os dados do upload
     *
     *  O post deve conter um atributo 'field_name' contendo o nome do campo que contém o arquivo
     * 
     */ 
    public function do_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $path = 'uploads/' . $field->get_table() . '/_temp/';

        //Cria o folder caso não exista
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $config['upload_path'] = $path;

        //Cria um filename unico
        $config['encrypt_name'] = TRUE;
        
        $config['max_size'] = $field->maxSize();
        $config['allowed_types'] = $field->allowedTypes();
        $config['remove_spaces'] = false;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($this->input->post('field_name'))) {

            $error = array('error' => strip_tags($this->upload->display_errors()));

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode($error));

        } else {

            $data = $this->upload->data();

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));            

        }

    }

    /**
     * Salva algum arquivo de upload no banco
     * @param  Recebe via POST os parametros field_name, key (FK), file_name e orig_name
     * @return void     Retorna o objeto inserido em caso de sucesso ou uma mensagem de erro em formato json em caso de falha
     */
    public function save_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $insert[$this->input->post('field_name')][$this->input->post('file_name')] = array(
           $field->fk()         => $this->input->post('key'),
           'file_name'          => $this->input->post('file_name'),
           'orig_name' => $this->input->post('orig_name'),
           'is_valid'           => 1,
        );

        if ($field->trigger('after_update', $insert, $this->input->post('key'))) {

            $insert[$field->model->get_key()] = $this->db->insert_id();

            //Retorna o objeto com as informacoes
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($insert));
        } else {

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode('Erro ao inserir registro do arquivo no banco de dados!'));

        }

    }

    /**
     * Deleta algum arquivo de upload
     * @param  Recebe via POST os parametros field_name e file_name a ser deletado
     * @return void     Retorna TRUE em caso de sucesso ou uma mensagem de erro em formato json em caso de falha
     */
    public function delete_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $path = 'uploads/' . $field->get_table();

        $file_name = $this->input->post('file_name');

        if (@unlink($path . '/' . $file_name) || @unlink($path . '/_temp/' . $file_name) || !file_exists($path . '/' . $file_name) || !file_exists($path . '/_temp/' . $file_name)) {

            if ($this->db->delete($field->get_table(), array('file_name' => $file_name))) {
                
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(true));

            } else {

                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode('Erro ao remover o registro do arquivo no banco de dados!'));

            }

        } else {

            $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode('Erro ao deletar o arquivo!'));
        }

    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Salva o registro no Banco
     * @param  string  $type "insert" ou "update"
     * @param  integer $id   ID do registro que está sendo atualizado (ignorar em caso de insert)
     * @return Mixed        Um INT em caso de sucesso no insert, TRUE em caso de sucesso no update ou FALSE
     */
    private function save($type='insert', $id=0, $is_ajax = false)
    {

        $original_postdata = $_POST;
        if ($type == 'update')
        {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want
        $data = array();

        if (!$this->wtche->form->getFields(($type == 'insert')?'create':(($type == 'update')?'edit':''))) {
            $this->{$this->module_name.'_model'}->setupCrud($this->wtche->context_name);//Inicia o CRUD
        }

        //Fields
        $fields = $this->wtche->form->getFields(($type == 'insert')?'create':(($type == 'update')?'edit':''));
        foreach ($fields as $field) {

            //Trigger Actions no Field
            $_POST = $field->trigger('before_' . $type, $_POST);

            //Remove o valor do primeiro item dos selects
            if (method_exists($field, 'items') && $this->input->post($field->fieldName()) == '') {
                unset($_POST[$field->fieldName()]);
            }

            //Seta o valor do field
            if (array_key_exists($field->fieldName(), $this->input->post()) && !$field->isStatic()) {
                $field->setValue($this->input->post($field->fieldName(), !(get_class($field) == 'Text' && $field->isRich())), $id); //Não deve habilitar o XSS para o Rich)
                $_POST[$field->fieldName()] = $this->input->post($field->fieldName(), !(get_class($field) == 'Text' && $field->isRich()));
            }

            //Se for um Switcher e não tiver no post então deve considerar o valor OFF dele
            if ((!array_key_exists($field->fieldName(), $this->input->post()) && (get_class($field) == 'Switcher' && !$is_ajax)) && !$field->isStatic()) {
                $_POST[$field->fieldName()] = $field->valueOff();
                $field->setValue($field->valueOff());
            }

            if ((array_key_exists($field->fieldName(), $this->input->post()) || (get_class($field) == 'File_upload' && !$is_ajax)) && !$field->isStatic()) {

                if ($this->db->field_exists($field->fieldName(), $this->{$this->module_name.'_model'}->get_table())) {
                    $_POST[$field->fieldName()] = $field->value((object)$_POST, true);
                }

            }

            //Set Validation
            if ($type == 'insert' || array_key_exists($field->fieldName(), $this->input->post())) {
                $this->form_validation->set_rules($field->fieldName(), $field->label(), $field->validationRules());
            }

            //Se for um array em branco de um select multiplo vamos remove-lo para validar certo
            if (method_exists($field, 'items') && $field->relationMultiple() && empty($_POST[$field->fieldName()])) {
                unset($_POST[$field->fieldName()]);
            }

        }
        //Percorre novamente deixando apenas os inputs que são fields do crud
        foreach ($fields as $field) {
            if (array_key_exists($field->fieldName(), $this->input->post())) {
                if ($this->db->field_exists($field->fieldName(), $this->{$this->module_name.'_model'}->get_table())) {
                    $data[$field->fieldName()] = $_POST[$field->fieldName()];
                }
            }
        }
        if ($this->form_validation->run() === FALSE) {
            $this->form_validation->forcePostData($original_postdata);
            if (isset($_POST['just_validate'])) {
                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode($this->form_validation->error_array()));
            }
            return FALSE;

        } elseif (isset($_POST['just_validate'])) {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));
            return FALSE;
        } else {
            if ($type == 'insert') {
                $id = $this->{$this->module_name.'_model'}->insert($data);

                if (is_numeric($id))
                {
                    $return = $id;
                }
                else
                {
                    $return = FALSE;
                }
            } elseif ($type == 'update') {
                $return = $this->{$this->module_name.'_model'}->update($id, $data);
            }
            foreach ($fields as $field) {
                $field->trigger('after_' . $type, $this->input->post(), $id);
            }

        }
        
        return $return;
    }

}

/* End of file Admin_Controller_WT.php */
/* Location: ./application/core/Admin_Controller_WT.php */