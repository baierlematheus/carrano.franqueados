<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Front Controller
 *
 * This class provides a common place to handle any tasks that need to
 * be done for all public-facing controllers.
 *
 * @package    WTchê\Core\Controllers
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtche.uy/#guides
 *
 */
class Front_Controller extends Base_Controller
{

    public $module_name;
    public $context_name;
    public $wtche;

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();

        $reflector = new ReflectionClass(get_class($this));

        $this->context_name = get_class($this);
        $this->module_name = get_module_name($reflector->getFileName());

        Events::trigger('before_front_controller');

        //Para carregar o arquivo de idioma descomente abaixo
        $this->lang->load('site', site_lang(), false, true, '', 'site');

        $this->load->library('template');
        $this->load->library('assets');

        $this->set_current_user();

        Events::trigger('after_front_controller');
    }//end __construct()

}

/* End of file Front_Controller.php */
/* Location: ./application/core/Front_Controller.php */