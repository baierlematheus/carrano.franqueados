<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Router.php";

class WT_Router extends MX_Router {

	function _set_request($segments = array()) {
		if ($this->routes['translate_uri_dashes']) {
			$segments[0] = str_replace('-', '_', $segments[0]);
			if (isset($segments[1]))
			{
				$segments[1] = str_replace('-', '_', $segments[1]);
			}
  			parent::_set_request($segments);
		}
 	}

}