<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "site";
$route['404_override'] = '';

/*
This option enables you to automatically replace dashes (‘-‘) with underscores in the controller and method URI segments, 
thus saving you additional route entries if you need to do that. This is required, 
because the dash isn’t a valid class or method name character and would cause a fatal error if you try to use it.
Copiado do CI3
*/
$route['translate_uri_dashes'] = TRUE;

// Authentication
$route[LOGIN_URL]				= 'users/login';
$route[REGISTER_URL]            = 'users/register';
$route['logout']				= 'users/logout';
$route['forgot_password']		= 'users/forgot_password';
$route['reset_password/(:any)/(:any)']	= "users/reset_password/$1/$2";

// Contexts
$route[ADMIN_AREA .'/([a-z_]+)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)']		= "$2/$1/$3/$4/$5/$6/$7";
$route[ADMIN_AREA .'/([a-z_]+)/(:any)/(:any)/(:any)/(:any)/(:any)']		= "$2/$1/$3/$4/$5/$6";
$route[ADMIN_AREA .'/([a-z_]+)/(:any)/(:any)/(:any)/(:any)']		= "$2/$1/$3/$4/$5";
$route[ADMIN_AREA .'/([a-z_]+)/(:any)/(:any)/(:any)']		= "$2/$1/$3/$4";
$route[ADMIN_AREA .'/([a-z_]+)/(:any)/(:any)'] 		= "$2/$1/$3";
$route[ADMIN_AREA .'/([a-z_]+)/(:any)']				= "$2/$1/index";

$route[ADMIN_AREA]	= 'dashboard';

// Activation
$route['activate']		        = 'users/activate';
$route['activate/(:any)']		= 'users/activate/$1';
$route['resend_activation']		= 'users/resend_activation';

//Install
$route['^install/(.+)$']		= 'install/$1';
$route['^install']		        = 'install';

//Graph
$route['^graph/(.+)$']		    = 'graph/index/$1';
$route['^preview/(.+)$']		= 'graph/preview/$1';

//Route Languages
$config_content = file(__DIR__ . "/config.php");
$pattern = "/available_languages/";
eval(reset(preg_grep($pattern, $config_content)));
$available_langs = implode('|', $config['available_languages']); //Alterar no config['available_languages']

//Route Languages
//para ativar o site com multi-idioma basta descomentar abaixo
$route['^('.$available_langs.')/(.+)'] = "$2";
$route['^('.$available_langs.')$'] = $route['default_controller'];
$route['^$'] = $route['default_controller'];



/* End of file routes.php */
/* Location: ./application/config/routes.php */	