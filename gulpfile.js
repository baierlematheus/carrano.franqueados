var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var prefix      = require('gulp-autoprefixer');

var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');


// SASS ------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
var cssFiles = './www/assets/css/';
var sassFiles = [
    './www/assets/scss/**/*.scss',
    './www/assets/scss/*.scss',
];

// JS --------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
var jsDest = './www/assets/js_unminify/';
var js_minDest = './www/assets/js/';
var jsFiles = [
    './www/assets/scripts/**/*.js',
    './www/assets/scripts/*.js',
];

// PHP -------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
var phpFiles = [
    './www/modules/site/views/**/*.php',
    './www/modules/site/views/*.php',
];

var displayError = function(error) {

    // Initial building up of the error
    var errorString = '[' + error.plugin + ']';
    errorString += ' ' + error.message.replace("\n", ''); // Removes new line at the end

    // If the error contains the filename or line number add it to the string
    if (error.fileName)
        errorString += ' in ' + error.fileName;

    if (error.lineNumber)
        errorString += ' on line ' + error.lineNumber;

    // This will output an error like the following:
    // [gulp-sass] error message in file_name on line 1
    console.error(errorString);
}

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        proxy: 'http://localhost/carrano.franqueados/www'
    });
    gulp.watch(sassFiles, ['sass']);
    gulp.watch(jsFiles, ['scripts']);
    gulp.watch(jsFiles).on('change', browserSync.reload);
    gulp.watch(phpFiles).on('change', browserSync.reload);
});


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    gulp.src(sassFiles)
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: sassFiles
        }))
        .on('error', function(err) {
            displayError(err);
        })
        // Pass the compiled sass through the prefixer with defined
        .pipe(prefix(
            'last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'
        ))
        .pipe(gulp.dest(cssFiles))
        .pipe(browserSync.stream());
});


//Concat and Minify js to folder scripts
gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('global.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('global.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(js_minDest));
});


gulp.task('default', ['serve'], function() {
    // Watch the files in the paths object, and when there is a change, fun the functions in the array
    gulp.watch(sassFiles)
        // Also when there is a change, display what file was changed, only showing the path after the 'sass folder'
        .on('change', function(evt) {
            console.log(
                '[watcher] File ' + evt.path.replace(/.*(?=sass)/, '') + ' was ' + evt.type + ', compiling...'
            );
        });
});